//
//  CartListVC.swift
//  MagentoShop
//
//  Created by Bunty on 24/06/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import MGNetworkClient
import MagentoHud
import Kingfisher
import MagentoClient

class CartListVC: BaseVc ,UITableViewDataSource,UITableViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,CartListCellDelegate{
    @IBOutlet var checkoutBtn: UIButton!
    @IBOutlet var priceBtnBcg: UIView!
    @IBOutlet weak var subTotalLbl: UILabel!
    @IBOutlet var blackOverlay: UIView!
    @IBOutlet weak var emptyListIMage: UIImageView!
    @IBOutlet var tableView: UITableView!
    var selectedRow = 0;
    var cartArr:[CartItem]?
    var heightArr = [CGFloat]()
    var picker: UIPickerView!
      let toolBar = UIToolbar()
    @IBOutlet weak var checkOutBtn: UIButton!
    @IBOutlet weak var priceBtn: UIButton!
     var pickerDataSource = ["1", "2", "3", "4","5","6","7","8","9","10"];
    
    var heightsAreUpdated = false;
    var updateCount = 0;
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "My Cart"
        self.tableView.contentInset = UIEdgeInsetsMake(60, 0, 0, 0);
        // Do any additional setup after loading the view.
        self.configureView();
        self.configurePicker();
        self.fetchCartList();
        
        
    }
    @IBAction func checkOutBtnAction(sender: UIButton) {
        
        if self.cartArr?.count > 0 {
            let vc:AddressListVc = self.storyboard?.instantiateViewControllerWithIdentifier("AddressListVc") as! AddressListVc;
            vc.totolAMount = "\(self.calculateTotalPrice(self.cartArr!))"
            vc.cartArr = self.cartArr;
            self.navigationController?.pushViewController(vc, animated: true);
        }
        else
        {
            MGNetworkClient.sharedClient.showErrorALertWithMessage("You don't have any item in cart");
        }
       

        
    }
    @IBAction func priceBtnAction(sender: UIButton) {
    }
    @IBAction func toggleSideMenu(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func fetchCartList()  {
        MagentoHud.show();
        MGNetworkClient.sharedClient.getAllCartItems(["":""]) { (er:NSError?, cartItems:[CartItem]?) in
              MagentoHud.dismiss();
            if let c = cartItems
            {
                if cartItems?.count > 0
                {
                    self.tableView.hidden = false;
                    self.emptyListIMage.hidden = true;
                    
                self.cartArr = c;
                for _:CartItem in self.cartArr!
                {
                    self.heightArr.append(220);
                
                }
                self.tableView.reloadData();
                let totalPrice = self.calculateTotalPrice(self.cartArr!)
                self.priceBtn.setTitle("$ \(totalPrice)", forState: UIControlState.Normal);
                }else
                {
                    self.tableView.hidden = true;
                    self.emptyListIMage.hidden = false;
                
                }
                
            }
            
            
            if let c = cartItems
            {
                if c.count > 0
                {
                    self.cartCount = "\(c.count)";
                }else
                {
                    self.cartCount = "";
                    
                }
                
            }
            else
            {
                self.cartCount = "";
            }
            MGNetworkClient.sharedClient.saveCartCount(self.cartCount!);
            self.updateCartCount();

            
            
        }
    }
    
    
    /*
    func fetchAgainCartList()  {

        MGNetworkClient.sharedClient.getAllCartItems(["":""]) { (er:NSError?, cartItems:[CartItem]?) in
            MagentoHud.dismiss();
            if let c = cartItems
            {
                if cartItems?.count > 0
                {
                    self.tableView.hidden = false;
                    self.emptyListIMage.hidden = true;
                    
                    self.cartArr = c;
                    for _:CartItem in self.cartArr!
                    {
                        self.heightArr.append(220);
                        
                    }
                    self.tableView.reloadData();
                    let totalPrice = self.calculateTotalPrice(self.cartArr!)
                    self.priceBtn.setTitle("$ \(totalPrice)", forState: UIControlState.Normal);
                }else
                {
                    self.tableView.hidden = true;
                    self.emptyListIMage.hidden = false;
                    
                }
                
            }
            
        }
    }*/
    
    func calculateTotalPrice(arr:[CartItem]) -> Int
    {
        var totalPrice = 0;
        for cart:CartItem in arr {
            let cartPrice = cart.price ?? 0 * cart.qty!;
            totalPrice += cartPrice;
        }
    return totalPrice
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        self.showNavigationBar();
    }

    func configureView()  {
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
         self.priceBtn.setTitle("$ 0", forState: UIControlState.Normal);
        self.checkoutBtn.backgroundColor = Theme.theme().themeColor
        self.priceBtnBcg.backgroundColor = Theme.theme().themeColor;
    }
    
    
    //MARK: - TableViewDataSource
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.cartArr != nil
        {
            return (self.cartArr?.count)!;
        }
        return 0;
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cartCell:CartListCell = tableView.dequeueReusableCellWithIdentifier("CartListCell", forIndexPath: indexPath) as! CartListCell
        self.configureCartCell(cartCell, onIndexpath: indexPath);
    cartCell.selectionStyle = UITableViewCellSelectionStyle.None
        return cartCell;
        
    }
    
    func configureCartCell(cell:CartListCell,onIndexpath:NSIndexPath)
    {
        
        cell.costLabel.backgroundColor = Theme.theme().themeColor
        if  self.cartArr != nil {
            
            let cart = self.cartArr![onIndexpath.row];
            
            cell.delegate = self;
            cell.titleLabel.text = cart.product?.name!;
            cell.subtitleLabel.text = cart.product?.sku!;
            
            let cartPrice = cart.price ?? 0
            
            cell.costLabel.text = "$ \(cartPrice)"
            cell.qauntityTxtFld.text = "\(cart.qty!)"
            cell.subTotalLbl.text = "$ \(cart.qty! * cartPrice)"
            cell.qauntityTxtFld.inputView = picker;
            cell.qauntityTxtFld.inputAccessoryView = toolBar;
            picker.tag = onIndexpath.row;
            cell.qauntityTxtFld.delegate = cell;
            cell.qauntityTxtFld.tag = onIndexpath.row;
            var imageurl:String?
            if cart.product_options != nil {
                
                for attr:Attribute in cart.product_options! {
                    if attr.attribute_code == "thumbnail" ||  attr.attribute_code == "image"{
                        imageurl = attr.value;
                        break;
                    }
                }
            }
            if imageurl != nil {
                cell.itemImageView.kf_setImageWithURL(NSURL(string: imageurl!)!,
                                                      placeholderImage: UIImage(named: "placeholder"),
                                                      optionsInfo: nil,
                                                      progressBlock: { (receivedSize, totalSize) -> () in
                                                        
                                                        debugPrint("Download Progress: \(receivedSize)/\(totalSize)")
                    },
                                                      completionHandler: { (image, error, cacheType, imageURL) -> () in
                                                        //  debugPrint("Downloaded and set!")
                                                        if let im = image
                                                        {
                                                            
                                                            
                                                            let expectedWidth = self.view.frame.size.width/3;
                                                            let rowH = self.rowHeightAccordingToImage(im, neww: expectedWidth);
                                                            
                                                            //                                                                let previousH = self.heightArr[onIndexpath.row];
                                                            
                                                            //self.tableView.beginUpdates()
                                                            
                                                            
                                                            if(cell.titleLabel.numberOfLines > 1)
                                                            {
                                                                
                                                                self.heightArr[onIndexpath.row]  = rowH + 15;
                                                                
                                                            }
                                                            else
                                                            {
                                                                self.heightArr[onIndexpath.row] = rowH;
                                                            }
                                                            self.updateCount += 1;
                                                            cell.itemImageView.image = im;
                                                            
                                                            if (self.updateCount == self.cartArr?.count && self.heightsAreUpdated == false)
                                                            {
                                                                self.heightsAreUpdated = true;
                                                                self.tableView.reloadData();
                                                                
                                                            }
                                                            // self.tableView.endUpdates();
                                                            
                                                            
                                                        }
                    }
                )
            }
          

        }
    
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if self.heightArr.count >= indexPath.row+1 {
            var height = self.heightArr[indexPath.row]
            if height < 140 {
                height = 140;
            }
            return height;
        }
        return 220;
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
            }
    
    
     func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
          
            if  self.cartArr != nil {
                
                let cart:CartItem = self.cartArr![indexPath.row];
                self.deleteCartItem(String(cart.item_id!));
            }


        }
    }
    func deleteCartItem(itemID:String!) {
       
        MagentoHud.show();
        MGNetworkClient.sharedClient.deleteItemToCart(itemID!) { (er:NSError?, cartItem:CartItem?) in
            self.fetchCartList() //fetchAgainCartList()
        }
        
    }
    
    func configurePicker()  {
        picker = UIPickerView()
        
        
        picker.showsSelectionIndicator = true
        picker.delegate = self
        picker.dataSource = self
        picker.backgroundColor = UIColor.whiteColor()
      
        toolBar.barStyle = UIBarStyle.Default
        toolBar.translucent = true
        toolBar.tintColor = Theme.theme().themeColor!
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(ProductDetailVC.donePicker))
        
        
        toolBar.setItems([doneButton], animated: false)
        toolBar.userInteractionEnabled = true
//        self.hiddenTxtField.text = "1";
//        self.hiddenTxtField.inputView = picker
//        self.hiddenTxtField.inputAccessoryView = toolBar
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataSource.count;
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDataSource[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if  self.cartArr != nil {
            let indexpath = NSIndexPath.init(forRow: selectedRow, inSection: 0);
            let cartCell:CartListCell = self.tableView.cellForRowAtIndexPath(indexpath) as! CartListCell;
            //let cart = self.cartArr![indexpath.row];
            cartCell.qauntityTxtFld.text = pickerDataSource[row];
            
        }
        
    }
    func donePicker()  {
        if  self.cartArr != nil {
            let indexpath = NSIndexPath.init(forRow: selectedRow, inSection: 0);
            let cartCell:CartListCell = self.tableView.cellForRowAtIndexPath(indexpath) as! CartListCell;
            let cart = self.cartArr![indexpath.row];
            cartCell.qauntityTxtFld.text = pickerDataSource[picker.selectedRowInComponent(0)];
            cartCell.qauntityTxtFld.resignFirstResponder();
    
            self.editCartWithQuantity(Int(cartCell.qauntityTxtFld.text!), cart: cart)
        }
    }
    
    
    func editCartWithQuantity(qty:Int?,cart:CartItem) {
        cart.qty = qty
        let params = cart.getParamsFromCartItem(cart);
         let finalParams = ["cartItem":params];
        MagentoHud.show()
        MGNetworkClient.sharedClient.editAIteminCart(finalParams) { (er:NSError?, fetchedcart:CartItem?) in
           self.fetchCartList()//fetchAgainCartList();
            
        }
        
        
        
        
    }
      func selectedRowIs(selectedRow:Int)
      {
        self.selectedRow = selectedRow;
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

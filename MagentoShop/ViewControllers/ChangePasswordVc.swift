//
//  ChangePasswordVc.swift
//  MagentoShop
//
//  Created by vikaskumar on 13/10/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import SwiftForms
import MGNetworkClient
import MagentoHud

class ChangePasswordVc: FormViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Change Password"
        self.configureForm();

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func configureForm()  {
        let form = FormDescriptor()
        form.title = "User Details"
        
        
        
        // Define first section
        let section1 = FormSectionDescriptor(headerTitle: "User Acount",footerTitle: "")
        
        let firstRow = FormRowDescriptor(tag: "old password", type: .Password, title: "Password: ")
        firstRow.configuration.cell.appearance = ["textField.placeholder" : "******" as AnyObject, "textField.textAlignment" : NSTextAlignment.Right.rawValue as AnyObject]
        
        
        section1.rows.append(firstRow)
        
        let secondRow = FormRowDescriptor(tag: "New password", type: .Password, title: "New Password: ")
        secondRow.configuration.cell.appearance = ["textField.placeholder" : "******" as AnyObject, "textField.textAlignment" : NSTextAlignment.Right.rawValue as AnyObject]
        
        section1.rows.append(secondRow)

        
        
        
        
        let section4 = FormSectionDescriptor(headerTitle: "Update",footerTitle: "")
        
        let updaterow = FormRowDescriptor(tag: "updatePassword", type: .Button, title: "Update Password")
        updaterow.configuration.button.didSelectClosure = { _ in
            self.changePassword();
        }
        
        
        section4.rows.append(updaterow)
        
        
        
        form.sections = [section1,section4]
        
        self.form = form
        
        
        
    }
    func changePassword() {
        let oldPassWord = self.valueForTag("old password") as? String;
         let newPassWord = self.valueForTag("New password") as? String;
        if oldPassWord == nil {
            //self.showNormalAlert("Please enter name");
            MGNetworkClient.sharedClient.showErrorALertWithMessage("Please enter your current Password");
            return ;
        }
        else if newPassWord == nil
        {
            MGNetworkClient.sharedClient.showErrorALertWithMessage("Please enter newPassword");
            // self.showNormalAlert("Please enter last name");
            return ;
        }
        else if newPassWord?.characters.count <= 5
        {
            MGNetworkClient.sharedClient.showErrorALertWithMessage("Password should be of atleast six characters");
            // self.showNormalAlert("Password should be of atleast six characters");
            return ;
        }


        var params =  [String:AnyObject]();
        params["currentPassword"] = oldPassWord;
        params["newPassword"] = newPassWord;
        
           MagentoHud.show()
        MGNetworkClient.sharedClient.changePassword(params){ (er:NSError?, userDetails:Bool?) in
            MagentoHud.dismiss()
            if er == nil && userDetails == true
            {
                MGNetworkClient.sharedClient.showSuccessALertWithMessage("Your password is changed successfully")
                
            }
            
            
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

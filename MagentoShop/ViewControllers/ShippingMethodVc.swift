//
//  ShippingMethodVc.swift
//  MagentoShop
//
//  Created by vikaskumar on 10/10/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import MGNetworkClient
import MagentoHud
import MagentoClient


class ShippingMethodVc: BaseVc {
    
    @IBOutlet var nextBlackOverLay: UIView!
    @IBOutlet var nextBcg: UIView!
    @IBOutlet var previousBtn: UIButton!
    @IBOutlet weak var noaddressLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
      @IBOutlet var headerBottomLine: UILabel!
    
        var fetchedAddress:Addres?
    var selectionIndexPath:NSIndexPath?
    
    var shippingInfo:ShippingInfo?
    var totolAMount:String?
    var shippingAddress:String?
    var cartArr:[CartItem]?
      var shippingMethods:[ShippingMethod]?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
                self.title = "SELECT SHIPPING METHOD"
//        tableView.estimatedRowHeight = 40;
//        tableView.rowHeight = UITableViewAutomaticDimension
      // self.tableView.allowsSelectionDuringEditing = true;
      //  self.tableView.allowsMultipleSelection = false
               //self.tableView.allowsMultipleSelectionDuringEditing = false;
        self.tableView.setEditing(true, animated: true);
        self.configureView();

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        
        self.fetchAllShippingMethods()
        
    }
    func configureView()  {
        self.previousBtn.backgroundColor = Theme.theme().themeColor!
        self.nextBcg.backgroundColor = Theme.theme().themeColor!
        self.noaddressLbl.textColor = Theme.theme().themeColor!
        self.headerBottomLine.backgroundColor  = Theme.theme().themeColor!

        
    }
   
    
    @IBAction func previousBtnAction(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true);
        
    }
    @IBAction func nextBtnAction(sender: UIButton) {
        if (self.tableView.indexPathForSelectedRow != nil)
        {
            if let  methods = self.shippingMethods
            {
                
                
                if methods.count > 0 {
                    let me:ShippingMethod = methods[self.tableView.indexPathForSelectedRow!.row];
                    self.addShippingAddress(me);
                    
                }
                
                
            }

           
            
            
            
//            let vc:PaymentVC = self.storyboard?.instantiateViewControllerWithIdentifier("PaymentVC") as! PaymentVC;
//            vc.shippingAddress = self.shippingAddress;
//            self.navigationController?.pushViewController(vc, animated: true)
            
            
            
        }
        else
        {
            MGNetworkClient.sharedClient.showErrorALertWithMessage("Please select address");
        }
        
    }
    @IBAction func addNewAddress(sender: AnyObject) {
        let vc:AddressVC = self.storyboard?.instantiateViewControllerWithIdentifier("AddressVC") as! AddressVC;
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func fetchAllShippingMethods()
    {
        MagentoHud.show()
        MGNetworkClient.sharedClient.getAllShippingMethods(nil) { (er:NSError?, methods:[ShippingMethod]?) in
            MagentoHud.dismiss()
            if methods == nil
            {
                self.noaddressLbl.hidden = false
                self.tableView.hidden = true;
            }
            else
            {
                
                if methods!.count <= 0
                {
                    self.noaddressLbl.hidden = false
                    self.tableView.hidden = true;
                    
                }
                else
                {
                    self.shippingMethods = methods;
                    self.noaddressLbl.hidden = true
                    self.tableView.hidden = false;
                   // self.fetchedAddress = address;
                    self.tableView.reloadData();
                    let indexPath = NSIndexPath.init(forRow: 0, inSection: 0)
                    self.tableView.selectRowAtIndexPath(indexPath, animated: false , scrollPosition:UITableViewScrollPosition.Top);

                }
            }
            
            
        }
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if let  methods = self.shippingMethods
        {
            return methods.count;
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell:UITableViewCell!
        cell = tableView.dequeueReusableCellWithIdentifier("ShippingMethodCell", forIndexPath: indexPath) as! ShippingMethodCell
        if let  methods = self.shippingMethods
        {

            let adCell:ShippingMethodCell = cell as! ShippingMethodCell
            if methods.count > 0 {
                let me:ShippingMethod = methods[indexPath.row];
                adCell.mainTitle.text = me.carrier_title;
                adCell.subTitle1.text = me.carrier_code
                adCell.subTitle2.text = "\(me.amount!)";
                
            }
             adCell.tintColor = Theme.theme().themeColor
            
            
        }
        
       
        return cell;
    }
          func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
        {
            for iP:NSIndexPath in tableView.indexPathsForSelectedRows!
            {
                if iP == indexPath {
                    continue;
                }
                tableView .deselectRowAtIndexPath(iP, animated: true);
            }
    }
     func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath)
    {
        
    }
//     func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool
//    {
//        return true;
//    
//    }
    
//    func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle
//    {
//    
//    }
    
    func getAddressText(adr:Addres) -> String {
        
        var addText:String = ""
        
        if adr.firstname != nil {
            addText += adr.firstname!;
        }
        
        if adr.lastname != nil {
            addText += adr.lastname! + "\n";
        }
        
        
        if adr.street != nil {
            addText += adr.street! + "\n";
        }
        
        
        if adr.region != nil {
            addText += adr.region! + "\n";
        }
        
        //        if adr.city != nil {
        //            addText += adr.city! + "\n";
        //        }
        
        if adr.postcode != nil {
            addText += adr.postcode! + "\n";
        }
        
        if adr.telephone != nil {
            addText +=  "M: " + adr.telephone!;
        }
        
        // let formattedText = String(sep:"\n" ,addText)
        return addText;
    }
    
    func addShippingAddress(shippingMethod:ShippingMethod?)   {
        
        
        if  let add = self.fetchedAddress
        {
            let params =  add.getPramsFromShippingAddress(add,shippingMethod: shippingMethod!)
            
            MagentoHud.show();
            MGNetworkClient.sharedClient.makeShippingAddresses(params) { (er:NSError?, ad:Bool?,info:ShippingInfo?) in
                MagentoHud.dismiss();
        
                if er == nil
                {
                    //                let vc:PaymentVC = self.storyboard?.instantiateViewControllerWithIdentifier("PaymentVC") as! PaymentVC;
                    let vc:PaymentVC = self.storyboard?.instantiateViewControllerWithIdentifier("PaymentVC") as! PaymentVC;
                    vc.totolAMount = self.totolAMount;
                    if let ad = self.fetchedAddress
                    {
                        
                        let adText = self.getAddressText(ad);
                        vc.shippingAddress = adText;
                       // vc.fetchedAddress = self.fetchedAddress;
                        
                    }
                    vc.shippingInfo = info;
                    vc.cartArr = self.cartArr;
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }

       
        
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

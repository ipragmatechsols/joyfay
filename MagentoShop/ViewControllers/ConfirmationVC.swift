//
//  ConfirmationVC.swift
//  MagentoShop
//
//  Created by Bunty on 27/06/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import MGNetworkClient
import MagentoHud
import MagentoClient
import PassKit

class ConfirmationVC: BaseVc,PayPalPaymentDelegate , AuthNetDelegate{
    
    
    let SupportedPaymentNetworks = [PKPaymentNetworkVisa, PKPaymentNetworkMasterCard, PKPaymentNetworkAmex]
    let ApplePaySwagMerchantID = "merchant.com.sandbox.joyfay"

    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet var headerBottomLine: UILabel!
    @IBOutlet var confirmBcg: UIView!
    @IBOutlet var confirmBlackOverLay: UIView!
    @IBOutlet var shippingCostNameLbl: UILabel!
    @IBOutlet var shippingCostValueLbl: UILabel!
    @IBOutlet var voucherNameLbl: UILabel!
    @IBOutlet var voucherValueLbl: UILabel!
    
    @IBOutlet weak var applePayBtn: UIButton!
    @IBOutlet var previousBtn: UIButton!
    var shippingInfo:ShippingInfo?
    var totolAMount:String?
    var shippingAddress:String?
    var paymentMethod:String?
    var cartArr:[CartItem]?
    var environment:String = PayPalEnvironmentNoNetwork {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnectWithEnvironment(newEnvironment)
            }
        }
    }
    
    #if HAS_CARDIO
    // You should use the PayPal-iOS-SDK+card-Sample-App target to enable this setting.
    // For your apps, you will need to link to the libCardIO and dependent libraries. Please read the README.md
    // for more details.
    var acceptCreditCards: Bool = true {
    didSet {
    payPalConfig.acceptCreditCards = acceptCreditCards
    }
    }
    #else
    var acceptCreditCards: Bool = false {
        didSet {
            payPalConfig.acceptCreditCards = acceptCreditCards
        }
    }
    #endif
    
    var resultText = "" // empty
    var payPalConfig = PayPalConfiguration() // default
    
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var totalAmtLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var paymentMethodLbl: UILabel!
    @IBAction func payThroughApplePay(sender: AnyObject) {
        
        if PKPaymentAuthorizationViewController.canMakePayments() == false {
            
            [self.showNormalAlert("Apple Pay is not available")];
            return;
            
        }
        
        if PKPaymentAuthorizationViewController.canMakePaymentsUsingNetworks(SupportedPaymentNetworks) == false {
            
            [self.showNormalAlert("No Apple Pay payment methods available")];
            return;
            
        }
        
        
        let request = PKPaymentRequest()
        request.merchantIdentifier = ApplePaySwagMerchantID
        request.supportedNetworks = SupportedPaymentNetworks
        request.merchantCapabilities = PKMerchantCapability.Capability3DS
        request.countryCode = "US"
        request.currencyCode = "USD"
        let str  = self.totalAmtLbl.text?.stringByReplacingOccurrencesOfString("$", withString: "");
     let amount =  NSDecimalNumber(string: str)
        request.paymentSummaryItems = [
            PKPaymentSummaryItem(label: "JoyFay Cart ", amount: amount)
          //  PKPaymentSummaryItem(label: "Razeware", amount: 120)
        ]
        
        let applePayController = PKPaymentAuthorizationViewController(paymentRequest: request)
        applePayController.delegate = self;
        self.presentViewController(applePayController, animated: true, completion: nil)
    }
    @IBAction func EditShippingAddress(sender: AnyObject) {
        var poppingvc:AddressListVc?
        
        for vc in (self.navigationController?.viewControllers)! {
            if vc.isKindOfClass(AddressListVc.classForCoder())
            {
                poppingvc = vc as? AddressListVc;
                break;
            }
        }
        if let p = poppingvc {
            self.navigationController?.popToViewController(p, animated: true)
        }
    }
    @IBAction func editPaymentMethodAction(sender: AnyObject) {
        var poppingvc:PaymentVC?
        
        for vc in (self.navigationController?.viewControllers)! {
            if vc.isKindOfClass(PaymentVC.classForCoder())
            {
                poppingvc = vc as? PaymentVC;
                break;
            }
        }
        if let p = poppingvc {
           self.navigationController?.popToViewController(p, animated: true)
        }
       
    }
    @IBAction func previousBtnAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true);
    }
    
    @IBAction func saveBtnAction(sender: AnyObject) {
        
        if paymentMethod == "joyfaypay" {
            self.makeCODPayment();
        }
        else if(paymentMethod == "CHECKMO")
        {
            self.makeCheckMOPayment();
        }
        else
        {
            self.buyClothingAction()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.title = "CONFIRMATION"
        self.configureViewForCheckMO();
        if paymentMethod == "PayPal(PBP)" {
            self.configurePayPal();
        }
        
        if paymentMethod == "Apple Pay" {
            self.configureApplePay();
        }
        
        // Do any additional setup after loading the view.
      //  self.getTotalAmount();
    }
    
    func configureApplePay()  {
        self.applePayBtn.hidden = false;
        self.bottomView.hidden =  true;
//        self.confirmBcg.hidden = true;
//        self.confirmBlackOverLay.hidden = true;
//        self.confirmBtn.hidden = true;
        
    }
    
    func configureView()  {
        self.headerBottomLine.backgroundColor  = Theme.theme().themeColor!
        self.previousBtn.backgroundColor = Theme.theme().themeColor!
        self.confirmBcg.backgroundColor  = Theme.theme().themeColor!
        self.amountLbl.textColor = Theme.theme().themeColor!;
        self.totalAmtLbl.textColor = Theme.theme().themeColor!
        self.amountLbl.text = "$" + self.totolAMount!;
        self.totalAmtLbl.text = "$" + self.totolAMount!;
        self.addressLbl.text = self.shippingAddress;
        
        //vc.paymentMethod = "Cash On Delivery (COD)"
        
        
        
    }
    func configureViewForCheckMO()  {
        self.headerBottomLine.backgroundColor  = Theme.theme().themeColor!
        self.previousBtn.backgroundColor = Theme.theme().themeColor!
        self.confirmBcg.backgroundColor  = Theme.theme().themeColor!
        self.amountLbl.textColor = Theme.theme().themeColor!;
        self.totalAmtLbl.textColor = Theme.theme().themeColor!
        
        if let info = shippingInfo {
            
            for segment:ShippingSegment in info.total_segments! {
                if segment.code == "subtotal" {
                    self.amountLbl.text = "$\(segment.value!)";
                }
                else if segment.code == "shipping"
                {
                    self.shippingCostNameLbl.text = segment.title;
                    shippingCostNameLbl.adjustsFontSizeToFitWidth = true
                    self.shippingCostValueLbl.text = "$\(segment.value!)";
                }
                else if segment.code == "Tax"
                {
                    self.voucherNameLbl.text = segment.title;
                    self.voucherValueLbl.text = "$\(segment.value!)";
                }
                else if segment.code == "grand_total"
                {
                    //self.totalAmtLbl.text = segment.title;
                    self.totalAmtLbl.text = "$\(segment.value!)";
                }
            }
            self.paymentMethodLbl.text = self.paymentMethod;
             self.addressLbl.text = self.shippingAddress;
        }
        else
        {
        self.amountLbl.text = "$" + self.totolAMount!;
        self.totalAmtLbl.text = "$" + "\(Float(self.totolAMount!)!+5.0)";
        self.addressLbl.text = self.shippingAddress;
        self.shippingCostValueLbl.text = "$5"
        self.paymentMethodLbl.text = self.paymentMethod;
             self.addressLbl.text = self.shippingAddress;
        }
        
        
        if(paymentMethod == "joyfaypay"){
            self.paymentMethodLbl.text = "Cash On Delivery (COD)";
        }else{
            self.paymentMethodLbl.text = self.paymentMethod;
        }
        
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if paymentMethod == "PayPal(PBP)" {
        PayPalMobile.preconnectWithEnvironment(environment)
        }
        
    }

    func configurePayPal()  {
        // Set up payPalConfig
        payPalConfig.acceptCreditCards = acceptCreditCards;
        payPalConfig.merchantName = "iPragmatech Pvt ltd.."
        payPalConfig.merchantPrivacyPolicyURL = NSURL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
        payPalConfig.merchantUserAgreementURL = NSURL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        
        // Setting the languageOrLocale property is optional.
        //
        // If you do not set languageOrLocale, then the PayPalPaymentViewController will present
        // its user interface according to the device's current language setting.
        //
        // Setting languageOrLocale to a particular language (e.g., @"es" for Spanish) or
        // locale (e.g., @"es_MX" for Mexican Spanish) forces the PayPalPaymentViewController
        // to use that language/locale.
        //
        // For full details, including a list of available languages and locales, see PayPalPaymentViewController.h.
        
        payPalConfig.languageOrLocale = NSLocale.preferredLanguages()[0]
        
        // Setting the payPalShippingAddressOption property is optional.
        //
        // See PayPalConfiguration.h for details.
        
        payPalConfig.payPalShippingAddressOption = .PayPal;
        
        print("PayPal iOS SDK Version: \(PayPalMobile.libraryVersion())")
    }
    
   
    
    // PayPalPaymentDelegate
    
    func payPalPaymentDidCancel(paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        self.resultText = "PayPal Payment Cancelled"
          MGNetworkClient.sharedClient.showErrorALertWithMessage(self.resultText);
        paymentViewController.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func payPalPaymentViewController(paymentViewController: PayPalPaymentViewController, didCompletePayment completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        paymentViewController.dismissViewControllerAnimated(true, completion: { () -> Void in
            // send completed confirmaion to your server
            print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            
            self.resultText = completedPayment.description
            //MGNetworkClient.sharedClient.showSuccessALertWithMessage(self.resultText);
            self.makePaypalPayment();
          
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getTotalAmount()  {
        MagentoHud.show();
        MGNetworkClient.sharedClient.getTotalAmountToCheckout(nil) { (er:NSError?, ad:Addres?) in
            MagentoHud.dismiss();
           

        }
    }
    func makePaypalPayment()  {
        MagentoHud.show();
        let params = Payment().getParamsWithPaymentMethod("ippaypal")
        MGNetworkClient.sharedClient.makePayment(params) { (er:NSError?, orderID:String?) in
            MagentoHud.dismiss();
            if er == nil
            {
                MGNetworkClient.sharedClient.removeCartID();
                MGNetworkClient.sharedClient.removeProductDict();
                MGNetworkClient.sharedClient.showSuccessALertWithMessage("Your order is placed successfully");
                let vc:HomeVc = self.storyboard?.instantiateViewControllerWithIdentifier("HomeVc") as! HomeVc;
                self.navigationController?.setViewControllers([vc], animated: true);
            }
        }
    }
    func makeAuthorizeNetPayment()  {
        MagentoHud.show();
        let params = Payment().getParamsWithPaymentMethod("authorizenet_directpost")
        MGNetworkClient.sharedClient.makePayment(params) { (er:NSError?, orderID:String?) in
            MagentoHud.dismiss();
            if er == nil
            {
                MGNetworkClient.sharedClient.removeCartID();
                MGNetworkClient.sharedClient.removeProductDict();
                MGNetworkClient.sharedClient.showSuccessALertWithMessage("Your order is placed successfully");
                let vc:HomeVc = self.storyboard?.instantiateViewControllerWithIdentifier("HomeVc") as! HomeVc;
                self.navigationController?.setViewControllers([vc], animated: true);
            }
        }
    }
    func makeCODPayment()  {
        MagentoHud.show();
        let params = Payment().getParamsWithPaymentMethod("joyfaypay")
        MGNetworkClient.sharedClient.makePayment(params) { (er:NSError?, orderID:String?) in
         MagentoHud.dismiss();
            if er == nil
            {
                MGNetworkClient.sharedClient.removeCartID();
                 MGNetworkClient.sharedClient.removeProductDict();
                MGNetworkClient.sharedClient.showSuccessALertWithMessage("Your order is placed successfully");
                let vc:HomeVc = self.storyboard?.instantiateViewControllerWithIdentifier("HomeVc") as! HomeVc;
                self.navigationController?.setViewControllers([vc], animated: true);
            }
        }
    }
    func makeCheckMOPayment()  {
        MagentoHud.show();
        let params = Payment().getParamsWithPaymentMethod("checkmo")
        MGNetworkClient.sharedClient.makePayment(params) { (er:NSError?, orderID:String?) in
            MagentoHud.dismiss();
            if er == nil
            {
                MGNetworkClient.sharedClient.removeCartID();
                MGNetworkClient.sharedClient.removeProductDict();
                MGNetworkClient.sharedClient.showSuccessALertWithMessage("Your order is placed successfully");
                let vc:HomeVc = self.storyboard?.instantiateViewControllerWithIdentifier("HomeVc") as! HomeVc;
                self.navigationController?.setViewControllers([vc], animated: true);
            }
            
        }
    }
     func buyClothingAction() {
        // Remove our last completed payment, just for demo purposes.
        resultText = ""
        
        // Note: For purposes of illustration, this example shows a payment that includes
        //       both payment details (subtotal, shipping, tax) and multiple items.
        //       You would only specify these if appropriate to your situation.
        //       Otherwise, you can leave payment.items and/or payment.paymentDetails nil,
        //       and simply set payment.amount to your total charge.
        
        // Optional: include multiple items
        if let c = self.cartArr
        {
            if self.cartArr?.count > 0
            {
               
                
                self.cartArr = c;
                var items = [PayPalItem]()
                for  item:CartItem in self.cartArr!
                {
                  let item1 = PayPalItem(name: (item.product?.name!)!, withQuantity: UInt(item.qty!), withPrice: NSDecimalNumber(string: "\(item.price ?? 0)"), withCurrency: "USD", withSku: item.product?.sku!)
                    items.append(item1);
                    
                }
                
             let subtotal = PayPalItem.totalPriceForItems(items)
                let shipping = NSDecimalNumber(string: "0")
                let tax = NSDecimalNumber(string: "0")
                let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
                let total = subtotal.decimalNumberByAdding(shipping).decimalNumberByAdding(tax)
                
                let payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: "Magento Clothing", intent: .Sale)
                payment.items = items
                payment.paymentDetails = paymentDetails
                
                if (payment.processable) {
                    let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
                    presentViewController(paymentViewController!, animated: true, completion: nil)
                }
                else {
                    // This particular payment will always be processable. If, for
                    // example, the amount was negative or the shortDescription was
                    // empty, this payment wouldn't be processable, and you'd want
                    // to handle that here.
                    print("Payment not processalbe: \(payment)")
                      MGNetworkClient.sharedClient.showErrorALertWithMessage("Payment not processalbe: \(payment)");
                    
                }

            }
            
        }
       
    }
    
        func createTransaction(payData:PKPayment)
        {
            
            let an = AuthNet(environment: ENV_TEST);
            an.delegate = self;
            let request = CreateTransactionRequest();
            let requestType = TransactionRequestType.transactionRequest();
            request.transactionRequest = requestType;
            request.transactionType = AUTH_ONLY;
            let fpData = FingerPrintObjectType();
            fpData.hashValue = "9cdc5e235856bdd62b2b15317ef0a727";
            fpData.sequenceNumber = 33763170;
            fpData.timeStamp = 1479119344;
            request.anetApiRequest.merchantAuthentication.name = "23E7uTk5";
            request.anetApiRequest.merchantAuthentication.transactionKey = "9DUV2jR5Ux43ap3u";
            
            let opData = OpaqueDataType();
            opData.dataValue = self.base64forData(payData.token.paymentData)  ;
            opData.dataDescriptor = "COMMON.APPLE.INAPP.PAYMENT";
            let paymentType = PaymentType();
            paymentType.creditCard = nil;
            paymentType.bankAccount = nil;
            paymentType.trackData = nil;
            paymentType.swiperData = nil;
            paymentType.opaqueData = opData;
            let str  = self.totalAmtLbl.text?.stringByReplacingOccurrencesOfString("$", withString: "");
            requestType.amount = str;
            requestType.payment = paymentType;
            //  requestType.retail.marketType = "0";
            //requestType.retail.deviceType = "7";
            
            let order = OrderType.order()
            order.invoiceNumber = "\( arc4random() % 100)";
            an.purchaseWithRequest(request);
        }
        
        
        func getTransactionDetailsSucceeded(response: GetTransactionDetailsResponse!) {
            MGNetworkClient.sharedClient.showSuccessALertWithMessage(response.description)
        }
        
        func paymentSucceeded(response: CreateTransactionResponse) -> () {
            
            self.makeAuthorizeNetPayment();
        }
        
        
        func requestFailed(response: AuthNetResponse) -> () {
            MGNetworkClient.sharedClient.showErrorALertWithMessage("Payment not processable: \(response.responseReasonText)");
        }
        
        func connectionFailed(response: AuthNetResponse) -> () {
             MGNetworkClient.sharedClient.showErrorALertWithMessage("Payment not processable: \(response.responseReasonText)");
            
        }
        func emvPaymentSucceeded(response:AnetEMVTransactionResponse )-> ()
        {
            
            
        }
        
        
        func base64forData(theData: NSData) -> String {
            let charSet = NSCharacterSet.URLQueryAllowedCharacterSet()
            let paymentString = NSString(data: theData, encoding: NSUTF8StringEncoding)!.stringByAddingPercentEncodingWithAllowedCharacters(charSet);
            // let paymentString = NSString(data: theData, encoding: String.Encoding.utf8.rawValue)!.addingPercentEncoding(withAllowedCharacters: charSet)
            
            return paymentString!
        }
       
        
        
        
       
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


extension ConfirmationVC:PKPaymentAuthorizationViewControllerDelegate {
    func paymentAuthorizationViewController(controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: ((PKPaymentAuthorizationStatus) -> Void)) {
        self.createTransaction(payment);
        completion(PKPaymentAuthorizationStatus.Success)
    }
    
    func paymentAuthorizationViewControllerDidFinish(controller: PKPaymentAuthorizationViewController) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}

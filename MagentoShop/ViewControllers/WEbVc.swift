//
//  WEbVc.swift
//  MagentoShop
//
//  Created by vikaskumar on 19/12/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import MagentoHud
import MGNetworkClient

public class WEbVc: BaseVc,UIWebViewDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    public var from:String?
    override public func viewDidLoad() {
        super.viewDidLoad()
       // Do any additional setup after loading the view.
          var url = NSURL?()
        self.title = from;
        switch from! {
        case "About Us":
             url = NSURL (string: "\(MGNetworkClient.host)/about-us");
        case "Help":
            url = NSURL (string: "\(MGNetworkClient.host)/service");
            
        default:
            url = NSURL (string: "\(MGNetworkClient.host)/service");
            
        }
        
        let requestObj = NSURLRequest(URL: url!);
        webView.delegate = self;
        webView.loadRequest(requestObj);
    }
    
   
  public func webViewDidStartLoad(webView: UIWebView)
    {
        MagentoHud.show()
    
    }
     public func webViewDidFinishLoad(webView: UIWebView)
     {
    MagentoHud.dismiss()
    }
   
     public func webView(webView: UIWebView, didFailLoadWithError error: NSError?)
     {
     MagentoHud.dismiss()
    }
}

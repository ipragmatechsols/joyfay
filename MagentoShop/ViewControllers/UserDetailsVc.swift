//
//  UserDetailsVc.swift
//  MagentoShop
//
//  Created by vikaskumar on 12/10/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import SwiftForms
import MGNetworkClient
import MagentoHud



class UserDetailsVc: FormViewController {

    var userDetails:UserDetails? = nil
   var firstnameRow :FormRowDescriptor?
    var lastnameRow :FormRowDescriptor?
    var emaileRow :FormRowDescriptor?
    var telephoneRow :FormRowDescriptor?
    var cityRow :FormRowDescriptor?
    var postcodeRow :FormRowDescriptor?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureForm();
        self.getUserDetails()
        self.title = "User Details"

        // Do any additional setup after loading the view.
    }
    func configureForm()  {
        let form = FormDescriptor()
        form.title = "User Details"
        
        
        
        // Define first section
        let section1 = FormSectionDescriptor(headerTitle: "User Acount",footerTitle: "")
        
       firstnameRow = FormRowDescriptor(tag: "firstName", type: .Text, title: "FirstName: ")
        firstnameRow!.configuration.cell.appearance = ["textField.placeholder" : "John" as AnyObject, "textField.textAlignment" : NSTextAlignment.Right.rawValue as AnyObject]

        
        section1.rows.append(firstnameRow!)
        
        lastnameRow = FormRowDescriptor(tag: "lasttName", type: .Text, title: "LastName: ")
        lastnameRow!.configuration.cell.appearance = ["textField.placeholder" : "Abhrahim" as AnyObject, "textField.textAlignment" : NSTextAlignment.Right.rawValue as AnyObject]
        section1.rows.append(lastnameRow!)
        
        emaileRow = FormRowDescriptor(tag: "email", type: .Email, title: "Email: ")
        emaileRow!.configuration.cell.appearance = ["textField.placeholder" : "john@gmail.com" as AnyObject, "textField.textAlignment" : NSTextAlignment.Right.rawValue as AnyObject]
        section1.rows.append(emaileRow!)
        
        
        
        // Define second section
      
       let section2 = FormSectionDescriptor(headerTitle: "Address",footerTitle: "")
       
        
       telephoneRow = FormRowDescriptor(tag: "telephone", type: .Phone, title: "Telephone: ")
        telephoneRow!.configuration.cell.appearance = ["textField.placeholder" : "" as AnyObject, "textField.textAlignment" : NSTextAlignment.Right.rawValue as AnyObject]
        section2.rows.append(telephoneRow!)
        
        cityRow = FormRowDescriptor(tag: "city", type: .Text, title: "City: ")
        cityRow!.configuration.cell.appearance = ["textField.placeholder" : "" as AnyObject, "textField.textAlignment" : NSTextAlignment.Right.rawValue as AnyObject]
        section2.rows.append(cityRow!)
        
      postcodeRow = FormRowDescriptor(tag: "postcode", type: .Number, title: "PostCode: ")
        postcodeRow!.configuration.cell.appearance = ["textField.placeholder" : "" as AnyObject, "textField.textAlignment" : NSTextAlignment.Right.rawValue as AnyObject]
        section2.rows.append(postcodeRow!)

        
        
         let section3 = FormSectionDescriptor(headerTitle: "Password",footerTitle: "")
        
        let passrow = FormRowDescriptor(tag: "password", type: .Button, title: "Change Password: ")
        passrow.configuration.button.didSelectClosure = { _ in
            let vc = ChangePasswordVc();
            self.navigationController?.pushViewController(vc, animated: true);
        }
       
        section3.rows.append(passrow)
        
        let section4 = FormSectionDescriptor(headerTitle: "Update",footerTitle: "")
        
        let updaterow = FormRowDescriptor(tag: "update", type: .Button, title: "Update")
        updaterow.configuration.button.didSelectClosure = { _ in
            self.saveUserDetails();
        }

        
        section4.rows.append(updaterow)

        
        
        form.sections = [section1, section2,section3,section4]
        
        self.form = form

        

    }
    
    func fillRowsWithUserDetails(userDetails:UserDetails?)  {
        if let user = userDetails {
         
            
          
            self.setValue(user.firstname!, forTag: "firstName");
            self.setValue(user.lastname!, forTag: "lasttName")
            self.setValue(user.email!, forTag: "email")
            self.setValue(user.telephone!, forTag: "telephone")
            self.setValue(user.city!, forTag: "city")
            self.setValue(user.postcode!, forTag: "postcode")
            
//            firstnameRow?.value = "dsfsdf";
//            lastnameRow?.value = user.lastname;
//            emaileRow?.value = user.email;
//            telephoneRow?.value = user.telephone;
//            cityRow?.value = user.city;
//            postcodeRow?.value = user.postcode;
            
            
        }
    }
    
    func getUserDetailsFromForm() ->UserDetails  {
        
        let user = UserDetails();
           user.firstname =  self.valueForTag("firstName") as? String;
         user.lastname =  self.valueForTag("lasttName") as? String;
         user.email =  self.valueForTag("email") as? String;
         user.telephone =  self.valueForTag("telephone") as? String;
         user.city =  self.valueForTag("city") as? String;
         user.postcode =  self.valueForTag("postcode") as? String ;
       
        
        return user;
        
        
    }

    func getUserDetails()  {
        MagentoHud.show()
        MGNetworkClient.sharedClient.getUserDetails(nil) { (er:NSError?, userDetails:UserDetails?) in
            MagentoHud.dismiss()
            if er == nil
            {
                self.userDetails = userDetails;
                dispatch_async(dispatch_get_main_queue(),{
                    
                  self.fillRowsWithUserDetails(self.userDetails);
                })
                
            }
            
            
        }
    }
    func saveUserDetails()  {
        MagentoHud.show()
    let params =    self.getParamsForUser()
        MGNetworkClient.sharedClient.saveUserDetails(params) { (er:NSError?, userDetails:UserDetails?) in
            MagentoHud.dismiss()
            if er == nil
            {
                self.userDetails = userDetails;
                dispatch_async(dispatch_get_main_queue(),{
                    
                    self.fillRowsWithUserDetails(self.userDetails);
                })
                
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func getParamsForUser()  -> [String:AnyObject]?
    {
        let user = self.getUserDetailsFromForm();
        var finalparams =  [String:AnyObject]();
        let params =  NSMutableDictionary();
        params["email"] = user.email;
        params["firstname"] = user.firstname;
        params["lastname"] = user.lastname;
        params["website_id"] = user.website_id;
        finalparams["customer"] = params
        return finalparams;
    }
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

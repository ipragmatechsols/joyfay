//
//  MyMenuTableViewController.swift
//  SwiftSideMenu
//
//  Created by Evgeny Nazarov on 29.09.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

import UIKit
import MGNetworkClient
import MagentoClient
protocol MyMenuTableViewControllerDelegate
{
    func countIsCleared();
    
}
 class MyMenuTableViewController: UITableViewController,SideMenuTopCellDelegate {
    var selectedMenuItem : Int = 0
    var category:SideCategory? = nil
     var selectedCategory:SideCategory? = nil
    var selectedCatArr = [SideCategory]();
      var delegate:MyMenuTableViewControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Customize apperance of table view
        tableView.contentInset = UIEdgeInsetsMake(64.0, 0, 0, 0) //
        tableView.separatorStyle = .None
        tableView.backgroundColor = UIColor.whiteColor()
        tableView.scrollsToTop = false
        
        // Preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false
        
//        tableView.selectRowAtIndexPath(NSIndexPath(forRow: selectedMenuItem, inSection: 0), animated: false, scrollPosition: .Middle)
        self .fetchCategories();
    }
    @IBAction func userDetailAction(sender: UIButton) {
        
//        let vc = UserDetailsVc()
//        let navC:UINavigationController = sideMenuController() as! UINavigationController;
//        navC.pushViewController(vc, animated: true)
//        toggleSideMenuView()

    }
    func reloadTable(completion: ()->())
    {
        self.tableView.reloadData(completion);
    }
    @IBAction func backInMenu(sender: AnyObject) {
        
        if self.selectedCatArr.count > 1 {
            self.selectedCatArr.removeAtIndex(self.selectedCatArr.count - 1)
            self.selectedCategory = self.selectedCatArr.last;
            self.tableView.reloadData();
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func fetchCategories()  {
        MGNetworkClient.sharedClient.getCategories { (error:NSError?, cat:SideCategory?) in
         
            if let c = cat
            {
                self.category = c;
                self.selectedCategory = self.category;
                self.selectedCatArr.append(self.selectedCategory!);
                self.tableView.reloadData()
            }
            
        }
    }
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        if self.selectedCategory != nil {
            return (self.selectedCategory?.children_data.count)! + 1;
        }
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        if section == 0 {
             return 0
        }
       else if self.selectedCategory != nil {
            let cat =  self.selectedCategory?.children_data[section - 1];
            if cat?.children_data.count > 0 {
                 return (cat?.children_data.count)!;
            }
            else
            {
                return 1;
            }
           
        }
        return 0
    }
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
    
        if section == 0 {
            if self.selectedCatArr.count > 1 {
                 return 120
            }
            else
            {
                return 80;
            }
            
        }
        
        let cat =  self.selectedCategory?.children_data[section - 1];
        if cat?.children_data.count > 0 {
            return 30;
        }
        else
        {
            return 0;
        }
        
        //return 30;
    }
    
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
        let  headerCell = tableView.dequeueReusableCellWithIdentifier("SideMenuTopCell") as! SideMenuTopCell
            headerCell.delegate = self;
            headerCell.nameLbl.textColor = Theme.theme().themeColor
            if (MGNetworkClient.sharedClient.authToken() != nil) {
                headerCell.logBtn.selected = true;
                headerCell.orderBtn.hidden = false;
                if  User.fetchSavedUser().firstname != nil &&  User.fetchSavedUser().lastname != nil
                {
                 headerCell.nameLbl.text = User.fetchSavedUser().firstname! + " " + User.fetchSavedUser().lastname!;
                }
                else if  User.fetchSavedUser().email != nil {
                    let strARr = User.fetchSavedUser().email!.componentsSeparatedByString("@");
                    headerCell.nameLbl.text = strARr[0];
                }
                
            }
            else
            {
                 headerCell.orderBtn.hidden = true;
             headerCell.logBtn.selected = false;
            }
//        headerCell.backgroundColor = UIColor.cyanColor()
//        
//        switch (section) {
//        case 0:
//            headerCell.headerLabel.text = "Europe";
//        //return sectionHeaderView
//        case 1:
//            headerCell.headerLabel.text = "Asia";
//        //return sectionHeaderView
//        case 2:
//            headerCell.headerLabel.text = "South America";
//        //return sectionHeaderView
//        default:
//            headerCell.headerLabel.text = "Other";
//        }
        
        return headerCell
        }
        else
        {
            var cell = tableView.dequeueReusableCellWithIdentifier("HeaderCELL")
            
            if (cell == nil) {
                cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "HeaderCELL")
              
                cell!.backgroundColor = UIColor.whiteColor()
                cell!.textLabel?.textColor = UIColor.darkGrayColor()
                cell?.textLabel!.font =   UIFont.boldSystemFontOfSize(15)
                cell?.textLabel!.textColor = Theme.theme().themeColor!;
                let selectedBackgroundView = UIView(frame: CGRectMake(0, 0, cell!.frame.size.width, cell!.frame.size.height))
                selectedBackgroundView.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.2)
                cell!.selectedBackgroundView = selectedBackgroundView
            }
            if self.selectedCategory != nil {
                let cat =  self.selectedCategory?.children_data[section - 1];
              //  let subcat = cat?.children_data?[indexPath.row]
                cell!.textLabel?.text =  cat?.name;
            }
            return cell;
        }
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("CELL")
        
        if (cell == nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "CELL")
            cell!.backgroundColor = UIColor.clearColor()
            cell!.textLabel?.textColor = UIColor.darkGrayColor()
                  cell!.textLabel?.font =   cell!.textLabel?.font.fontWithSize(12)
            let selectedBackgroundView = UIView(frame: CGRectMake(0, 0, cell!.frame.size.width, cell!.frame.size.height))
            selectedBackgroundView.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.2)
            cell!.selectedBackgroundView = selectedBackgroundView
        }
        if indexPath.section > 0 {
            
        
        if self.selectedCategory != nil {
            let cat =  self.selectedCategory?.children_data[indexPath.section - 1];
           
            if cat?.children_data.count > 0 {
                let subcat = cat?.children_data[indexPath.row]
                 cell!.textLabel?.text =  subcat?.name;
            }
            else
            {
                cell!.textLabel?.text =  cat?.name;
            }
           
        }
            else
        {

        cell!.textLabel?.text = "ViewController #\(indexPath.row+1)"
            }
        }
        cell!.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        return cell!
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 30.0
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if self.selectedCategory != nil {
            let cat =  self.selectedCategory?.children_data[indexPath.section - 1];
            
            if cat?.children_data.count > 0 {
                let subcat = cat?.children_data[indexPath.row]
                if subcat?.children_data.count > 0  {
                    selectedCategory = subcat;
                    self.selectedCatArr.append(selectedCategory!);
                    tableView.reloadData();
                }
                else
                {
                    let vc:ProductListVc = self.storyboard?.instantiateViewControllerWithIdentifier("ProductListVc") as!ProductListVc
                    vc.catID = subcat?.id;
                    vc.titleName = subcat?.name;
                    let navC:UINavigationController = sideMenuController() as! UINavigationController;
                     navC.pushViewController(vc, animated: true);
                     toggleSideMenuView() 
                   // self.navigationController?.pushViewController(vc, animated: true);
                   // sideMenuController()?.setContentViewController(vc)
                }
               
               // cell!.textLabel?.text =  subcat?.name;
            }
            else
            {
               // cell!.textLabel?.text =  cat?.name;
                let vc:ProductListVc = self.storyboard?.instantiateViewControllerWithIdentifier("ProductListVc") as!ProductListVc
                vc.catID = cat?.id;
                 vc.titleName = cat?.name;
                let navC:UINavigationController = sideMenuController() as! UINavigationController;
                navC.pushViewController(vc, animated: true);
                 toggleSideMenuView()
                //sideMenuController()?.setContentViewController(vc)
            }
            
        }
        
    }
    
    func orderBtnTapped()
    {
    
        let vc:OrdersVc = self.storyboard?.instantiateViewControllerWithIdentifier("OrdersVc") as!OrdersVc;
         let navC:UINavigationController = sideMenuController() as! UINavigationController;
       navC.pushViewController(vc, animated: true)
         toggleSideMenuView()
        
    }
    func logBtnTapped(sender:UIButton!)
    {
        if MGNetworkClient.sharedClient.authToken() != nil {
            
           
            
            let refreshAlert = UIAlertController(title: "LogOut", message: "Are you sure you want to logout.", preferredStyle: UIAlertControllerStyle.Alert)
//            if let popoverController = refreshAlert.popoverPresentationController {
//                popoverController.sourceView = sender
//                popoverController.sourceRect = sender.bounds
//            }
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                //print("Handle Ok logic here")
                if (MGNetworkClient.sharedClient.authToken() != nil) {
                    MGNetworkClient.sharedClient.removeAuthToken()
                    
                }
                if (MGNetworkClient.sharedClient.cartID() != nil) {
                    MGNetworkClient.sharedClient.removeCartID()
                    
                }
                if (MGNetworkClient.sharedClient.getCartCount() != "") {
                    MGNetworkClient.sharedClient.removeCartCount()
                    self.delegate?.countIsCleared();
                    
                }

                
                self.tableView.reloadData();
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { (action: UIAlertAction!) in
               // print("Handle Cancel Logic here")
            }))
               presentViewController(refreshAlert, animated: true, completion: nil)
            return;
        }
        
       
     
        if (MGNetworkClient.sharedClient.authToken() != nil) {
            MGNetworkClient.sharedClient.removeAuthToken()
           
        }
        if (MGNetworkClient.sharedClient.cartID() != nil) {
             MGNetworkClient.sharedClient.removeCartID()
            
        }
        if (MGNetworkClient.sharedClient.getCartCount() != "") {
            MGNetworkClient.sharedClient.removeCartCount()
            
        }
        
      
        let vc:LoginVC = self.storyboard?.instantiateViewControllerWithIdentifier("LoginVC") as! LoginVC;
        let navigationController = UINavigationController.init(rootViewController: vc);
        AppDelegate.appDelegate.navigationController?.presentViewController(navigationController, animated: true, completion: {
            
        })
        
    }

    
//        print("did select row: \(indexPath.row)")
//        
//        if (indexPath.row == selectedMenuItem) {
//            return
//        }
//        
//        selectedMenuItem = indexPath.row
//        
//        //Present new view controller
//        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
//        var destViewController : UIViewController
//        switch (indexPath.row) {
//        case 0:
//            destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("ViewController1") 
//            break
//        case 1:
//            destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("ViewController2")
//            break
//        case 2:
//            destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("ViewController3")
//            break
//        default:
//            destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("ViewController4") 
//            break
//        }
//        sideMenuController()?.setContentViewController(destViewController)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
extension UITableView {
    func reloadData(completion: ()->()) {
        self.reloadData()
        dispatch_async(dispatch_get_main_queue()) {
            completion()
        }
    }
}

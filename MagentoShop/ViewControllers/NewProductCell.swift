//
//  NewProductCell.swift
//  MagentoShop
//
//  Created by Bunty on 18/08/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import MagentoClient

class NewProductCell: UITableViewCell {

    @IBOutlet var newProductLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        newProductLbl.textColor = Theme.theme().themeColor
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  ProductListVc.swift
//  MagentoShop
//
//  Created by Bunty on 08/07/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import MGNetworkClient;
import MagentoHud
import PopupController
import MagentoClient


class ProductListVc: BaseVc,UITableViewDataSource,UITableViewDelegate,ProductListCellDelegate,UIScrollViewDelegate ,UISearchBarDelegate{

    @IBOutlet var sortBtn: UIButton!
    @IBOutlet var filterBtn: UIButton!
    @IBOutlet var filterheightConstraint: NSLayoutConstraint!
    @IBOutlet var emptyIMage: UIImageView!
    @IBOutlet var tableView: UITableView!
    var fromSearchBar:Bool?
     var searchText:String?
     let searchBar = UISearchBar()
    var catID:Int?
    var catIDs = [Int]()
    var titleName:String?
    var productsArr = [Product]()
    var fetchedProductsArr = [Product]()
    var calculatedRows:Int?
    var heightDictionary:[Int:CGFloat]?
    var popOverISOpened = false;
    var popOverDismissed: ((Int,Int) -> (Void))?
    var fiterpopOverDismissed: ((Int,Filter) -> (Void))?
    var sortPopUp:PopupController?
    var filterPopUp:PopupController?
     var cartButton: ENMBadgedBarButtonItem!
    var wishListButton: ENMBadgedBarButtonItem!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.contentInset = UIEdgeInsetsMake(40, 0, 0, 0)
        self.configureView();
         self.setUpRightBarButton();
        
        if (fromSearchBar != nil) && fromSearchBar == true {
            self.fetchProductsForSearch(self.searchText);
        }
        else
        {
        
        if self.catID == nil{
           
            self.tableView.hidden = true ;

        }
        else
        {
           self.fetchProducts();
        }
        
        }
        
        if self.titleName != nil {
            self.title = titleName!;
            
        }          // Do any additional setup after loading the view.
        
        self.popOverDismissed = ({(type:Int,index:Int) -> (Void) in
            
            
                if type == 0
                {
                    self.sortPopUp?.dismiss()

                    switch index {
                    case 0:
                         self.productsArr.sortInPlace({ $0.price > $1.price })
                    case 1:
                        self.productsArr.sortInPlace({ $0.price < $1.price })

                    case 2:
                        self.productsArr.sortInPlace({ $0.name < $1.name })

                    case 3:
                        self.productsArr.sortInPlace({ $0.name > $1.name })
                    case 4:break

                    default:
                        break
                    }
               
                self.tableView.reloadData()
                }
            
        })
        
        
        self.fiterpopOverDismissed = ({(type:Int, index:Filter) -> (Void) in
            self.filterPopUp?.dismiss()
            if type == 8 {
                return;
            }
            else if type == 9
            {
                self.productsArr = [Product]()
                
                for p:Product in self.fetchedProductsArr
                {
                    self.productsArr.append(p)
                }
                self.tableView.reloadData()
                return;
            }
            self.updateViewAccrodingToFilter(index);
            
        })
        
        
        
       
        // searchBar.barTintColor = Theme.theme().themeColor!
        // searchBar.tintColor = Theme.theme().themeColor!
        // searchBar.backgroundColor = Theme.theme().themeColor!
        searchBar.placeholder = "Search Product"
        searchBar.sizeToFit()
        searchBar.delegate = self;
        navigationItem.titleView = searchBar
    }
    
    
    
    
    
    //MARK: Search Bar Delegate
    func searchBarSearchButtonClicked(searchBar: UISearchBar)
    {
        searchBar.resignFirstResponder()
        if searchBar.text?.characters.count > 0 {
             self.fetchProductsForSearch(searchBar.text);
        }
        
    }
    func searchBarCancelButtonClicked(searchBar: UISearchBar)
    {
        searchBar.resignFirstResponder()
    }
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        // The user clicked the [X] button or otherwise cleared the text.
        if (searchText.characters.count == 0) {
            searchBar.performSelector(#selector(UIResponder.resignFirstResponder), withObject: nil, afterDelay: 0.1)
        }
    }

    
    
    
    
    override  func updateCartCount()  {
        super.updateCartCount()
        self.cartButton.badgeValue = self.cartCount!;
        
    }
    
    override func updateWishListCount()
    {
        super.updateWishListCount()
         self.wishListButton.badgeValue = self.wishListCount!;
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
         self.cartButton.badgeValue = self.cartCount!;
        self.wishListButton.badgeValue = self.wishListCount!;
    }
//    func setUpRightBarButton() {
//        let image = UIImage(named: "cart")
//        let button = UIButton(type: .Custom)
//        if let knownImage = image {
//            button.frame = CGRectMake(0.0, 0.0, knownImage.size.width, knownImage.size.height)
//        } else {
//            button.frame = CGRectZero;
//        }
//        
//        button.setBackgroundImage(image, forState: UIControlState.Normal)
//        button.addTarget(self,
//                         action: #selector(HomeVc.cartAction(_:)),
//                         forControlEvents: UIControlEvents.TouchUpInside)
//        
//        let newBarButton = ENMBadgedBarButtonItem(customView: button, value: "")
//        cartButton = newBarButton
//        navigationItem.rightBarButtonItem = cartButton
//    }
    func setUpRightBarButton() {
        let image = UIImage(named: "cart")
        let button = UIButton(type: .Custom)
        if let knownImage = image {
            button.frame = CGRectMake(0.0, 0.0, knownImage.size.width, knownImage.size.height)
        } else {
            button.frame = CGRectZero;
        }
        
        button.setBackgroundImage(image, forState: UIControlState.Normal)
        button.addTarget(self,
                         action: #selector(HomeVc.cartAction(_:)),
                         forControlEvents: UIControlEvents.TouchUpInside)
        
        let newBarButton = ENMBadgedBarButtonItem(customView: button, value: "")
        cartButton = newBarButton
        
        
        let image1 = UIImage(named: "wishlist")
        let button1 = UIButton(type: .Custom)
        if let knownImage1 = image1 {
            button1.frame = CGRectMake(0.0, 0.0, knownImage1.size.width+10, knownImage1.size.height)
        } else {
            button1.frame = CGRectZero;
        }
        button1.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Right
        button1.setImage(image1, forState: UIControlState.Normal)
        button1.addTarget(self,
                          action: #selector(HomeVc.wishListAction(_:)),
                          forControlEvents: UIControlEvents.TouchUpInside)
        
        let newBarButton1 = ENMBadgedBarButtonItem(customView: button1, value: "")
        wishListButton = newBarButton1
        
        
        
        navigationItem.rightBarButtonItems = [wishListButton , cartButton]
    }

    func popOverClosed(type:Int,SelectedIndex:Int) -> Void {
        
    }
    @IBAction func sortAction(sender: AnyObject) {
       
        if self.fetchedProductsArr.count == 0
        {
            return;
        }
        
        if self.popOverISOpened == false {
            self.popOverISOpened = true;
            let vc:SortVc = self.storyboard?.instantiateViewControllerWithIdentifier("SortVc") as! SortVc;
            vc.popOverDismissed = self.popOverDismissed;
          sortPopUp =   PopupController
                .create(self)
                .show(vc)
            .didCloseHandler { _ in
                // Do something
                self.popOverISOpened = false;

            }
           
        }
        
        
    }
    
    
    @IBAction func filterAction(sender: AnyObject) {
        if self.fetchedProductsArr.count == 0
        {
            return;
        }

        if self.popOverISOpened == false {
            self.popOverISOpened = true;

        let vc:FilterVc = self.storyboard?.instantiateViewControllerWithIdentifier("FilterVc") as! FilterVc;
             vc.fiterpopOverDismissed = self.fiterpopOverDismissed;
            
            let product = self.fetchedProductsArr[0]
            if(product.price != nil){
                vc.hasPriceAttribute = true
            }
            if(product.color != nil){
                vc.hasPriceAttribute = true
            }
            
            
       filterPopUp = PopupController
            .create(self)
            .show(vc)
        .didCloseHandler { _ in
            // Do something
            self.popOverISOpened = false;
            
        }
        }

//         vc.modalPresentationStyle = .OverCurrentContext
//       
//        self.navigationController!.presentViewController(vc, animated: true) {
//            
//        }
    }
    
    
    func configureView()  {
        tableView.dataSource = self;
        tableView.delegate = self;
        self.filterBtn.setTitleColor(Theme.theme().themeColor!, forState: .Normal);
         self.sortBtn.setTitleColor(Theme.theme().themeColor!, forState: .Normal);
    }
    
    func updateViewAccrodingToFilter(filter:Filter) {
      
        self.productsArr = [Product]()
        
        for p:Product in self.fetchedProductsArr
        {
            var shouldBeAdded = false;
            if filter.below30 && p.price < 30{
                if shouldBeAdded == false {
                    shouldBeAdded = true;
                }
            }
            else  if filter.below60 && p.price < 60{
                if shouldBeAdded == false {
                    shouldBeAdded = true;
                }
            }
            else  if filter.below70 && p.price < 70{
                if shouldBeAdded == false {
                    shouldBeAdded = true;
                }
            }
            else  if filter.above30 && p.price > 30{
                if shouldBeAdded == false {
                    shouldBeAdded = true;
                }
            }
            else  if filter.above60 && p.price > 60{
                if shouldBeAdded == false {
                    shouldBeAdded = true;
                }
            }
            
            
            if p.custom_attributes != nil {
                
                var color:Int?
                for attr:Attribute in p.custom_attributes! {
                    if attr.attribute_code == "color" {
                       color = Int(attr.value!)
                        
                        break;
                    }
                }
                
                var size:Int?
                for attr:Attribute in p.custom_attributes! {
                    if attr.attribute_code == "size" {
                        size = Int(attr.value!)
                        
                        break;
                    }
                }
                
                if color != nil
                {
                    if filter.blue && color == 50{
                        if shouldBeAdded == false {
                            shouldBeAdded = true;
                        }
                    }
                    else if filter.white && color == 59{
                        if shouldBeAdded == false {
                            shouldBeAdded = true;
                        }
                    }
                   else if filter.black && color == 49{
                        if shouldBeAdded == false {
                            shouldBeAdded = true;
                        }
                    }
                    else if filter.green && color == 53{
                        if shouldBeAdded == false {
                            shouldBeAdded = true;
                        }
                    }
                   else if filter.red && color == 58{
                        if shouldBeAdded == false {
                            shouldBeAdded = true;
                        }
                    }

                
                
                }
                
                if size != nil
                {
                    if filter.xs && color == 171{
                        if shouldBeAdded == false {
                            shouldBeAdded = true;
                        }
                    }
                    else if filter.s && color == 172{
                        if shouldBeAdded == false {
                            shouldBeAdded = true;
                        }
                    }
                    else if filter.m && color == 173{
                        if shouldBeAdded == false {
                            shouldBeAdded = true;
                        }
                    }
                    else if filter.l && color == 174{
                        if shouldBeAdded == false {
                            shouldBeAdded = true;
                        }
                    }
                    else if filter.xl && color == 175{
                        if shouldBeAdded == false {
                            shouldBeAdded = true;
                        }
                    }
                    
                    
                    
                }
            }
            if shouldBeAdded
            {
                self.productsArr.append(p);
            }
        }
        
      
        self.tableView.reloadData();
    }
    
    
    func fetchProductsForFilter(filter:Filter)  {
        MagentoHud.show()
        
       /* MGNetworkClient.sharedClient.getProductsBYFilter(<#T##name: String##String#>, completion: <#T##(NSError?, [Product]?) -> Void#>) { (er:NSError?, products:[Product]?) in
            MagentoHud.dismiss()
            if let pArr = products
            {
                
                if pArr.count > 0
                {
                    self.fetchedProductsArr = pArr;
                    self.productsArr = nil
                    self.productsArr = [Product]()
                    
                    for p:Product in self.fetchedProductsArr!
                    {
                        self.productsArr?.append(p);
                    }
                    
                    self.heightDictionary = [Int:CGFloat]();
                    self.tableView.hidden = false ;
                    self.tableView.reloadData();
                }
                else
                {
                    self.tableView.hidden = true ;
                }
            }
            else
            {
                self.tableView.hidden = true ;
            }
        }*/
    }
    
    
    
    
    
    
    func fetchProductsForSearch(str:String?)  {
        MagentoHud.show()
       
        MGNetworkClient.sharedClient.getProductsBYName(str!) { (er:NSError?, products:[Product]?) in
            MagentoHud.dismiss()
            if let pArr = products
            {
                
                if pArr.count > 0
                {
                    self.fetchedProductsArr = pArr;
                    //self.productsArr = nil
                    self.productsArr.removeAll() // = [Product]()
                    
                    for p:Product in self.fetchedProductsArr
                    {
                        self.productsArr.append(p);
                    }
                    
                    self.heightDictionary = [Int:CGFloat]();
                     self.tableView.hidden = false ;
                    self.tableView.reloadData();
                }
                else
                {
                    self.tableView.hidden = true ;
                }
            }
            else
            {
                self.tableView.hidden = true ;
            }
        }
    }
    
    
  
    
    
    func fetchProducts()  {
        MagentoHud.show()
        var categoryIDs:[Int]!
        if(self.catIDs.count > 0)
        {
            categoryIDs = self.catIDs;
        }
        else
        {
            categoryIDs = [self.catID!];
        }
        MGNetworkClient.sharedClient.getProducts(categoryIDs) { (er:NSError?, products:[Product]?) in
            MagentoHud.dismiss()
            if let pArr = products
            {
                
                if pArr.count > 0
                {
                self.fetchedProductsArr = pArr;
                    self.productsArr = [Product]()
                    
                    for p:Product in self.fetchedProductsArr
                    {
                        self.productsArr.append(p);
                    }
                    
                self.heightDictionary = [Int:CGFloat]();
                self.tableView.reloadData();
                }
                else
                {
                self.tableView.hidden = true ;
                }
            }
            else
            {
                self.tableView.hidden = true ;
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
//        if self.productsArr != nil
//        {
//            if self.productsArr!.count > 0
//            {
//                
//                let modulo = self.productsArr!.count%2;
//                self.calculatedRows = self.productsArr!.count/2;
//                if (modulo != 0)
//                {
//                    self.calculatedRows! = self.calculatedRows! + 1;
//                }
//                return self.calculatedRows!
//            }
//        }
        return 1;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
       // if self.productsArr != nil
        //{
            if self.productsArr.count > 0
            {
                
                let modulo = self.productsArr.count%2;
                self.calculatedRows = self.productsArr.count/2;
                if (modulo != 0)
                {
                    self.calculatedRows! = self.calculatedRows! + 1;
                }
                return self.calculatedRows!
            }
        //}
        return 0;
        
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let prodCell:ProductListCell = tableView.dequeueReusableCellWithIdentifier("ProductListCell", forIndexPath: indexPath) as! ProductListCell
        prodCell.delegate = self;
        prodCell.selectionStyle = UITableViewCellSelectionStyle.None
        //prodCell.titleLbl1.adjustsFontSizeToFitWidth = true;
        //prodCell.titleLbl2.adjustsFontSizeToFitWidth = true;
        prodCell.imageView2.hidden = false;
        prodCell.textContainer2.hidden = false;
         prodCell.secondBtn.userInteractionEnabled = true;
         prodCell.firstBtn.userInteractionEnabled = true;
        
        if indexPath.row > 0 {
            let firstIndex = indexPath.row * 2;
            let secondIndex = firstIndex+1;
            let product1 = self.productsArr[firstIndex];
                        prodCell.firstBtn.tag = firstIndex;
           
            prodCell.titleLbl1.text = product1.name;
           
            prodCell.subtitleLbl1.text = "by " + product1.sku!;
          
            prodCell.priceLbl1.text = "$ \(product1.price ?? 0)";
          
            var imageurl1 :String? = nil;
          
            if product1.custom_attributes != nil {
                
                for attr:Attribute in product1.custom_attributes! {
                    if attr.attribute_code == "image" {
                         imageurl1 = attr.value;
                        break;
                    }
                }
            }
           
           
              var imageurl2 :String? = nil;
            
         //  let product2 = try self.productsArr![secondIndex];
            if ((self.productsArr.count) - 1) >= secondIndex {
                let product2 =   self.productsArr[secondIndex];
                prodCell.secondBtn.hidden = false;
                prodCell.titleLbl2.hidden = false;
                prodCell.subtitleLbl2.hidden = false;
                prodCell.priceLbl2.hidden = false;
                prodCell.imageView2.hidden = false;
                prodCell.secondBtn.tag = secondIndex;
                prodCell.titleLbl2.text = product2.name;
                prodCell.subtitleLbl2.text = "by " + product2.sku!;
                prodCell.priceLbl2.text = "$ \(product2.price ?? 0)";
                
                if product2.custom_attributes != nil {
                    
                    for attr:Attribute in product2.custom_attributes! {
                        if attr.attribute_code == "image" {
                            imageurl2 = attr.value;
                            break;
                        }
                    }
                }
                

            }
            else
            {
                prodCell.secondBtn.hidden = true;
                prodCell.titleLbl2.hidden = true;
                prodCell.subtitleLbl2.hidden = true;
                prodCell.priceLbl2.hidden = true;
                prodCell.imageView2.hidden = true;
            }
            
            
           
            self.updateImageAndHeightOF(indexPath, imageUrl1: imageurl1, imageUrl2: imageurl2, onCell: prodCell);
        }
        else
        {
            if self.productsArr.count > 1 {
                let product1 = self.productsArr[0];
                let product2 = self.productsArr[1];
                prodCell.firstBtn.tag = 0;
                prodCell.secondBtn.tag = 1;
                prodCell.titleLbl1.text = product1.name;
                prodCell.titleLbl2.text = product2.name;
                prodCell.subtitleLbl1.text = "by " + product1.sku!;
                prodCell.subtitleLbl2.text = "by " + product2.sku!;
                prodCell.priceLbl1.text = "$ \(product1.price ?? 0)";
                prodCell.priceLbl2.text = "$ \(product2.price ?? 0)";
                var imageurl1 :String? = nil;
                var imageurl2 :String? = nil;
                if product1.custom_attributes != nil {
                    
                    for attr:Attribute in product1.custom_attributes! {
                        if attr.attribute_code == "image" {
                            imageurl1 = attr.value;
                            break;
                        }
                    }
                }
                if product2.custom_attributes != nil {
                    
                    for attr:Attribute in product2.custom_attributes! {
                        if attr.attribute_code == "image" {
                            imageurl2 = attr.value;
                            break;
                        }
                    }
                }
                self.updateImageAndHeightOF(indexPath, imageUrl1: imageurl1, imageUrl2: imageurl2, onCell: prodCell);
             
                //let product1 = self.productsArr![0];
            }
            else
            {
                let product1 = self.productsArr[0];
                prodCell.firstBtn.tag = 0;
                prodCell.secondBtn.userInteractionEnabled = false;
                prodCell.titleLbl1.text = product1.name;
                prodCell.subtitleLbl1.text = "by " + product1.sku!;
               
                prodCell.priceLbl1.text = "$ \(product1.price!)";
              
                var imageurl1 :String? = nil;
                
                if product1.custom_attributes != nil {
                    
                    for attr:Attribute in product1.custom_attributes! {
                        if attr.attribute_code == "image" {
                            imageurl1 = attr.value;
                            break;
                        }
                    }
                }
                prodCell.imageView2.hidden = true;
                prodCell.textContainer2.hidden = true;
                self.updateImageAndHeightOF(indexPath, imageUrl1: imageurl1, imageUrl2: nil, onCell: prodCell);
            }
            

        
        }
        
        return prodCell;
        
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        
        if self.heightDictionary != nil {
            if self.heightDictionary![indexPath.row] != nil
            {
            return self.heightDictionary![indexPath.row]!
            }
        }
        return 330;
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
//        let vc:AddressVC = self.storyboard?.instantiateViewControllerWithIdentifier("AddressVC") as! AddressVC;
//        self.navigationController?.pushViewController(vc, animated: true);
    }

    func updateImageAndHeightOF(indexpath:NSIndexPath, imageUrl1:String?, imageUrl2:String? ,onCell:ProductListCell)
    {
          if imageUrl1 != nil
          {
        onCell.imageViewOne.kf_setImageWithURL(NSURL(string: imageUrl1!)!,
                                                     placeholderImage: UIImage(named: "placeholder"),
                                                     optionsInfo: nil,
                                                     progressBlock: { (receivedSize, totalSize) -> () in
                                                        
                                                       // debugPrint("Download Progress: \(receivedSize)/\(totalSize)")
            },
                                                     completionHandler: { (image, error, cacheType, imageURL) -> () in
                                                        //  debugPrint("Downloaded and set!")
                                                        if let im = image
                                                        {
                                                            if (self.heightDictionary![indexpath.row] == nil)
                                                            {
                                                                
                                                                let rowH = self.rowHeightAccordingToImage(im, neww: (self.view.frame.size.width/2)-7);
                                                                 onCell.imageViewOne.image = im;
                                                                  let rowHeight = rowH + 100;
                                                                self.heightDictionary![indexpath.row] = CGFloat(rowHeight);
                                                                
                                                                if self.heightDictionary!.count == self.productsArr.count
                                                                {
                                                                self.tableView.reloadData()
                                                                }
                                                               // self.tableView.beginUpdates()
                                                              
                                                              //  self.tableView.reloadRowsAtIndexPaths([indexpath], withRowAnimation: .Automatic)
                                                                
                                                                //self.tableView.endUpdates();
                                                                
                                                         
                                                            }
                                                            else
                                                            {
                                                                 onCell.imageViewOne.image = im;
                                                            }
                                                        }
            }
        )
        }
        
        if imageUrl2 != nil {
            onCell.imageView2.kf_setImageWithURL(NSURL(string: imageUrl2!)!,placeholderImage:UIImage(named: "placeholder") );
        }
     
    }
    
    func leftButtonTapped(sender: UIButton)
    {
        //if self.productsArr != nil {
        if(self.productsArr.count > 0){
             let product1 = self.productsArr[sender.tag];
            let vc:ProductDetailVC = self.storyboard?.instantiateViewControllerWithIdentifier("ProductDetailVC") as! ProductDetailVC;
            vc.skuID = product1.sku;
            self.navigationController?.pushViewController(vc, animated: true);
        }
       

    
    }
    func rightButtonTapped(sender: UIButton)
    {
       // if self.productsArr != nil {
        
        if(self.productsArr.count > 0){
            let product1 = self.productsArr[sender.tag];
            let vc:ProductDetailVC = self.storyboard?.instantiateViewControllerWithIdentifier("ProductDetailVC") as! ProductDetailVC;
            vc.skuID = product1.sku;
            self.navigationController?.pushViewController(vc, animated: true);
        }
        

    }
    func captureScreen() -> UIImage {
        var window: UIWindow? = UIApplication.sharedApplication().keyWindow
        window = UIApplication.sharedApplication().windows[0] 
        UIGraphicsBeginImageContextWithOptions(window!.frame.size, window!.opaque, 0.0)
        window!.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image;
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView)
    {
        self.filterheightConstraint.constant = 0;
    
    }
    func scrollViewDidEndDecelerating(scrollView: UIScrollView)
    {
    self.filterheightConstraint.constant = 40;
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

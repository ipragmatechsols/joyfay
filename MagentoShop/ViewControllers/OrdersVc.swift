//
//  OrdersVc.swift
//  MagentoShop
//
//  Created by vikaskumar on 7/15/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import MGNetworkClient
import MagentoHud
import MagentoClient
let cellIdentifierForOrder = "OrderCell";
let cellIdentifierForState = "CityTableViewCell";
let HeaderCellIdentifier = "OrderHeaderView";


class OrdersVc: BaseVc {
    var cartButton: ENMBadgedBarButtonItem!
    @IBOutlet weak var emptyIMageView: UIImageView!
    // Declaring the Mutable array of PopularCities Structure
    var ordersArr = [Order]()
   // var cityArray:[Dictionary<String, Array<City>>] = []
   // var ordersArr:[String] = []
    var booleanArray = [NSNumber]()
    
    var selectedIndexPath: NSIndexPath?
    var selectedHeaderIndexPath: NSIndexPath?
    var heightDictionary: [NSIndexPath:Int] = [NSIndexPath:Int]()
    
    
    @IBOutlet weak var tblView: UITableView!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblView.delegate = self;
        self.tblView.dataSource = self;
        self.title = "My Orders"
        //self.tblView.estimatedRowHeight = 218;
          //self.tblView.contentInset = UIEdgeInsetsMake(-60, 0, 0, 0);
//        self.tblView.registerNib(UINib(nibName: "CityTableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifierForState)
        self.tblView.registerNib(UINib(nibName: "OrderHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: HeaderCellIdentifier)
        //        self.tblView.registerNib(UINib.init(nibName: "ParallaxCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "ParallaxCellIdentifier")
        
        self.tblView!.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
        self.setUpRightBarButton();

        self.getAllOrders()
        //self.populateData()
    }
    
    func setUpRightBarButton() {
        let image = UIImage(named: "profile")
        let button = UIButton(type: .Custom)
        if let knownImage = image {
            button.frame = CGRectMake(0.0, 0.0, knownImage.size.width, knownImage.size.height)
        } else {
            button.frame = CGRectZero;
        }
        
        button.setBackgroundImage(image, forState: UIControlState.Normal)
        button.addTarget(self,
                         action: #selector(OrdersVc.goToOrders),
                         forControlEvents: UIControlEvents.TouchUpInside)
        
        let newBarButton = ENMBadgedBarButtonItem(customView: button, value: "")
        cartButton = newBarButton
        navigationItem.rightBarButtonItem = cartButton
    }
    
    func goToOrders()  {
        let vc = UserDetailsVc()
        let navC:UINavigationController = sideMenuController() as! UINavigationController;
        navC.pushViewController(vc, animated: true)
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        //self.cartButton.badgeValue = self.cartCount!;
    }
    
    override  func updateCartCount()  {
        super.updateCartCount()
       // self.cartButton.badgeValue = self.cartCount!;
        
    }
    
    
    override func updateWishListCount()
    {
        super.updateWishListCount()
       // self.wishListButton.badgeValue = self.wishListCount!;
        
        
    }

    func getAllOrders()  {
        MagentoHud.show()
        MGNetworkClient.sharedClient.getAllOrders(nil) { (er:NSError?, orders:[Order]?) in
            MagentoHud.dismiss()
            if er == nil
            {
                 if orders?.count > 0
                 {
                    
                    for order in orders!{
                        if(order.products.count > 0){
                            self.ordersArr.append(order)
                        }
                    }
                    self.ordersArr.sortInPlace({ $0.order_date > $1.order_date })
                   // self.ordersArr = orders!;
                    self.tblView.hidden = false;
                    self.emptyIMageView.hidden = true;
                    self.populateData()
                    self.tblView.reloadData();
                }
                else
                 {
                    self.tblView.hidden = true;
                    self.emptyIMageView.hidden = false;
                }
            }
            else
            {
                self.tblView.hidden = true;
                self.emptyIMageView.hidden = false;
            }
        }
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Populate Data
    func populateData() {
        
       self.booleanArray = [NSNumber]()
       for _ in 0 ..< self.ordersArr.count {
            self.booleanArray.append(NSNumber(bool: false))
        }
    }
    
    
    // MARK: Tap Gesture Action
    func headerViewTapped(gesture: UITapGestureRecognizer) {
        self.selectedHeaderIndexPath = NSIndexPath(forRow: 0, inSection: (gesture.view?.tag)!)
        if self.selectedHeaderIndexPath!.row == 0 {
            var isCollapsed = self.booleanArray[(self.selectedHeaderIndexPath?.section)!].boolValue
            isCollapsed = !isCollapsed
            
            for (idx, _) in self.booleanArray.enumerate() {
                if idx == self.selectedHeaderIndexPath?.section {
                    self.booleanArray[idx] = NSNumber(bool: isCollapsed)
                    break
                }
            }
            
            let range = NSMakeRange(self.selectedHeaderIndexPath!.section, 1)
            let sectionToReload = NSIndexSet(indexesInRange: range)
            self.tblView?.reloadSections(sectionToReload, withRowAnimation: .Fade)
            if gesture.view?.tag == self.ordersArr.count - 1 && isCollapsed == true{
                self.tblView.scrollToRowAtIndexPath(self.selectedHeaderIndexPath!, atScrollPosition: .Bottom, animated: true)
            }
        }
    }
    
}
// MARK: UITableView Delegates & DataSource
extension OrdersVc: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.ordersArr.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.booleanArray[section].boolValue {
           
              let item = self.ordersArr[section]
               // if (item.products != nil) {
               return item.products.count
                //}
           
            
        }
        return 0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        //  let sectionTitle = self.ordersArr![section] as String
        let headerView:OrderHeaderView = tableView.dequeueReusableHeaderFooterViewWithIdentifier(HeaderCellIdentifier) as! OrderHeaderView;
                headerView.tag = section
           if let order: Order = self.ordersArr[section] as Order {
            let products = order.products
        
            if products.count > 0
            {
                let product = products[0] as Product
                headerView.titleLbl.text = product.name ?? "";
                headerView.subtitleLbl1.text = product.sku ?? "";
                headerView.subtitleLbl2.text = order.order_date ?? "";
                headerView.subtitleLbl3.text = order.status ?? "";
                let amount = Float(order.amount ?? "0")
                let  amountText =   String(format: "%.2f", amount ?? 0.0)
                headerView.priceLbl.text = "$ " + amountText;
               // headerView.priceLbl.adjustsFontSizeToFitWidth = true
            }
        }
        
        let manyCells = self.booleanArray[section].boolValue
        if !manyCells {
            headerView.acessoryImageNormal()
                  }
        else
        {
        headerView.acessoryImageSelcted()
        }
        // Adding Tap gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(OrdersVc.headerViewTapped(_:)))
        headerView.addGestureRecognizer(tapGesture)
        
        return headerView
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: CGRectZero)
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       
        
        return 117.0
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if self.booleanArray[indexPath.section].boolValue {
            return 218
        }
        return 1.0
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) ->
        UITableViewCell {
        
        let tempCell = UITableViewCell()
        if let order: Order = self.ordersArr[indexPath.section] as Order {
            
            let manyCells = self.booleanArray[indexPath.section].boolValue
            if !manyCells {
                let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifierForState) as! CityTableViewCell
                cell.priceLbl.textColor = Theme.theme().themeColor!
               // cell.lblCityName?.text = order.order_date
                let products = order.products
                
                        if products.count > 0
                        {
                            let product = products[0] as Product
                            cell.titleLbl.text = product.name ?? "";
                            cell.subtitleLbl1.text = product.sku ?? "";
                            /*let formatter = NSDateFormatter()
                            formatter.dateFormat = "dd MM YY, hh:mm"
                            formatter.timeStyle = .MediumStyle*/
                            
                            cell.subtitleLbl2.text = order.order_date ?? "";
                            
                            cell.subtitleLbl3.text = order.status ?? "";
                            let amount = Float(order.amount ?? "0")
                            let  amountText =   String(format: "%.2f", amount ?? 0.0)
                            cell.priceLbl.text = "$ " + amountText;
                            
                        }
                
                return cell
            }
            else {
                
       
                let cell:OrderCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifierForOrder)  as! OrderCell
                self.configureOrderCell(cell, onIndexpath: indexPath);
                return cell
                
            }
        }
        return tempCell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedIndexPath = indexPath
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
    }
    
    func configureOrderCell(cell:OrderCell,onIndexpath:NSIndexPath)
    {
       // if  self.ordersArr != nil {
            let order:Order = self.ordersArr[onIndexpath.section];
            let product = order.products[onIndexpath.row];
           
            cell.titleLabel.text = product.name ?? "";
            cell.subtitleLabel.text = product.sku ?? "";
            cell.priceTxtFld.text = "$ \(product.amount ?? "0")"
            if product.qty_ordered != nil {
                cell.qauntityTxtFld.text = "\(product.qty_ordered ?? 0)"
            }
            if product.size != nil {
                cell.sizeTxtFld.text = product.size ?? "";
            }
            
         
            let imageurl:String = product.image ?? ""
        
            cell.itemImageView.kf_setImageWithURL(NSURL(string: imageurl),
                                                  placeholderImage: UIImage(named: "placeholder"),
                                                  optionsInfo: nil,
                                                  progressBlock: { (receivedSize, totalSize) -> () in
                                                    
                                                    debugPrint("Download Progress: \(receivedSize)/\(totalSize)")
                                                    },
                                                  completionHandler: { (image, error, cacheType, imageURL) -> () in
                                                    if let im = image
                                                    {
                                                        cell.itemImageView.image = im;
                                                    }
                                                    })
    }
    
 
}

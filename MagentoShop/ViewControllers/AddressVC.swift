//
//  AddressVC.swift
//  MagentoShop
//
//  Created by Bunty on 27/06/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import MGNetworkClient
import MagentoClient
import MagentoHud
import CoreLocation

class AddressVC: BaseVc,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource
    , CLLocationManagerDelegate {
    
    @IBOutlet weak var street2TxtFld: UITextField!
    @IBOutlet var saveBlckOverlay: UIView!
    @IBOutlet var saveBcg: UIView!
    @IBOutlet var cancelBtn: UIButton!
    @IBOutlet weak var postcodeTxtField: UITextField!
    @IBOutlet var headerView: UIView!
    @IBOutlet var step1Lbl: UILabel!
    @IBOutlet var selectAddressLbl: UILabel!
    @IBOutlet var headerBottomLine: UILabel!
    @IBOutlet var scrollViewContainer: UIView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var scrollContentView: UIView!
    @IBOutlet var firstNameTextField: UITextField!
    
    @IBOutlet var lastNameTxtFld: UITextField!
    
    @IBOutlet var telephoneTxtFld: UITextField!
    
    @IBOutlet weak var countryTxtFld: UITextField!
    
    @IBOutlet var companyTxtFld: UITextField!
    
    @IBOutlet var regionTxtFld: UITextField!
    
    @IBOutlet var addnewAddressLbl: UILabel!
    @IBOutlet var bottomContainer: UIView!
    @IBOutlet var previousBtn: UIButton!
    @IBOutlet var saveBtn: UIButton!
    
    let picker =  UIPickerView();
    let toolBar = UIToolbar()
   
    var selectedCountry: Country?
    var selectedRegion:Region?
    
    var fetchedAddresses = [Addres]()
    
     let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureUI()
        self.title = "ADD ADDRESS"
        self.fillDataFromCurrentUser()
        getCurrentLocation()

        // Do any additional setup after loading the view.
    }

    
    
    func fillDataFromCurrentUser()  {
        let  savedUSer = User.fetchSavedUser();
        self.firstNameTextField.text = savedUSer.firstname ?? "";
        self.lastNameTxtFld.text = savedUSer.lastname ?? "";
        
        if self.selectedCountry != nil {
            //self.countryTxtFld.text = self.selectedCountry!.full_name_english;
            let index =    MGNetworkClient.sharedClient.countries!.indexOf(selectedCountry!)
            picker.selectRow(index!, inComponent: 0, animated: false);
        }else{
            
            getSelectedCountry("United States")
            /*if(MGNetworkClient.sharedClient.countries != nil && MGNetworkClient.sharedClient.countries?.count > 0){
                for ct in MGNetworkClient.sharedClient.countries!{
                    if(ct.countryid == "US"){
                        self.selectedCountry = ct;
                        break
                    }
                }
                if(self.selectedCountry == nil){
                    self.selectedCountry = (MGNetworkClient.sharedClient.countries?[0])!;
                }
            }*/
            
          
            
        }
        
        
        
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.contentSize = CGSizeMake(0, 480);
    }
    
    
    func configureUI()
    {
        firstNameTextField.delegate = self
        lastNameTxtFld.delegate =  self
        telephoneTxtFld.delegate = self
        companyTxtFld.delegate = self
        regionTxtFld.delegate = self
         postcodeTxtField.delegate = self
         countryTxtFld.delegate = self
        street2TxtFld.delegate = self
        self.headerBottomLine.backgroundColor = Theme.theme().themeColor!
        self.cancelBtn.backgroundColor = Theme.theme().themeColor!
        self.saveBcg.backgroundColor = Theme.theme().themeColor!
        self.configurePicker()
    }
    
    @IBAction func previousBtnAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true);
    }

    @IBAction func saveBtnAction(sender: AnyObject) {
        if self.validateFields() {
            self.addAddress();
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backgroundTouch(sender: AnyObject) {
        
        self.view.endEditing(true);
          scrollView.setContentOffset(CGPointZero, animated: true)
        
    }
    func validateFields() -> Bool {
        
        if firstNameTextField.text?.characters.count <= 0 {
            self.showNormalAlert("Please enter your firstname");
            return false;
        }
        else if lastNameTxtFld.text?.characters.count <= 0
        {
            self.showNormalAlert("Please enter your last name");
            return false;
        }
        else if telephoneTxtFld.text?.characters.count <= 0
        {
            self.showNormalAlert("Please enter phone number");
            return false ;
        }
        else if companyTxtFld.text?.characters.count <= 0
        {
            self.showNormalAlert("Please enter street ");
            return false ;
        }
        /*else if street2TxtFld.text?.characters.count <= 0
        {
            self.showNormalAlert("Please enter street ");
            return false ;
        }*/
        else if regionTxtFld.text?.characters.count <= 0
        {
            self.showNormalAlert("Please enter your region/state");
            return false ;
        }
        else if postcodeTxtField.text?.characters.count <= 0
        {
            self.showNormalAlert("Please enter postcode");
            return false ;
        }
        else if countryTxtFld.text?.characters.count <= 0
        {
            self.showNormalAlert("Please select country");
            return false ;
        }

        
        return true;
    }
    
    
    
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        if textField == firstNameTextField {
            lastNameTxtFld.becomeFirstResponder();
        }
        else if textField == lastNameTxtFld
        {
            telephoneTxtFld.becomeFirstResponder();
           
            
        }
        else if textField == telephoneTxtFld
        {
            companyTxtFld.becomeFirstResponder();
              scrollView.setContentOffset(CGPoint(x: 0, y: 60), animated: true)
        }
        
        else if textField == companyTxtFld
        {
            street2TxtFld.becomeFirstResponder();
             scrollView.setContentOffset(CGPoint(x: 0, y: 90), animated: true)
        }
        else if textField == street2TxtFld
        {
            countryTxtFld.becomeFirstResponder();
            scrollView.setContentOffset(CGPoint(x: 0, y: 140), animated: true)
        }
        else if textField == countryTxtFld
        {
            regionTxtFld.becomeFirstResponder();
            scrollView.setContentOffset(CGPoint(x: 0, y: 180), animated: true)
            
        }
        else if textField == regionTxtFld
        {
            postcodeTxtField.becomeFirstResponder();
            scrollView.setContentOffset(CGPoint(x: 0, y: 200), animated: true)
            
        }
//        else if textField == countryTxtFld
//        {
//            //postcodeTxtField.becomeFirstResponder();
//            scrollView.setContentOffset(CGPoint(x: 0, y: 190), animated: true)
//            
//        }
        else
        {
            scrollView.setContentOffset(CGPointZero, animated: true)
            textField.resignFirstResponder();
        }
        return true;
    }
    
     func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
         if textField == companyTxtFld
        {
            //companyTxtFld.becomeFirstResponder();
            scrollView.setContentOffset(CGPoint(x: 0, y: 60), animated: true)
        }
            
        else if textField == street2TxtFld
        {
           // street2TxtFld.becomeFirstResponder();
            scrollView.setContentOffset(CGPoint(x: 0, y: 90), animated: true)
        }
        else if textField == countryTxtFld
        {
           // regionTxtFld.becomeFirstResponder();
            scrollView.setContentOffset(CGPoint(x: 0, y: 140), animated: true)
            if self.selectedCountry != nil {
            let index =    MGNetworkClient.sharedClient.countries!.indexOf(selectedCountry!)
            picker.selectRow(index!, inComponent: 0, animated: false);
        }
         }
        else if textField == regionTxtFld
        {
           // postcodeTxtField.becomeFirstResponder();
            self.selectedRegion = nil;
            setRegion()
            scrollView.setContentOffset(CGPoint(x: 0, y: 180), animated: true)
            
        }
        else if textField == postcodeTxtField
        {
            // countryTxtFld.becomeFirstResponder();
            scrollView.setContentOffset(CGPoint(x: 0, y: 200), animated: true)
        }
        return true;
    }
    
 func textFieldShouldEndEditing(textField: UITextField) -> Bool
 {
    if textField == postcodeTxtField {
         scrollView.setContentOffset(CGPointZero, animated: true)
    }
   
    return true;
}
    
    
    func setRegion(){
        if self.selectedCountry?.available_regions != nil &&  self.selectedCountry?.available_regions?.count > 0{
            let st:Region = (self.selectedCountry?.available_regions![0])!;
            self.selectedRegion = st;
            self.regionTxtFld.text = st.name;
        }
        
        
        if self.selectedCountry?.available_regions != nil && self.selectedCountry?.available_regions?.count > 0 {
            self.makeRegionPicker()
        }
        else
        {
            self.removeRegionPicker()
        }
        picker .reloadAllComponents();
        
    }
    
    
    func getAddressFromFields() ->Addres
    {
        let ad = Addres()
        ad.firstname = self.firstNameTextField.text;
         ad.lastname = self.lastNameTxtFld.text;
        ad.street = self.companyTxtFld.text ?? "";
        ad.street2 = self.street2TxtFld.text ?? "";
        ad.region = self.regionTxtFld.text;
        if selectedRegion != nil {
            ad.region_id = selectedRegion?.id ;
            ad.region_code = selectedRegion?.code;
        }
         ad.postcode = self.postcodeTxtField.text;
         ad.telephone = self.telephoneTxtFld.text;
         ad.email = User.fetchSavedUser().email;
         ad.city = ad.region;
        
       
        ad.country_id = selectedCountry?.countryid ?? "US"
        return ad;
    
    }
    
    
    func addAddress()   {
       let add:Addres = self.getAddressFromFields();
        
        var addressArray = [Addres]()
        addressArray.append(add)
        for address in self.fetchedAddresses {
            addressArray.append(address)
        }
       
        var addessDict =  add.getPramsFromAddressNew(addressArray)
        let  savedUSer = User.fetchSavedUser();
        addessDict["email"] = savedUSer.email;
        addessDict["firstname"] = savedUSer.firstname;
        addessDict["lastname"] = savedUSer.lastname;
        addessDict["website_id"] = savedUSer.website_id;
        
         
        var finalParamDict = [String:AnyObject]()
        finalParamDict["customer"] = addessDict;
      
        
        MagentoHud.show();
        MGNetworkClient.sharedClient.addAddresses(finalParamDict) { (er:NSError?, ad:String?) in
            MagentoHud.dismiss();
            self.navigationController?.popViewControllerAnimated(true);
            if er == nil{}
        }
        
    }
    func configurePicker()  {
        
        
        
        picker.tag = 4203
        picker.showsSelectionIndicator = true
        picker.delegate = self
        picker.dataSource = self
        picker.backgroundColor = UIColor.whiteColor()
      
        toolBar.barStyle = UIBarStyle.Default
        toolBar.translucent = true
        toolBar.tintColor = Theme.theme().themeColor!
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(AddressVC.donePicker))
        
        
        toolBar.setItems([doneButton], animated: false)
        toolBar.userInteractionEnabled = true
        self.countryTxtFld.inputView = picker;
        self.countryTxtFld.inputAccessoryView = toolBar
        
    }
    
    func makeRegionPicker() {
        self.regionTxtFld.inputView = picker;
        self.regionTxtFld.inputAccessoryView = toolBar
        self.regionTxtFld.text = "";
           if self.selectedCountry?.available_regions != nil {
          //  let index =    MGNetworkClient.sharedClient.countries!.indexOf(selectedCountry!)
            picker.selectRow(0, inComponent: 0, animated: false);
        }
       
    }
    func removeRegionPicker() {
        self.regionTxtFld.inputView = nil;
        self.regionTxtFld.inputAccessoryView = nil
    }
    
    func makeCountryPicker() {
        self.countryTxtFld.inputView = picker;
        self.countryTxtFld.inputAccessoryView = toolBar
    }
    func removeCountryPicker() {
        self.countryTxtFld.inputView = nil;
        self.countryTxtFld.inputAccessoryView = nil
    }
    
    
    func donePicker()  {
        
        if self.selectedCountry?.available_regions != nil && self.regionTxtFld.isFirstResponder() {
            let st:Region = (self.selectedCountry?.available_regions?[picker.selectedRowInComponent(0)])!;
            self.selectedRegion = st;
            self.regionTxtFld.text = st.name;
             scrollView.setContentOffset(CGPointZero, animated: true);
              self.regionTxtFld.resignFirstResponder();
            postcodeTxtField.becomeFirstResponder();
            
            return
        }
        
        
        let ct:Country = (MGNetworkClient.sharedClient.countries?[picker.selectedRowInComponent(0)])!;
        self.countryTxtFld.text = ct.full_name_english;
        self.selectedCountry = ct;
        
        scrollView.setContentOffset(CGPointZero, animated: true);
        if self.selectedCountry?.available_regions != nil  {
            self.makeRegionPicker()
        }
        else
        {
            
        self.removeRegionPicker()
            
        }
        
        self.countryTxtFld.resignFirstResponder();
        self.regionTxtFld.becomeFirstResponder();
        picker .reloadAllComponents();
    }
    
    
    
   
    
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(self.regionTxtFld.isFirstResponder()){
            return self.selectedCountry?.available_regions?.count ?? 0
        }else{
            return MGNetworkClient.sharedClient.countries?.count ?? 0
        }
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(self.regionTxtFld.isFirstResponder()){
            if self.selectedCountry?.available_regions != nil {
                let st:Region = (self.selectedCountry?.available_regions?[row])!;
                return  st.name;
            }else{
                return ""
            }
        }else{
            
            let ct:Country = (MGNetworkClient.sharedClient.countries?[row])!;
            return ct.full_name_english ?? "";
            
        }
        
        
        
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        
        
        if(self.regionTxtFld.isFirstResponder()){
            if self.selectedCountry?.available_regions != nil {
                let st:Region = (self.selectedCountry?.available_regions?[row])!;
                self.selectedRegion = st;
                self.regionTxtFld.text = st.name ?? "";
                return
            }
        }else{
            let ct:Country = (MGNetworkClient.sharedClient.countries?[row])!;
            self.selectedCountry = ct;
            self.countryTxtFld.text = ct.full_name_english;
            
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: - Current Location
    
    func getCurrentLocation(){
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    
    
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation)
    {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error)->Void in
            
            if (error != nil) {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }
            
            if placemarks!.count > 0 {
                let pm = placemarks![0]
                self.displayLocationInfo(pm)
            } else {
                print("Problem with the data received from geocoder")
            }
        })
        
        
    }
    
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Error while updating location " + error.localizedDescription)
    }
    
    
    func displayLocationInfo(placemark: CLPlacemark?) {
        if let containsPlacemark = placemark {
            //stop updating location to save battery life
            locationManager.stopUpdatingLocation()
            /*let locality = (containsPlacemark.locality != nil) ? containsPlacemark.locality : ""
             let postalCode = (containsPlacemark.postalCode != nil) ? containsPlacemark.postalCode : ""
             let administrativeArea = (containsPlacemark.administrativeArea != nil) ? containsPlacemark.administrativeArea : ""
             */
            let country = (containsPlacemark.country != nil) ? containsPlacemark.country : ""
            let countryString = String(format: "%@", country!)
            print(countryString)
            self.getSelectedCountry(countryString)
           
            
            /*localityTxtField.text = locality
             postalCodeTxtField.text = postalCode
             aAreaTxtField.text = administrativeArea
             countryTxtField.text = country*/
        }
        
    }
    
    
    
    func getSelectedCountry(country: String?){
        if(MGNetworkClient.sharedClient.countries != nil &&
            MGNetworkClient.sharedClient.countries?.count > 0){
            for ct in MGNetworkClient.sharedClient.countries!{
                if(country != nil &&  ct.full_name_locale == country){
                    self.selectedCountry = ct;
                    break
                }
            }
            if(self.selectedCountry == nil){
                self.selectedCountry = (MGNetworkClient.sharedClient.countries?[0])!;
            }
        }
        
        self.countryTxtFld.text = self.selectedCountry?.full_name_english ?? ""
       
    }
}

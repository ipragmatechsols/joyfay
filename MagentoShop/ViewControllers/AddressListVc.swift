//
//  AddressListVc.swift
//  MagentoShop
//
//  Created by vikaskumar on 7/14/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import MGNetworkClient
import MagentoHud
import MagentoClient

class AddressListVc: BaseVc,UITableViewDelegate,UITableViewDataSource {

    
    @IBOutlet var nextBlackOverLay: UIView!
    @IBOutlet var nextBcg: UIView!
    @IBOutlet var previousBtn: UIButton!
    @IBOutlet weak var noaddressLbl: UILabel!
    @IBOutlet weak var tableview: UITableView!
       var totolAMount:String?
    var fetchedAddress:Addres?
     var fetchedAddresses = [Addres]()
    var cartArr:[CartItem]?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "SELECT ADDRESS"

        // Do any additional setup after loading the view.
        tableview.estimatedRowHeight = 40;
        tableview.rowHeight = UITableViewAutomaticDimension
             self.tableview.allowsSelectionDuringEditing = true;
//        self.tableview.allowsMultipleSelection = true;
        self.tableview.setEditing(true, animated: true);

        //self.tableview.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        self.configureView();
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        
        self.fetchAllAddresses()

    }
    
    func configureView()
    {
        self.previousBtn.backgroundColor = Theme.theme().themeColor!
        self.nextBcg.backgroundColor = Theme.theme().themeColor!
        self.noaddressLbl.textColor = Theme.theme().themeColor!
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func previousBtnAction(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true);
        
    }
    @IBAction func nextBtnAction(sender: UIButton) {
    if (self.tableview.indexPathForSelectedRow != nil)
    {
        
        self.addShippingAddress();
      
 
        
    }
        else
    {
        MGNetworkClient.sharedClient.showErrorALertWithMessage("Please select address");
    }
        
    }
    
    
    @IBAction func addNewAddress(sender: AnyObject) {
            let vc:AddressVC = self.storyboard?.instantiateViewControllerWithIdentifier("AddressVC") as! AddressVC;
            vc.fetchedAddresses = self.fetchedAddresses
            self.navigationController?.pushViewController(vc, animated: true)
    }
    
//    func fetchAllAddresses()
//    {
//        MagentoHud.show()
//        MGNetworkClient.sharedClient.getAllAddresses(nil) { (er:NSError?, address:Addres?) in
//        MagentoHud.dismiss()
//            if address == nil
//            {
//            self.noaddressLbl.hidden = false
//                self.tableview.hidden = true;
//            }
//            else
//            {
//                
//                 if address!.id == nil
//                 {
//                    self.noaddressLbl.hidden = false
//                    self.tableview.hidden = true;
//
//                }
//                else
//                 {
//                  self.noaddressLbl.hidden = true
//                 self.tableview.hidden = false;
//            self.fetchedAddress = address;
//            self.tableview.reloadData();
//                    let indexPath = NSIndexPath.init(forRow: 0, inSection: 0)
//                    self.tableview.selectRowAtIndexPath(indexPath, animated: false , scrollPosition:UITableViewScrollPosition.Top);
//                }
//            }
//            
//            
//        }
//    }
    
    
    func fetchAllAddresses()
    {
        MagentoHud.show()
        MGNetworkClient.sharedClient.getAllAddresses(nil) { (er:NSError?, address:[Addres]?) in
            MagentoHud.dismiss()
            if address == nil
            {
                self.noaddressLbl.hidden = false
                self.tableview.hidden = true;
            }
            else
            {
                
                if address == nil || address?.count <= 0
                {
                    self.noaddressLbl.hidden = false
                    self.tableview.hidden = true;
                    
                }
                else
                {
                    self.noaddressLbl.hidden = true
                    self.tableview.hidden = false;
                    self.fetchedAddresses = address!;
                    self.fetchedAddress = self.fetchedAddresses[0]
                    self.tableview.reloadData();
                    let indexPath = NSIndexPath.init(forRow: 0, inSection: 0)
                    self.tableview.selectRowAtIndexPath(indexPath, animated: false , scrollPosition:UITableViewScrollPosition.Top);
                }
            }
            
            
        }
    }

     func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
     {
       /* if let t = self.fetchedAddresses.count
        {
        return t.count;
        }*/
        return self.fetchedAddresses.count
    }
    
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
     {
        var cell:UITableViewCell!
                    cell = tableView.dequeueReusableCellWithIdentifier("AddressCell", forIndexPath: indexPath) as! AddressCell
         let ads = self.fetchedAddresses
        
            let adCell:AddressCell = cell as! AddressCell
            let ad = ads[indexPath.row]
            let adText = self.getAddressText(ad);
            adCell.titleLbl.text = adText;
             adCell.tintColor = Theme.theme().themeColor
        
        
        return cell;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        for iP:NSIndexPath in tableView.indexPathsForSelectedRows!
        {
                if iP == indexPath {
                    continue;
                }
                tableView .deselectRowAtIndexPath(iP, animated: true);
         }
         let ads = self.fetchedAddresses
         let ad = ads[indexPath.row]
         self.fetchedAddress = ad;
    
    }
    
    
    /*
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            //I did some work here
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
        }
    }*/
    
    
    
    func getAddressText(adr:Addres) -> String {
        
        var addText:String = ""
        
        if adr.firstname != nil  && adr.firstname?.characters.count > 0 {
           addText += adr.firstname!;
        }
        
        if adr.lastname != nil  && adr.lastname?.characters.count > 0 {
       addText += adr.lastname! + "\n";
        }
        
        
        if adr.street != nil  && adr.street?.characters.count > 0 {
          addText += adr.street! + "\n";
        }
        
        if adr.street2 != nil  && adr.street2?.characters.count > 0 {
            addText += adr.street2! + "\n";
        }

        
        
        if adr.region != nil  && adr.region?.characters.count > 0 {
           addText += adr.region! + "\n";
        }
        
//        if adr.city != nil {
//            addText += adr.city! + "\n";
//        }
        
        if adr.postcode != nil  && adr.postcode?.characters.count > 0 {
        addText += adr.postcode! + "\n";
        }
        
        if adr.country_id != nil  && adr.country_id?.characters.count > 0 {
            if let ct = self.getCountryFromId(adr.country_id!) {
                 addText += ct.full_name_english! + "\n";
            }
           
        }
        
        
        
        if adr.telephone != nil   && adr.telephone?.characters.count > 0{
            addText +=  "M: " + adr.telephone!;
        }
        
        
        
       // let formattedText = String(sep:"\n" ,addText)
        return addText;
    }
    func getCountryFromId(ctID:String) -> Country? {
         var ct:Country?
        
        if MGNetworkClient.sharedClient.countries != nil {
            for cts:Country in MGNetworkClient.sharedClient.countries!{
                if cts.countryid == ctID {
                    ct = cts;
                    break
                }
            }
        }
        if ct != nil {
            return ct;
        }
        else
        {
            return nil;
        }
        
        
    }
    func addShippingAddress()   {
        
       if  let add = self.fetchedAddress
       {
        let params =  add.getPramsFromShippingAddress(add,shippingMethod: nil)
        
        MagentoHud.show();
        MGNetworkClient.sharedClient.makeShippingAddresses(params) { (er:NSError?, ad:Bool?,info:ShippingInfo?) in
            MagentoHud.dismiss();
           
            if er == nil
            {
//                let vc:PaymentVC = self.storyboard?.instantiateViewControllerWithIdentifier("PaymentVC") as! PaymentVC;
                let vc:ShippingMethodVc = self.storyboard?.instantiateViewControllerWithIdentifier("ShippingMethodVc") as! ShippingMethodVc;
                vc.totolAMount = self.totolAMount;
                if let ad = self.fetchedAddress
                {
                    
                    let adText = self.getAddressText(ad);
                    vc.shippingAddress = adText;
                    vc.fetchedAddress = self.fetchedAddress;
                    
                }
                vc.shippingInfo = info;
                vc.cartArr = self.cartArr;
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  ForgetPasswordVC.swift
//  MagentoShop
//
//  Created by Noopur Virmani on 26/12/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import Foundation
import MGNetworkClient
import MagentoHud
import MagentoClient

class ForgetPasswordVC: BaseVc , UITextFieldDelegate{

    @IBOutlet var fadingView: UIView!
    @IBOutlet var backgroundImage: UIImageView!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var submitBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Home"
        configureUIForSignUp()
        emailTextField.becomeFirstResponder()
    
    }
    
    
    
    func configureUIForSignUp()
    {
        emailTextField.attributedPlaceholder =
            NSAttributedString(string: "Email", attributes: [NSForegroundColorAttributeName : UIColor.whiteColor()])
        emailTextField.delegate = self
        submitBtn.layer.cornerRadius = 5.0;
        submitBtn.clipsToBounds = true;
        fadingView.backgroundColor = Theme.theme().themeColor
    }
    
    
    @IBAction func cancelAction(sender: AnyObject) {
        self.dismissViewControllerAnimated(true) {
            
        }
    }


    @IBAction func submitBtnClicked(sender: AnyObject) {
        if(validateFields() == true){
            forgetPasswordAPI()
        }
        
    }
    
    
    //MARK : UITextFieldDelegate
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
       textField .resignFirstResponder();
        return true;
    }
    
    
    
    func validateFields() -> Bool {
        let userText = emailTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet());
      
        
        if userText?.characters.count <= 0 {
            
            MGNetworkClient.sharedClient.showErrorALertWithMessage("Please enter your email address");
            //  self.showNormalAlert("Please enter Username");
            return false;
        }
        else if (self.isValidEmail(userText!) == false)
        {
            MGNetworkClient.sharedClient.showErrorALertWithMessage("Please enter a valid email address");
            //self.showNormalAlert("Password should be of atleast six characters");
            return false;
            
        }
        
        return true;
    }

    
    //MARK : MakeRequest
    
    func forgetPasswordAPI(){
        var  params = [String: AnyObject]()
        params["email"] = emailTextField.text;
        params["websiteId"] = "1";
        params["template"] = "email_reset";
        
         MagentoHud.showHudOnView(self.view);
        MGNetworkClient.sharedClient.resetPassword(params) { (er:NSError?, success:Bool?) in
            MagentoHud.dismiss();
            if(success == true){
                self.showSuccessAlert()
            }else{
               MGNetworkClient.sharedClient.showErrorALertWithMessage("Please enter valid email-Id");
            }
        }
    }
    
    
    
    func showSuccessAlert(){
        let message = String(format: "If there is an account associated with %@ you will receive an email with a link to reset your password.", emailTextField.text!)
        let refreshAlert = UIAlertController(title: "Success", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            let vc:LoginVC = self.storyboard?.instantiateViewControllerWithIdentifier("LoginVC") as! LoginVC;
            self.navigationController?.setViewControllers([vc], animated: true);
            
        }))
        presentViewController(refreshAlert, animated: true, completion: nil)
        return;
    }


}
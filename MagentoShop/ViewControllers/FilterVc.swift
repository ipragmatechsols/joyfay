
//
//  FilterVc.swift
//  MagentoShop
//
//  Created by Bunty on 09/07/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import PopupController
import MagentoClient


class FilterVc: BaseVc ,PopupContentViewController{
    
    @IBOutlet var applyFakeBtn: UIButton!
    @IBOutlet var applyBlackOverLay: UIView!
    @IBOutlet var clearBcg: UIView!
    var filter:Filter = Filter();
   // var selectedVision:String = "Size"
    @IBOutlet weak var priceCOntainer: UIView!
    @IBOutlet weak var colorContainer: UIView!
    @IBOutlet weak var sizeCOntainer: UIView!
    @IBOutlet weak var firstLbl: UILabel!
    @IBOutlet weak var secondLbl: UILabel!
    @IBOutlet weak var thirdLbl: UILabel!
    @IBOutlet weak var fourthLbl: UILabel!
    @IBOutlet weak var fifthLbl: UILabel!
    @IBOutlet var firstColorView: UIView!
    @IBOutlet var secondColorView: UIView!
     var fiterpopOverDismissed: ((Int,Filter) -> (Void))?

     @IBOutlet var thirdColorView: UIView!
    
     @IBOutlet var fourthColorView: UIView!
     @IBOutlet var fifthColorView: UIView!
     @IBOutlet var cancelBtn: UIButton!

     @IBOutlet var applyBtn: UIButton!
     @IBOutlet var xsradioBtnoutlet: UIButton!
     @IBOutlet var sradioBtnoutlet: UIButton!
     @IBOutlet var mradioBtnoutlet: UIButton!
     @IBOutlet var lradioBtnoutlet: UIButton!
     @IBOutlet var xlradioBtnoutlet: UIButton!
    
    var hasSizeAttribute: Bool = false
    var hasColorAttribute: Bool = false
    var hasPriceAttribute: Bool = false
    
    
    let alphaValue:CGFloat = 0.3
    let normalValue:CGFloat = 1.0
    
    
    enum selectedVisionE : String {
        case SIZE
        case COLOR
        case PRICE
        case NONE
    }
    
    // var selectedVision:String = "Size"
    var selectedVision: selectedVisionE = selectedVisionE.SIZE
    
    
    func configureView()  {
        clearBcg.backgroundColor = Theme.theme().themeColor;
        cancelBtn.backgroundColor =  Theme.theme().themeColor!
        applyFakeBtn.backgroundColor = Theme.theme().themeColor!
    }
    

    
    @IBAction func xsradioBtn(sender: UIButton) {
        sender.selected = !sender.selected
        
        if sender.selected {
            //sender.backgroundColor = Theme.theme().themeColor!
             sender.backgroundColor = UIColor.whiteColor()
        }
        else{
        sender.backgroundColor = UIColor.clearColor()
        }
        
        self.updateFilterONButtonTap(sender.tag, selected: sender.selected)
    }
    
    @IBAction func sradioBtnaction(sender: UIButton!) {
        sender.selected = !sender.selected
        
        if sender.selected {
           // sender.backgroundColor = Theme.theme().themeColor!
            sender.backgroundColor = UIColor.whiteColor()

        }
        else{
            sender.backgroundColor = UIColor.clearColor()
        }
        self.updateFilterONButtonTap(sender.tag, selected: sender.selected)
    }
    @IBAction func mradioBtnaction(sender: UIButton!) {
        sender.selected = !sender.selected
        
        if sender.selected {
           // sender.backgroundColor = Theme.theme().themeColor!
            sender.backgroundColor = UIColor.whiteColor()

        }
        else{
            sender.backgroundColor = UIColor.clearColor()
        }
        self.updateFilterONButtonTap(sender.tag, selected: sender.selected)
    }
    
    @IBAction func lradioAction(sender: UIButton!) {
        sender.selected = !sender.selected
        
        if sender.selected {
           // sender.backgroundColor = Theme.theme().themeColor!
            sender.backgroundColor = UIColor.whiteColor()

        }
        else{
            sender.backgroundColor = UIColor.clearColor()
        }
        self.updateFilterONButtonTap(sender.tag, selected: sender.selected)
    }
    @IBAction func xlradioAction(sender: UIButton!) {
        sender.selected = !sender.selected
        
        if sender.selected {
           // sender.backgroundColor = Theme.theme().themeColor!
            sender.backgroundColor = UIColor.whiteColor()

        }
        else{
            sender.backgroundColor = UIColor.clearColor()
        }
        self.updateFilterONButtonTap(sender.tag, selected: sender.selected)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //selectedVision = "Size"
        
        if(hasSizeAttribute == true){
            selectedVision = selectedVisionE.SIZE
        }else if (hasColorAttribute == true){
            selectedVision = selectedVisionE.COLOR
        }else if (hasPriceAttribute == true){
            selectedVision = selectedVisionE.PRICE
        }
        else{
            selectedVision = selectedVisionE.NONE
        }
        disableAttributeifnotAvailable()
        
        self.configureView();
        updateSelectedVision()
        
        self.firstColorView.layer.borderWidth = 1.0;
        self.firstColorView.layer.borderColor = UIColor .lightGrayColor().CGColor
        self.firstColorView.clipsToBounds = true;
        self.secondColorView.layer.borderWidth = 1.0;
        self.secondColorView.layer.borderColor = UIColor .lightGrayColor().CGColor
        self.secondColorView.clipsToBounds = true;
        
        self.thirdColorView.layer.borderWidth = 1.0;
        self.thirdColorView.layer.borderColor = UIColor .lightGrayColor().CGColor
        self.thirdColorView.clipsToBounds = true;
        
        self.fourthColorView.layer.borderWidth = 1.0;
        self.fourthColorView.layer.borderColor = UIColor .lightGrayColor().CGColor
        self.fourthColorView.clipsToBounds = true;
        
        self.fifthColorView.layer.borderWidth = 1.0;
        self.fifthColorView.layer.borderColor = UIColor .lightGrayColor().CGColor
        self.fifthColorView.clipsToBounds = true;

    }

    
    override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()
  
    }
    @IBAction func touchedBackground(sender: AnyObject) {
        self.dismissViewControllerAnimated(true) {
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSizeMake(300,380)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func disableAttributeifnotAvailable(){
        
        if(hasSizeAttribute == true){
            sizeActionBtn.enabled = true;
            sizeActionBtn.alpha = normalValue;
        }
        else{
            sizeActionBtn.enabled = false;
            sizeActionBtn.alpha = alphaValue;
        }
        
        if(hasColorAttribute == true){
            colorBtn.enabled = true;
            colorBtn.alpha = normalValue;
        }
        else{
            colorBtn.enabled = false;
            colorBtn.alpha = alphaValue;
        }
        
        if(hasPriceAttribute == true){
            priceBtn.enabled = true;
            priceBtn.alpha = normalValue;
        }
        else{
            priceBtn.enabled = false;
            priceBtn.alpha = alphaValue;
        }
    }
    
    
    func setSizeView() {
        
        firstLbl.hidden = false
        secondLbl.hidden = false
        thirdLbl.hidden = false
        fourthLbl.hidden = false
        fifthLbl.hidden = false
        
//       xsradioBtnoutlet.hidden = false
//        sradioBtnoutlet.hidden = false
//        mradioBtnoutlet.hidden = false
//        lradioBtnoutlet.hidden = false
//        xlradioBtnoutlet.hidden = false
        firstLbl.text = "XS"
        secondLbl.text = "S"
        thirdLbl.text = "M"
        fourthLbl.text = "L"
        fifthLbl.text = "XL"
        
        firstColorView.hidden = true
        secondColorView.hidden = true
        thirdColorView.hidden = true
        fourthColorView.hidden = true
        fifthColorView.hidden = true
    }
    
    func setColorView() {
        
        
//        xsradioBtnoutlet.hidden = false
//        sradioBtnoutlet.hidden = false
//        mradioBtnoutlet.hidden = false
//        lradioBtnoutlet.hidden = false
//        xlradioBtnoutlet.hidden = false

        firstLbl.hidden = true
        secondLbl.hidden = true
        thirdLbl.hidden = true
        fourthLbl.hidden = true
        fifthLbl.hidden = true
        
        firstColorView.hidden = false
        secondColorView.hidden = false
        thirdColorView.hidden = false
        fourthColorView.hidden = false
        fifthColorView.hidden = false
    }
    
    func setPriceView() {
        
        
        firstLbl.hidden = false
        secondLbl.hidden = false
        thirdLbl.hidden = false
        fourthLbl.hidden = false
        fifthLbl.hidden = false

//        xsradioBtnoutlet.hidden = false
//        sradioBtnoutlet.hidden = false
//        mradioBtnoutlet.hidden = false
//        lradioBtnoutlet.hidden = false
//        xlradioBtnoutlet.hidden = false

        firstLbl.text = "Below $30"
        secondLbl.text = "Below $60"
        thirdLbl.text = "Below $70"
        fourthLbl.text = "Above $30"
        fifthLbl.text = "Above $60"
        
        firstColorView.hidden = true
        secondColorView.hidden = true
        thirdColorView.hidden = true
        fourthColorView.hidden = true
        fifthColorView.hidden = true
    }
    
    func configureRadioBtns()  {
        
//        xsradioBtnoutlet.selected = false
//        sradioBtnoutlet.selected = false
//        mradioBtnoutlet.selected = false
//        lradioBtnoutlet.selected = false
//        xlradioBtnoutlet.selected = false
        xsradioBtnoutlet.backgroundColor = UIColor.clearColor()
         sradioBtnoutlet.backgroundColor = UIColor.clearColor()
         mradioBtnoutlet.backgroundColor = UIColor.clearColor()
         lradioBtnoutlet.backgroundColor = UIColor.clearColor()
         xlradioBtnoutlet.backgroundColor = UIColor.clearColor()
         xsradioBtnoutlet.backgroundColor = UIColor.clearColor()
    }

    @IBOutlet weak var sizeActionBtn: UIButton!
    @IBOutlet weak var colorBtn: UIButton!
    @IBOutlet weak var priceBtn: UIButton!
    
    
    @IBAction func SizeBtnAction(sender: UIButton!) {
        selectedVision = selectedVisionE.SIZE
        updateSelectedVision()
    }
    
    
    @IBAction func colorBtnAction(sender:  UIButton!) {
        // selectedVision = "Color"
        selectedVision = selectedVisionE.COLOR
        updateSelectedVision()
        
    }
    
    @IBAction func priceBtnACtion(sender:  UIButton!) {
         //selectedVision = "Price"
        selectedVision = selectedVisionE.PRICE
        updateSelectedVision()
    }
    
    
    
    func updateSelectedVision() {
        if selectedVision == selectedVisionE.SIZE {
            sizeActionBtn.selected = true;
            colorBtn.selected = false;
            priceBtn.selected = false
            
            self.sizeCOntainer.backgroundColor = Theme.theme().themeColor!;
            self.priceCOntainer.backgroundColor = UIColor.clearColor();
            self.colorContainer.backgroundColor = UIColor.clearColor();
            
            self.setSizeView();
            if(hasSizeAttribute == true){
                self.applyBtn.enabled = true
                self.applyBtn.alpha = normalValue
            }else{
                self.applyBtn.enabled = false
                self.applyBtn.alpha = alphaValue
            }

          
            
        }
        else if selectedVision == selectedVisionE.COLOR
        {
            sizeActionBtn.selected = false;
            colorBtn.selected = true;
            priceBtn.selected = false
            
            self.colorContainer.backgroundColor = Theme.theme().themeColor!
            self.sizeCOntainer.backgroundColor = UIColor.clearColor();
            self.priceCOntainer.backgroundColor = UIColor.clearColor();
            
            self.setColorView();
            if(hasColorAttribute == true){
                self.applyBtn.enabled = true
                self.applyBtn.alpha = normalValue
            }else{
                self.applyBtn.enabled = false
                self.applyBtn.alpha = alphaValue
            }

            
        }
            
    
        else  if selectedVision == selectedVisionE.PRICE
        {
            sizeActionBtn.selected = false;
            colorBtn.selected = false;
            priceBtn.selected = true
            
            self.priceCOntainer.backgroundColor = Theme.theme().themeColor!;
            self.sizeCOntainer.backgroundColor = UIColor.clearColor();
            self.colorContainer.backgroundColor = UIColor.clearColor();

            self.setPriceView()
            if(hasPriceAttribute == true){
                self.applyBtn.enabled = true
                self.applyBtn.alpha = normalValue
            }else{
                self.applyBtn.enabled = false
                self.applyBtn.alpha = alphaValue
            }
            
        }else{
            sizeActionBtn.selected = false;
            colorBtn.selected = false;
            priceBtn.selected = false
            self.sizeCOntainer.backgroundColor = UIColor.clearColor();
            self.priceCOntainer.backgroundColor = UIColor.clearColor();
            self.colorContainer.backgroundColor = UIColor.clearColor();
            self.setSizeView();
            self.applyBtn.enabled = false
            self.applyBtn.alpha = alphaValue
          

        }
        
        self.updateViewFromFilter()
        self.updateButtonsColor();

    }

    
    
    
    @IBAction func thirdBtnAction(sender: AnyObject) {
        self.mradioBtnaction(mradioBtnoutlet)
        
    }
    
    
    @IBAction func secondBtnAction(sender: AnyObject) {
        self.sradioBtnaction(sradioBtnoutlet)
    }
    @IBAction func firstBtnAction(sender: AnyObject) {
        self.xsradioBtn(self.xsradioBtnoutlet);
    }
    
    @IBAction func fourthBtnAction(sender: AnyObject) {
        self.lradioAction(lradioBtnoutlet)
    }
    @IBAction func fifthBtnAction(sender: AnyObject) {
        self.xlradioAction(xlradioBtnoutlet)
    }
    
    
    
    
    
    
    func updateViewFromFilter() {
        if selectedVision == selectedVisionE.SIZE {
            xsradioBtnoutlet.selected = filter.xs
            sradioBtnoutlet.selected = filter.s
            mradioBtnoutlet.selected = filter.m
            lradioBtnoutlet.selected = filter.l
            xlradioBtnoutlet.selected = filter.xl

        }
          else if selectedVision == selectedVisionE.COLOR
         {
            xsradioBtnoutlet.selected = filter.blue
            sradioBtnoutlet.selected = filter.white
            mradioBtnoutlet.selected = filter.black
            lradioBtnoutlet.selected = filter.green
            xlradioBtnoutlet.selected = filter.red

        
        }
        else  if selectedVision == selectedVisionE.PRICE
         {
            xsradioBtnoutlet.selected = filter.below30
            sradioBtnoutlet.selected = filter.below60
            mradioBtnoutlet.selected = filter.below70
            lradioBtnoutlet.selected = filter.above30
            xlradioBtnoutlet.selected = filter.above60
            
        }
    }
    
    func updateButtonsColor()  {
        if xsradioBtnoutlet.selected {
           // xsradioBtnoutlet.backgroundColor = Theme.theme().themeColor!
            xsradioBtnoutlet.backgroundColor = UIColor.whiteColor()

        }
        else{
            xsradioBtnoutlet.backgroundColor = UIColor.clearColor()
        }
        
        if sradioBtnoutlet.selected {
            //sradioBtnoutlet.backgroundColor = Theme.theme().themeColor!
            sradioBtnoutlet.backgroundColor = UIColor.whiteColor()

        }
        else{
            sradioBtnoutlet.backgroundColor = UIColor.clearColor()
        }
        
        
        if mradioBtnoutlet.selected {
           // mradioBtnoutlet.backgroundColor = Theme.theme().themeColor!
            mradioBtnoutlet.backgroundColor = UIColor.whiteColor()

        }
        else{
            mradioBtnoutlet.backgroundColor = UIColor.clearColor()
        }
        
        
        if lradioBtnoutlet.selected {
           // lradioBtnoutlet.backgroundColor = Theme.theme().themeColor!
            lradioBtnoutlet.backgroundColor = UIColor.whiteColor()

        }
        else{
            lradioBtnoutlet.backgroundColor = UIColor.clearColor()
        }
        
        
        if xlradioBtnoutlet.selected {
           // xlradioBtnoutlet.backgroundColor = Theme.theme().themeColor!
            xlradioBtnoutlet.backgroundColor = UIColor.whiteColor()

        }
        else{
            xlradioBtnoutlet.backgroundColor = UIColor.clearColor()
        }

    }
    
    
    func updateFilterONButtonTap(tag:Int ,selected:Bool) {
        if selectedVision == selectedVisionE.SIZE {
            
            switch tag {
            case 0:
                self.filter.xs = selected;
                case 1:
                self.filter.s = selected;
                case 2:
                self.filter.m = selected;
                case 3:
                self.filter.l = selected;
                case 4:
                self.filter.xl = selected;
            default : break
                
                
               
            
            }
        }
        else if selectedVision == selectedVisionE.COLOR
        {
             switch tag {
            case 0:
            self.filter.blue = selected;
            case 1:
            self.filter.white = selected;
            case 2:
            self.filter.black = selected;
            case 3:
            self.filter.green = selected;
            case 4:
            self.filter.red = selected;
            default : break

        
        
        }
        }
        else if selectedVision == selectedVisionE.PRICE
            {
                switch tag {
                case 0:
                    self.filter.below30 = selected;
                case 1:
                    self.filter.below60 = selected;
                case 2:
                    self.filter.below70 = selected;
                case 3:
                    self.filter.above30 = selected;
                case 4:
                    self.filter.above60 = selected;
                default : break
                }
        }
    
    }
    
    
    @IBAction func clearButtonAction(sender: AnyObject) {
         self.fiterpopOverDismissed!(9,self.filter);
    }
    
    @IBAction func cancelBtnAction(sender: UIButton!) {
        self.fiterpopOverDismissed!(8,self.filter);
    }
    
    
    @IBAction func applyBtnAction(sender: UIButton!) {
        
        if(hasPriceAttribute == false && hasColorAttribute == false && hasSizeAttribute == false){
            self.fiterpopOverDismissed!(8,self.filter);
        }else{
            self.fiterpopOverDismissed!(1,self.filter);
        }
    }
    

}



class Filter: NSObject {
    
    var xs:Bool = false
    var s:Bool = false
    var m:Bool = false
    var l:Bool = false
    var xl:Bool = false
    var blue:Bool = false
    var white:Bool = false
    var black:Bool = false
    var green:Bool = false
    var red:Bool = false
    var below30:Bool = false
    var below60:Bool = false
    var below70:Bool = false
    var above30:Bool = false
    var above60:Bool = false

  
    
}

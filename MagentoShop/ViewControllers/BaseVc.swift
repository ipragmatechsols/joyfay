//
//  BaseVc.swift
//  MagentoShop
//
//  Created by Bunty on 11/06/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import MGNetworkClient
import MagentoClient

public class BaseVc: UIViewController {
    let IS_IPAD              = UIDevice.currentDevice().userInterfaceIdiom == .Pad
    public var cartCount: String?;
    public var wishListCount: String?;
    public override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        //sideMenuController()?.sideMenu?.allowLeftSwipe = false;
        sideMenuController()?.sideMenu?.menuWidth = self.view.frame.size.width/2;
        //  AppDelegate.appDelegate.navigationController = self.navigationController;
        // Do any additional setup after loading the view.
        
    }
    public override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        self.cartCount = MGNetworkClient.sharedClient.getCartCount()
        self.wishListCount = MGNetworkClient.sharedClient.getWishListCount()
    }
    
    public  func hideNavigationBar()
    {
        self.navigationController?.navigationBarHidden = true
    }
    
    public  func showNavigationBar()  {
        self.navigationController?.navigationBarHidden = false
    }
    
    public  func showNormalAlert(withMessage:String)
    {
        let alert = UIAlertController(title: "Alert!", message: withMessage, preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .Destructive, handler: { (action: UIAlertAction!) in
            
        }))
        
        presentViewController(alert, animated: true, completion: nil)
    }
    
    public   func rowHeightAccordingToImage(image:UIImage , neww:CGFloat) -> CGFloat {
        let h:CGFloat = image.size.height;
        let w:CGFloat = image.size.width;
        
        
        let scaleFactor:CGFloat = h/w;
        
        //let newh:CGFloat = 200;
        // let neww:neww = self.view.frame.size.width;
        let newh =  neww * scaleFactor;
        return newh;
    }
    public func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    public  override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func getCartItems()  {
        if (MGNetworkClient.sharedClient.authToken() != nil)
        {
            
            if MGNetworkClient.sharedClient.cartID() == nil
            {
                
                MGNetworkClient.sharedClient.createACart({ (er:NSError?, cartID) in
                    self.fetchCart();
                    
                })
            }
            else
            {
                self.fetchCart();
            }
            
            
        }
        else
        {
            self.cartCount = "";
            MGNetworkClient.sharedClient.saveCartCount(self.cartCount!);
            self.updateCartCount();
        }
    }
    public func updateCartCount()  {
        
    }
    
    public func updateWishListCount()  {
        
    }
    
    func getWishListItems()  {
        if (MGNetworkClient.sharedClient.authToken() != nil)
        {
            self.fetchWishList()
            
            
        }
        else
        {
            self.wishListCount = "";
            MGNetworkClient.sharedClient.saveWishListCount(self.wishListCount!);
            self.updateWishListCount();
        }
    }
    public func fetchWishList()
    {
        MGNetworkClient.sharedClient.getAllWishListItems(["":""]) { (er:NSError?, cartItems:[WishListItem]?) in
            
            if let c = cartItems
            {
                if c.count > 0
                {
                    self.wishListCount = "\(c.count)";
                }else
                {
                    self.wishListCount = "";
                    
                }
                
            }
            else
            {
                self.wishListCount = "";
            }
            MGNetworkClient.sharedClient.saveWishListCount(self.wishListCount!);
            self.updateWishListCount();
        }
    }
    
    
    public func fetchCart()
    {
        MGNetworkClient.sharedClient.getAllCartItems(["":""]) { (er:NSError?, cartItems:[CartItem]?) in
            
            if let c = cartItems
            {
                if c.count > 0
                {
                    self.cartCount = "\(c.count)";
                }else
                {
                    self.cartCount = "";
                    
                }
                
            }
            else
            {
                self.cartCount = "";
            }
            MGNetworkClient.sharedClient.saveCartCount(self.cartCount!);
            self.updateCartCount();
        }
    }
    @IBAction public func cartAction(sender: AnyObject) {
        if MGNetworkClient.sharedClient.authToken() != nil {
            
            let vc:CartListVC = self.storyboard?.instantiateViewControllerWithIdentifier("CartListVC") as! CartListVC;
            self.navigationController?.pushViewController(vc, animated: true);
        }
        else
        {
            
            //            let vc:LoginVC = self.storyboard?.instantiateViewControllerWithIdentifier("LoginVC") as! LoginVC;
            //            self.navigationController?.setViewControllers([vc], animated: true);
            let vc:LoginVC = self.storyboard?.instantiateViewControllerWithIdentifier("LoginVC") as! LoginVC;
            let navigationController = UINavigationController.init(rootViewController: vc);
            AppDelegate.appDelegate.navigationController?.presentViewController(navigationController, animated: true, completion: {
                
            })
        }
        //
    }
    
    public func wishListAction(sender: AnyObject) {
        if MGNetworkClient.sharedClient.authToken() != nil {
            
            let vc:WishListVC = self.storyboard?.instantiateViewControllerWithIdentifier("WishListVC") as! WishListVC;
            self.navigationController?.pushViewController(vc, animated: true);
        }
        else
        {
            
            //            let vc:LoginVC = self.storyboard?.instantiateViewControllerWithIdentifier("LoginVC") as! LoginVC;
            //            self.navigationController?.setViewControllers([vc], animated: true);
            let vc:LoginVC = self.storyboard?.instantiateViewControllerWithIdentifier("LoginVC") as! LoginVC;
            let navigationController = UINavigationController.init(rootViewController: vc);
            AppDelegate.appDelegate.navigationController?.presentViewController(navigationController, animated: true, completion: {
                
            })
        }
        //
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
class Delegate
{
    static var appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
}
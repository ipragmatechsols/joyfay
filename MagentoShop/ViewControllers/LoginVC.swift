//
//  LoginVC.swift
//  MagentoShop
//
//  Created by Bunty on 11/06/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import MagentoHud
import MGNetworkClient
import MagentoClient

class LoginVC: BaseVc , UITextFieldDelegate{

    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet var backgroundImage: UIImageView!
    @IBOutlet var backgroundOverlayView: UILabel!
    @IBOutlet var userNameTxtfld: UITextField!
    @IBOutlet var passwordTxtFls: UITextField!
    @IBOutlet var loginBtn: UIButton!
    @IBOutlet var registerBtn: UIButton!
    @IBOutlet var registerBtnBackgroundView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        

        self.configureUI();
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        self.hideNavigationBar();
    }

    
    
    func configureUI()
    {
        userNameTxtfld.attributedPlaceholder =
            NSAttributedString(string: "Email", attributes: [NSForegroundColorAttributeName : UIColor.whiteColor()])
        passwordTxtFls.attributedPlaceholder =
            NSAttributedString(string: "Password", attributes: [NSForegroundColorAttributeName : UIColor.whiteColor()])
        
        userNameTxtfld.delegate = self;
        passwordTxtFls.delegate = self;
        
        
        loginBtn.layer.cornerRadius = 5.0;
        loginBtn.clipsToBounds = true;
        backgroundView.backgroundColor = Theme.theme().themeColor!
        self.titleLbl.text = Theme.theme().appTitleName!;
    
    }
    
    @IBAction func cancelAction(sender: AnyObject) {
        self.dismissViewControllerAnimated(true) {
            
        }
    }
    @IBAction func loginBtnAction(sender: AnyObject) {
        
      let fieldsAreValid =  self.validateFields();
        
        if fieldsAreValid {
           passwordTxtFls.resignFirstResponder();
        userNameTxtfld.resignFirstResponder()
            let userText = userNameTxtfld.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet());
            let passText = passwordTxtFls.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet());
            let loguser = User();
            loguser.email = userText;
            loguser.password = passText;
            MagentoHud.showHudOnView(self.view);
                    MGNetworkClient.sharedClient.getCustomerAuthenticationToken(loguser) { (error:NSError?, fetchedUser:User?) in
            
          
                        if(error == nil)
                        {
                            
                          
                                MGNetworkClient.sharedClient.saveUserProfile({ (er:NSError?, user:User?) in
                                       MagentoHud.dismiss();
                                   // if self.navigationController != nil
                                   // {
//                                        let navC:MyNavigationController = AppDelegate.appDelegate.navigationController! as! MyNavigationController
//                                        navC.menuVC?.reloadTable({ 
//                                           // let vc:HomeVc = self.storyboard?.instantiateViewControllerWithIdentifier("HomeVc") as! HomeVc;
//                                            //self.navigationController?.setViewControllers([vc], animated: true);
//                                            //self.navigationController?.pushViewController(vc, animated: true);
//                                           
//                                        });
                                    
                                        self.dismissViewControllerAnimated(true, completion: {
                                            
                                        })

                                                                        //   }
                                    
                                })
                            
                          

                            
                         
                        }
                        else
                        {
                           MagentoHud.dismiss();
                        }
                       
                    }
            
        }
        
        
        
//        let vc:CartListVC = self.storyboard?.instantiateViewControllerWithIdentifier("CartListVC") as! CartListVC;
//        self.navigationController?.pushViewController(vc, animated: true);
//        let vc:ProductDetailVC = self.storyboard?.instantiateViewControllerWithIdentifier("ProductDetailVC") as! ProductDetailVC;
//        self.navigationController?.pushViewController(vc, animated: true);
        
//        let vc:HomeVc = self.storyboard?.instantiateViewControllerWithIdentifier("HomeVc") as! HomeVc;
//        self.navigationController?.pushViewController(vc, animated: true);
        
    }
    
    @IBAction func registerBtnAction(sender: AnyObject){
        let registerVC:SignUpVC = self.storyboard?.instantiateViewControllerWithIdentifier("SignUpVC") as! SignUpVC;
        //self.navigationController?.pushViewController(registerVC, animated: true);
         self.navigationController?.setViewControllers([registerVC], animated: true);
    }
    
    
    @IBAction func forgetPasswordBtnAction(sender: AnyObject)
    {
        let forgetVC:ForgetPasswordVC = self.storyboard?.instantiateViewControllerWithIdentifier("ForgetPasswordVC") as! ForgetPasswordVC;
       // self.navigationController?.pushViewController(forgetVC, animated: true);
        self.navigationController?.setViewControllers([forgetVC], animated: true);
    }
    
    
    
    
    func validateFields() -> Bool {
        let userText = userNameTxtfld.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet());
        let passText = passwordTxtFls.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet());
        
        if userText?.characters.count <= 0 {
            
            MGNetworkClient.sharedClient.showErrorALertWithMessage("Please enter your email address");
          //  self.showNormalAlert("Please enter Username");
            return false;
        }
        else if passText?.characters.count <= 0
        {
             MGNetworkClient.sharedClient.showErrorALertWithMessage("Please enter Password");
            //self.showNormalAlert("Please enter Password");
            return false;
        }
        else if passText?.characters.count <= 5
        {
             MGNetworkClient.sharedClient.showErrorALertWithMessage("Password should be of atleast six characters");
           // self.showNormalAlert("Password should be of atleast six characters");
            return false;
        }
        else if (self.isValidEmail(userText!) == false)
        {
            MGNetworkClient.sharedClient.showErrorALertWithMessage("Please enter a valid email address");
            //self.showNormalAlert("Password should be of atleast six characters");
            return false;
        
        }
        
        return true;
    }
    
    //MARK : UITextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        if textField == userNameTxtfld {
            passwordTxtFls.becomeFirstResponder();
        }
        else
        {
            textField .resignFirstResponder();
        }
        return true;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   

}

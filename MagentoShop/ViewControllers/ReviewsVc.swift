//
//  ReviewsVc.swift
//  MagentoShop
//
//  Created by vikaskumar on 21/12/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import MGNetworkClient
import MagentoClient
class ReviewsVc: BaseVc,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalReviewsLbl: UILabel!
    
    var reviews:[Review]?
      var product: Product?

    override func viewDidLoad() {
        super.viewDidLoad()
        if self.product != nil {
            self.title = product!.name!;
        }
        // Do any additional setup after loading the view.
        if self.reviews != nil {
            self.totalReviewsLbl.text = "\(self.reviews!.count)"
        }
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        self.tableView.estimatedRowHeight = 80
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.setNeedsLayout()
        self.tableView.layoutIfNeeded()
        
    }
    @IBAction func rateBtnAction(sender: AnyObject) {
        
        if MGNetworkClient.sharedClient.authToken() != nil {
            let vc:RatingVc = self.storyboard?.instantiateViewControllerWithIdentifier("RatingVc") as! RatingVc;
            vc.product = self.product;
            self.navigationController?.pushViewController(vc, animated: true);
            
        }
        else
        {
            
            //            let vc:LoginVC = self.storyboard?.instantiateViewControllerWithIdentifier("LoginVC") as! LoginVC;
            //            self.navigationController?.setViewControllers([vc], animated: true);
            let vc:LoginVC = self.storyboard?.instantiateViewControllerWithIdentifier("LoginVC") as! LoginVC;
            let navigationController = UINavigationController.init(rootViewController: vc);
            AppDelegate.appDelegate.navigationController?.presentViewController(navigationController, animated: true, completion: {
                
            })
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    //MARK: - TableViewDataSource
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.reviews != nil {
            return (self.reviews?.count)!
        }
        else
        {
            return 0;
        
        }
        
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell:UITableViewCell!
        
        cell = tableView.dequeueReusableCellWithIdentifier("ReviewCell", forIndexPath: indexPath) as! ReviewCell
        let desCell: ReviewCell = cell as! ReviewCell
        
        let rev = self.reviews![indexPath.row];
        desCell.titleLbl.text = rev.title;
        desCell.nameLbl.text = rev.nickname;
        desCell.descLbl.text = rev.detail;
       // self.configureReviewCell(cell, onIndexpath: indexPath);
        
        desCell.selectionStyle = UITableViewCellSelectionStyle.None
        
        return desCell;
        
    }
    
    func configureReviewCell(cell:UITableViewCell,onIndexpath:NSIndexPath)  {
        
      
        
    }
  

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

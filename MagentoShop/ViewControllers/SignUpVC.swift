//
//  SignUpVC.swift
//  MagentoShop
//
//  Created by Bunty on 23/06/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import MGNetworkClient
import MagentoHud
import MagentoClient
class SignUpVC: BaseVc,UITextFieldDelegate
{

    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var fadingView: UIView!
    @IBOutlet var backgroundImage: UIImageView!
    @IBOutlet var firstnameTextField: UITextField!
    
    @IBOutlet var lastNameTextField: UITextField!
    
     @IBOutlet var signUPBtn: UIButton!
    
  
    
    @IBOutlet var emailTextField: UITextField!
    
    @IBOutlet var passwordTextField: UITextField!
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.configureUIForSignUp()

        // Do any additional setup after loading the view.
    }
    func configureUIForSignUp()
    {
        firstnameTextField.attributedPlaceholder =
            NSAttributedString(string: "First name", attributes: [NSForegroundColorAttributeName : UIColor.whiteColor()])
        lastNameTextField.attributedPlaceholder =
            NSAttributedString(string: "Last name", attributes: [NSForegroundColorAttributeName : UIColor.whiteColor()])
        emailTextField.attributedPlaceholder =
            NSAttributedString(string: "Email", attributes: [NSForegroundColorAttributeName : UIColor.whiteColor()])
        passwordTextField.attributedPlaceholder =
            NSAttributedString(string: "Password", attributes: [NSForegroundColorAttributeName : UIColor.whiteColor()])
        

        firstnameTextField.delegate = self
        lastNameTextField.delegate =  self
        emailTextField.delegate = self
        passwordTextField.delegate = self
        
        signUPBtn.layer.cornerRadius = 5.0;
        signUPBtn.clipsToBounds = true;
        fadingView.backgroundColor = Theme.theme().themeColor
        self.titleLbl.text = Theme.theme().appTitleName;
        
        
    }
    @IBAction func cancelAction(sender: AnyObject) {
         self.dismissViewControllerAnimated(true) {
            
        }
    }
    
    @IBAction func signUpBtn(sender: UIButton) {
        if self.validateFields() {
            let firstName = firstnameTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet());
            let lastname = lastNameTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet());
            let email = emailTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet());
            let password = passwordTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet());
            let user = User();
            user.firstname = firstName;
            user.lastname = lastname;
            user.email = email;
            user.password = password;
              MagentoHud.showHudOnView(self.view);
            MGNetworkClient.sharedClient.signUpwithParams(user) { (error:NSError?, fetchedUser:User?) in
                
                
                if((error) != nil)
                {
                      MagentoHud.dismiss();
                    //print(error!.localizedDescription);
                }
                else
                {
                  //  print(fetchedUser?.firstname);
                    fetchedUser?.password = user.password;
                    MGNetworkClient.sharedClient.getCustomerAuthenticationToken(fetchedUser!) { (error:NSError?, fetchedUser:User?) in
                        
                        MagentoHud.dismiss();
                        
                        self.dismissViewControllerAnimated(true, completion: {
                            
                        })
//                        if(error == nil)
//                        {
//                            let vc:HomeVc = self.storyboard?.instantiateViewControllerWithIdentifier("HomeVc") as! HomeVc;
//                            self.navigationController?.setViewControllers([vc], animated: true);
//                        }
                        
                    }
                }
            }
        }
    }
    
    
    func validateFields() -> Bool {
        let firstName = firstnameTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet());
        let lastname = lastNameTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet());
         let email = emailTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet());
         let password = passwordTextField.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet());

        
        if firstName!.characters.count <= 0 {
            //self.showNormalAlert("Please enter name");
               MGNetworkClient.sharedClient.showErrorALertWithMessage("Please enter your first name");
            return false;
        }
        else if lastname!.characters.count <= 0
        {
             MGNetworkClient.sharedClient.showErrorALertWithMessage("Please enter your last name");
           // self.showNormalAlert("Please enter last name");
            return false;
        }
        else if email!.characters.count <= 0
        {
             MGNetworkClient.sharedClient.showErrorALertWithMessage("Please enter your email address");
            //self.showNormalAlert("Please enter mail");
            return false ;
        }
        else if password!.characters.count <= 0
        {
             MGNetworkClient.sharedClient.showErrorALertWithMessage("Please enter password");
           // self.showNormalAlert("Please enter password");
            return false ;
        }
        else if password?.characters.count <= 5
        {
            MGNetworkClient.sharedClient.showErrorALertWithMessage("Password should be of atleast six characters");
            // self.showNormalAlert("Password should be of atleast six characters");
            return false;
        }
        else if (self.isValidEmail(email!) == false)
        {
            MGNetworkClient.sharedClient.showErrorALertWithMessage("Please enter a valid email address");
            //self.showNormalAlert("Password should be of atleast six characters");
            return false;
            
        }
        
        return true;
    }
    
    //MARK : UITextFieldDelegate
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        if textField == firstnameTextField {
            lastNameTextField.becomeFirstResponder();
        }
            else if textField == lastNameTextField
        {
            emailTextField.becomeFirstResponder();
            
        }
            else if textField == emailTextField
        {
            passwordTextField.becomeFirstResponder();
        }
          
        else
        {
            textField .resignFirstResponder();
        }
        return true;
    }
    @IBAction func logInBtn(sender: UIButton)
    {
        
        let vc:LoginVC = self.storyboard?.instantiateViewControllerWithIdentifier("LoginVC") as! LoginVC;
        self.navigationController?.setViewControllers([vc], animated: true);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

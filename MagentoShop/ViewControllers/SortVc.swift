//
//  SortVc.swift
//  MagentoShop
//
//  Created by vikaskumar on 7/19/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import PopupController

class SortVc: BaseVc,PopupContentViewController {

    @IBOutlet var fourthBtn: UIButton!
    @IBOutlet var firstBtn: UIButton!
    @IBOutlet var secondBtn: UIButton!
    @IBOutlet var thirdBtn: UIButton!
    var popOverDismissed: ((Int,Int) -> (Void))?
    @IBAction func firstBtnAction(sender: UIButton) {
        self.selectradioButton(firstBtn)
         self.dismissedWithIndex(0);
        
    }
    @IBAction func secondBtnAction(sender: UIButton) {
        self.selectradioButton(secondBtn)
        self.dismissedWithIndex(1);

    }
    @IBAction func thirdBtnAction(sender: UIButton) {
        self.selectradioButton(thirdBtn)
          self.dismissedWithIndex(2);

    }
    @IBAction func fourthBtnAction(sender: UIButton) {
        self.selectradioButton(fourthBtn)
          self.dismissedWithIndex(3);

    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissedWithIndex(index:Int)  {
        self.popOverDismissed!(0,index);
    }
    
    @IBAction func touchedBackground(sender: AnyObject) {
        self.dismissViewControllerAnimated(true) {
              self.dismissedWithIndex(0);
        }
    }

    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSizeMake(280,277)
    }
func selectradioButton (sender: UIButton)
{   self.firstBtn.selected = false
    self.secondBtn.selected = false
    self.thirdBtn.selected = false
    self.fourthBtn.selected = false
    sender.selected = true
    
    
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  ProductDetailVC.swift
//  MagentoShop
//
//  Created by Bunty on 27/06/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import MGNetworkClient
import MagentoHud
import Kingfisher
import MagentoClient
import IQDropDownTextField
import WebKit



class ProductDetailVC: BaseVc ,UITableViewDataSource,UITableViewDelegate,
ProductDetailColorCellDelegate,ProductDetailSizeCellDelegate,
UIPickerViewDelegate,UIPickerViewDataSource,IQDropDownTextFieldDelegate,
UITextFieldDelegate ,UIWebViewDelegate, WKNavigationDelegate{
    @IBOutlet var addCartBcg: UIView!
    @IBOutlet var blackOverLayView: UIView!
    @IBOutlet weak var hiddenTxtField: UITextField!
    @IBOutlet weak var footerView: UIView!

    @IBOutlet weak var addToCartBtn: UIButton!
    @IBOutlet weak var chooseQauntityBtn: UIButton!
    @IBOutlet var tableView: UITableView!
     var pickerDataSource = ["1", "2", "3", "4","5","6","7","8","9","10"];
    var skuID:String?
    var selectedColorText:String?;
    var selectedColorNumber:Int?;
    var selectedSizeNumber:Int?;
    var selectedImage:String?;
     var fetchedProduct:Product?
    var reviews = [Review]()
    var firstRowHeight:CGFloat?;
    var storedOffsets = [Int: CGFloat]()
    var firstCellISUpdated = false;
    var noOFRows:Int?
    let toolBar = UIToolbar()
     var cartButton: ENMBadgedBarButtonItem!
    var wishListButton: ENMBadgedBarButtonItem!
    var currentPicker:UIPickerView?
    var customizedSelected = false;
    var webViewContentHeight:CGFloat = 50.0;
     var shippingWebViewContentHeight:CGFloat = 50.0;
    var knownColorsArr = ["blue","red","yellow","gray","purple","orange","green","black","white",]
   
    var detailSizeAdjust: Bool = true
    var shippingSizeAdjust: Bool = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView();
        self.title = "Detail"
        if self.skuID != nil {
            self.fetchProductsDetail();
        }
        self.tableView.estimatedRowHeight = 80
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.setNeedsLayout()
        self.tableView.layoutIfNeeded()
    
        
        self.configurePicker()
        self.setUpRightBarButton();
        self.setTableViewFooter()
        
        
        
        // Do any additional setup after loading the view.
    }
    
    func setTableViewFooter()  {
        let v  = UIView.init(frame: CGRectMake(0, 0, self.view.frame.size.width, 60));
        //v.backgroundColor = Theme.theme().themeColor
        let btnWidth:CGFloat = v.frame.size.width;
        let btnHeight:CGFloat = 60;
        
        let hwlpbutton = UIButton(frame: CGRect(x: 10, y: 10, width: btnWidth-20, height: btnHeight-20))
        hwlpbutton.backgroundColor = Theme.theme().themeColor
       // hwlpbutton.setImage(UIImage.init(named: "help"), forState: .Normal);
        hwlpbutton.setTitle("Rate this Product", forState: .Normal)
        hwlpbutton.layer.cornerRadius = 5.0;
        hwlpbutton.clipsToBounds = true;
        hwlpbutton.addTarget(self,
                             action: #selector(ProductDetailVC.tabbedOnRating),
                             forControlEvents: UIControlEvents.TouchUpInside)
        
        
        v.addSubview(hwlpbutton)
        self.tableView.tableFooterView = v;
    }
    
    
    func tabbedOnRating() {
        
        if MGNetworkClient.sharedClient.authToken() != nil {
                    let vc:RatingVc = self.storyboard?.instantiateViewControllerWithIdentifier("RatingVc") as! RatingVc;
            vc.product = self.fetchedProduct;
            self.navigationController?.pushViewController(vc, animated: true);
        
        }
        else
        {
            
            //            let vc:LoginVC = self.storyboard?.instantiateViewControllerWithIdentifier("LoginVC") as! LoginVC;
            //            self.navigationController?.setViewControllers([vc], animated: true);
            let vc:LoginVC = self.storyboard?.instantiateViewControllerWithIdentifier("LoginVC") as! LoginVC;
            let navigationController = UINavigationController.init(rootViewController: vc);
            AppDelegate.appDelegate.navigationController?.presentViewController(navigationController, animated: true, completion: {
                
            })
        }

        
    }

    func configurePicker()  {
        let picker: UIPickerView
        picker = UIPickerView()
       
        picker.tag = 4203
        picker.showsSelectionIndicator = true
        picker.delegate = self
        picker.dataSource = self
         picker.backgroundColor = UIColor.whiteColor()
        
        toolBar.barStyle = UIBarStyle.Default
        toolBar.translucent = true
        toolBar.tintColor = Theme.theme().themeColor!
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(ProductDetailVC.donePicker))
        
        
        toolBar.setItems([doneButton], animated: false)
        toolBar.userInteractionEnabled = true
        self.hiddenTxtField.text = "1";
        self.hiddenTxtField.inputView = picker
        self.hiddenTxtField.inputAccessoryView = toolBar
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        self.cartButton.badgeValue =  MGNetworkClient.sharedClient.getCartCount(); // self.cartCount!;
        self.wishListButton.badgeValue = MGNetworkClient.sharedClient.getWishListCount();//self.wishListCount!;
        self.showNavigationBar();
    }
    
    func pickerViewWithTag(tag:Int) -> UIPickerView {
        
        let picker: UIPickerView
        picker = UIPickerView()
        picker.showsSelectionIndicator = true
        picker.delegate = self
        picker.dataSource = self
        picker.backgroundColor = UIColor.whiteColor()
        picker.tag = tag;
        return picker;
    }
    
    @IBAction func wishList(sender: AnyObject) {
        if MGNetworkClient.sharedClient.authToken() != nil {
             self.addingWishListItem()
            
        }
        else
        {
            
            let vc:LoginVC = self.storyboard?.instantiateViewControllerWithIdentifier("LoginVC") as! LoginVC;
            let navigationController = UINavigationController.init(rootViewController: vc);
            AppDelegate.appDelegate.navigationController?.presentViewController(navigationController, animated: true, completion: {
                
            })
        }
        
        

        
    }
    
    @IBAction func share(sender: AnyObject) {
        var imgUrl:String = ""
        
        MagentoHud.show()
        
        if self.fetchedProduct != nil {
            if self.fetchedProduct?.medias?.count > 0 {
                let media:Media = (self.fetchedProduct?.medias![0])!
                imgUrl = media.file ?? "";
            }
            else
            {
                if self.fetchedProduct!.custom_attributes != nil {
                    
                    for attr:Attribute in self.fetchedProduct!.custom_attributes! {
                        if attr.attribute_code == "image" {
                            imgUrl = attr.value ?? "";
                            break;
                        }
                    }
                }
            }
            
            
            let string: String = (self.fetchedProduct?.name)!;
            
            let imageView = UIImageView()
            imageView.kf_setImageWithURL(NSURL(string: imgUrl)!,
                                         placeholderImage: UIImage(named: "placeholder"),
                                         optionsInfo: nil,
                                         progressBlock: { (receivedSize, totalSize) -> () in },
                                         
                                         completionHandler: { (image, error, cacheType, imageURL) -> () in
                                           MagentoHud.dismiss()
                                            if let im = image
                                            {
                                                let activityViewController = UIActivityViewController(activityItems: [(im), string], applicationActivities: nil)
                                                self.navigationController?.presentViewController(activityViewController, animated: true, completion: nil)
                                                
                                            }
                                            else
                                            {
                                                let image: UIImage = UIImage(named: "AppIcon")!
                                                let activityViewController = UIActivityViewController(activityItems: [(image), string], applicationActivities: nil)
                                                self.navigationController?.presentViewController(activityViewController, animated: true, completion: nil)
                                            
                                            }
                                            
                                            
                                            
                                            
                                            
                                                
                                            
                }
            )

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        }
        
        
        
        
        
    }
    
    
    
    
    
    
    @IBAction func addToCartBtnAction(sender: AnyObject) {
        
        if MGNetworkClient.sharedClient.authToken() != nil {
            
         self.addingToCartProcess()
        }
        else
        {
//            let vc:LoginVC = self.storyboard?.instantiateViewControllerWithIdentifier("LoginVC") as! LoginVC;
//            self.navigationController?.setViewControllers([vc], animated: true);
            let vc:LoginVC = self.storyboard?.instantiateViewControllerWithIdentifier("LoginVC") as! LoginVC;
            let navigationController = UINavigationController.init(rootViewController: vc);
            AppDelegate.appDelegate.navigationController?.presentViewController(navigationController, animated: true, completion: {})
        }
        
    }
//    func setUpRightBarButton() {
//        let image = UIImage(named: "cart")
//        let button = UIButton(type: .Custom)
//        if let knownImage = image {
//            button.frame = CGRectMake(0.0, 0.0, knownImage.size.width, knownImage.size.height)
//        } else {
//            button.frame = CGRectZero;
//        }
//        
//        button.setBackgroundImage(image, forState: UIControlState.Normal)
//        button.addTarget(self,
//                         action: #selector(HomeVc.cartAction(_:)),
//                         forControlEvents: UIControlEvents.TouchUpInside)
//        
//        let newBarButton = ENMBadgedBarButtonItem(customView: button, value: "")
//        cartButton = newBarButton
//        
////       // let editImage   = UIImage(named: "plus")!
////        let shareImage = UIImage(named: "Share-99")!
////        
////       // let editButton   = UIBarButtonItem(image: editImage,  style: .Plain, target: self, action: "didTapEditButton:")
////        let shareButton = UIBarButtonItem(image: shareImage,  style: .Plain, target: self, action: #selector(ProductDetailVC.share(_:)))
//
//        navigationItem.rightBarButtonItem = cartButton
//    }
    
    func setUpRightBarButton() {
        let image = UIImage(named: "cart")
        let button = UIButton(type: .Custom)
        if let knownImage = image {
            button.frame = CGRectMake(0.0, 0.0, knownImage.size.width, knownImage.size.height)
        } else {
            button.frame = CGRectZero;
        }
        
        button.setBackgroundImage(image, forState: UIControlState.Normal)
        button.addTarget(self,
                         action: #selector(HomeVc.cartAction(_:)),
                         forControlEvents: UIControlEvents.TouchUpInside)
        
        let newBarButton = ENMBadgedBarButtonItem(customView: button, value: "")
        cartButton = newBarButton
        
        
        let image1 = UIImage(named: "wishlist")
        let button1 = UIButton(type: .Custom)
        if let knownImage1 = image1 {
            button1.frame = CGRectMake(0.0, 0.0, knownImage1.size.width+10, knownImage1.size.height)
        } else {
            button1.frame = CGRectZero;
        }
        button1.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Right
        button1.setImage(image1, forState: UIControlState.Normal)
        button1.addTarget(self,
                          action: #selector(HomeVc.wishListAction(_:)),
                          forControlEvents: UIControlEvents.TouchUpInside)
        
        let newBarButton1 = ENMBadgedBarButtonItem(customView: button1, value: "")
        wishListButton = newBarButton1
        
        
        
        navigationItem.rightBarButtonItems = [wishListButton , cartButton]
    }
    
    override  func updateCartCount()  {
        super.updateCartCount()
        self.cartButton.badgeValue = self.cartCount!;
        
    }
    
    override func updateWishListCount()
    {
        super.updateWishListCount()
        self.wishListButton.badgeValue = self.wishListCount!;
        
        
    }
    func addingToCartProcess()  {
         MagentoHud.show();
         if let cID =  MGNetworkClient.sharedClient.cartID() {
            self.addingCartItem(cID);
        
        }
        else
         {
           
            MGNetworkClient.sharedClient.createACart({ (er:NSError?, cartID) in
                if er != nil
                {
                    MagentoHud.dismiss();
                }
                else
                {
             self.addingCartItem(cartID);
                }
        })
        
        }
    }
    
//    func addingCartItem(cartID:String!) {
//        if self.fetchedProduct != nil {
//            
//            let cart = CartItem(product: self.fetchedProduct);
//            cart.quote_id = cartID;
//            cart.qty = Int(self.hiddenTxtField.text!);
//            var customOptions = [[String:String]]();
//            let colorOption = ["option_id":"color","option_value":String(self.selectedColorNumber)]
//             let sizeOption = ["option_id":"size","option_value":String(self.selectedSizeNumber)]
//             let imageOption = ["option_id":"thumbnail","option_value":self.selectedImage!]
//            customOptions = [colorOption,sizeOption,imageOption];
//            
//            let params = cart.getParamsFromProduct(cart, custom_options: customOptions);
//            let finalParams = ["cartItem":params];
//              debugPrint(finalParams);
//            
//            MGNetworkClient.sharedClient.addItemToCart(finalParams, completion: { (er:NSError?, cart:CartItem?) in
//                self.saveProduct();
//                MagentoHud.dismiss();
//                if(er == nil)
//                {
//                MGNetworkClient.sharedClient.showSuccessALertWithMessage("\(self.fetchedProduct!.name!)  is successfully added to your cart)")
//                }
//                self.getCartItems();
//            })
//        }
//    }
    
    
    func addingWishListItem() {
        if self.fetchedProduct != nil {
            MagentoHud.show();
            MGNetworkClient.sharedClient.addItemToWishList("\(self.fetchedProduct!.id!)", completion: { (er:NSError?, isAdded:Bool?) in
                
                MagentoHud.dismiss();
                if(er == nil )
                {
                    MGNetworkClient.sharedClient.showSuccessALertWithMessage("\(self.fetchedProduct!.name!)  is successfully added to Wish List)")
                }
                self.getWishListItems();
            })
        }
        
       
    }

    
    
    
    
    func addingCartItem(cartID:String!) {
        if self.fetchedProduct != nil {
            
            if self.checkIfAllOptionsAreSelected() == false {
                  MagentoHud.dismiss();
                MGNetworkClient.sharedClient.showErrorALertWithMessage("Please select all options");
                return;
                
            }
            
            
            /*var customOptions = [[String:String]]();
            for opt:Option in self.fetchedProduct!.options! {
                if opt.selectedValue != nil {
                    let opt = ["option_id":String(opt.option_id!),"option_value":opt.selectedValue!];
                    customOptions.append(opt);
                }
            }*/
            
            
            var customOptions = [[String:String]]();
            for opt:Option in self.fetchedProduct!.options! {
                
                if(opt.selectedValue != nil){
                    
                    for optionvalue:OptionValue in opt.optionsValues{
                        if(optionvalue.title == opt.selectedValue){
                            let opt = ["option_id": String(opt.option_id!),
                                       "option_value": String( format: "%d",optionvalue.option_type_id ?? 0)];
                            customOptions.append(opt);
                        }
                    }
                    
                }
                
                
                
            }

            
            
            //if customOptions.count <= 0 {
                for attr:Attribute in self.fetchedProduct!.custom_attributes! {
                    if attr.attribute_code == "image" {
                        let opt = ["option_id":"image","option_value":attr.value!];
                        customOptions.append(opt);
                    }
                    if attr.attribute_code == "size" {
                        let opt = ["option_id":"size","option_value":attr.value!];
                        customOptions.append(opt);
                    }
                    
                    if attr.attribute_code == "color" {
                        let opt = ["option_id":"color","option_value":attr.value!];
                        customOptions.append(opt);
                    }
            }
            //}
            let cart = CartItem(product: self.fetchedProduct);
            cart.quote_id = cartID;
            cart.qty = Int(self.hiddenTxtField.text!);
            
//            let colorOption = ["option_id":"color","option_value":String(self.selectedColorNumber)]
//            let sizeOption = ["option_id":"size","option_value":String(self.selectedSizeNumber)]
//            let imageOption = ["option_id":"thumbnail","option_value":self.selectedImage!]
//            customOptions = [colorOption,sizeOption,imageOption];
            
            let params = cart.getParamsFromProduct(cart, custom_options: customOptions);
            let finalParams = ["cartItem":params];
            //debugPrint(finalParams);
            
            MGNetworkClient.sharedClient.addItemToCart(finalParams, completion: { (er:NSError?, cart:CartItem?) in
                self.saveProduct(cart?.product?.sku);
                MagentoHud.dismiss();
                if(er == nil)
                {
                    MGNetworkClient.sharedClient.showSuccessALertWithMessage("\(self.fetchedProduct!.name!)  is successfully added to your cart)")
                }
                self.getCartItems();
            })
        }
    }

    func  saveProduct(skuNumber: String?)  {
        var productDict = MGNetworkClient.sharedClient.productDict()
        //let productSKu = self.fetchedProduct?.sku!
        let productSKu = skuNumber ?? ""
         let productPrice = self.fetchedProduct?.price ?? 0
        productDict[productSKu] = "\(productPrice)";
        MGNetworkClient.sharedClient.saveProductDict(productDict);
        
        
    }
    
    @IBAction func chooseQauantityAction(sender: AnyObject) {
        
        
        self.hiddenTxtField.becomeFirstResponder();
    }
    func configureView()  {
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        self.chooseQauntityBtn.backgroundColor = Theme.theme().themeColor!
        self.addCartBcg.backgroundColor = Theme.theme().themeColor!
    }
    
    func donePicker()  {
        
        if self.hiddenTxtField.isFirstResponder() {
            self.chooseQauntityBtn .setTitle(self.hiddenTxtField.text, forState: .Normal);
        }
      self.hiddenTxtField.resignFirstResponder();
        for cell in tableView.visibleCells  {
            
            if cell.isKindOfClass(DropDownCell) {
                 let desCell: DropDownCell = cell as! DropDownCell
               if desCell.dropdownTextField.isFirstResponder()
               {
                if self.fetchedProduct != nil && self.fetchedProduct?.options != nil
                {
                    
                    let opt:Option!;
                    if !self.customizedSelected {
                        self.getDropDownOptions()
                        opt = self.getDropDownOptions()[cell.tag - 1]
                    }
                    else
                    {
                        opt = self.fetchedProduct!.options![cell.tag - 1];
                        
                    }
                    let optValue:OptionValue = (opt.optionsValues[(currentPicker?.selectedRowInComponent(0))!]);
                    let path:NSIndexPath = NSIndexPath.init(forItem: cell.tag, inSection: 0)
                    let dropCell:DropDownCell = tableView.cellForRowAtIndexPath(path) as! DropDownCell;
                    dropCell.dropdownTextField.text = optValue.title;
                    opt.selectedValue =  optValue.title;
                    
                    if ((optValue.title?.containsString("Customized")) == true || (optValue.title?.containsString("Children's Size")) == true) {
                        self.customizedSelected = true;
                        self.tableView.reloadData();
                    }
                    else
                    {
                        self.customizedSelected = false;
                        self.tableView.reloadData();
                    }
                }

                }
                desCell.dropdownTextField.resignFirstResponder()
            }
            
        }
    }
    
    func fetchProductsDetail()  {
        MagentoHud.show();
      //self.skuID = "WP01"
        MGNetworkClient.sharedClient.getProductsDetailfromSKU(self.skuID!) { (er:NSError?, product:Product?) in
           
            self.fetchedProduct = product;
            if self.fetchedProduct != nil
            {
                self.title = self.fetchedProduct?.name;
                 self.setDefaultSelectedValues()
                 self.getProductReviews();
            }
            else
            {
            MagentoHud.dismiss();
            }
            
            
           
            
            
        }    }
    
    
    func getProductReviews()  {
        if self.fetchedProduct != nil {
            MGNetworkClient.sharedClient.getAllReviews("\(self.fetchedProduct!.id!)", completion: { (er:NSError?, cts:[Review]?) in
                 //MagentoHud.dismiss();
                if er == nil
                {
                    if cts != nil
                    {
                            if cts?.count > 0
                            {
                                        self.tableView.tableFooterView?.hidden = true;
                                    self.reviews = cts!;
                            }
                            else
                            {
                                self.tableView.tableFooterView?.hidden = false;
                            }
                    
                    }
                }else{
                    MagentoHud.dismiss();
                }
                self.tableView.reloadData();
                
            });
        }
    }
    
    func getDropDownOptions() -> [Option] {
       // var count = 0
        var opts = [Option]()
        
        for opt:Option in (self.fetchedProduct?.options)! {
            if opt.optionsValues.count > 0 {
                opts.append(opt);
            }
        }
        return opts;
    }
    
    //MARK: - TableViewDataSource
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        var staticCount = 3;
        
        if self.reviews.count > 0 {
            staticCount = 4;
        }
        
        if self.fetchedProduct != nil && self.fetchedProduct?.options != nil  && self.fetchedProduct?.options?.count > 0
        {
            if !self.customizedSelected {
                noOFRows = staticCount + self.getDropDownOptions().count;
            }
            else
            {
            noOFRows = staticCount + (self.fetchedProduct?.options?.count)!;
            }

        }
        else
        {
            noOFRows = staticCount;

        }
        return noOFRows!;
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell:UITableViewCell!
        var staticCount = 3;
        
        if self.reviews.count > 0 {
            staticCount = 4;
        }

        print(" All Indexpath %@", indexPath)
        
        if indexPath.row == 0  {
            cell = tableView.dequeueReusableCellWithIdentifier("ProductDetailimageViewCellTableViewCell", forIndexPath: indexPath);
            let imCell:ProductDetailimageViewCellTableViewCell = cell as! ProductDetailimageViewCellTableViewCell;
            imCell.backgroundColor = Theme.theme().themeColor!
            imCell.pageControl.numberOfPages = self.getImageCout();
            imCell.pageControl.currentPageIndicatorTintColor = Theme.theme().themeColor!
            imCell.priceLbl.backgroundColor = Theme.theme().themeColor!
            if self.fetchedProduct != nil {
                imCell.priceLbl.text = "$ \(self.fetchedProduct!.price ?? 0)";
            }else{
                imCell.priceLbl.text = "$ 0";
            }
            imCell.priceLbl.superview?.layer.cornerRadius = 5.0;
            imCell.priceLbl.superview?.clipsToBounds = true;
            //self.configureImageCell(cell, onIndexpath: indexPath);
        }
            
        else if (indexPath.row == noOFRows! - (staticCount - 1))
        {
            cell = tableView.dequeueReusableCellWithIdentifier("DescriptionViewCell", forIndexPath: indexPath) as! DescriptionViewCell
             print("Desc indexpath %@", indexPath.row)
            self.configureDescriptionCell(cell, onIndexpath: indexPath);
        }
        else if (indexPath.row == noOFRows! - (staticCount - 2))
        {
            cell = tableView.dequeueReusableCellWithIdentifier("DescriptionViewCell", forIndexPath: indexPath) as! DescriptionViewCell
            self.configureShippingCell(cell, onIndexpath: indexPath);
        }
        else if (indexPath.row == noOFRows! - (staticCount - 3) && staticCount >= 4)
        {
            cell = tableView.dequeueReusableCellWithIdentifier("ReviewCell1", forIndexPath: indexPath) as! ReviewCell
            self.configureReviewCell(cell, onIndexpath: indexPath);
        }
        else
        {
          //  if cell == nil {
            cell = nil;
                cell = tableView.dequeueReusableCellWithIdentifier("DropDownCell") as! DropDownCell
            
                self.configureDropdownCell(cell, onIndexpath: indexPath);
            
            //}
            
            
            
        
        }
        
        
       
//            switch indexPath.row {
//            case 0:
//                cell = tableView.dequeueReusableCellWithIdentifier("ProductDetailimageViewCellTableViewCell", forIndexPath: indexPath);
//                let imCell:ProductDetailimageViewCellTableViewCell = cell as! ProductDetailimageViewCellTableViewCell;
//                imCell.backgroundColor = Theme.theme().themeColor!
//                imCell.pageControl.numberOfPages = self.getImageCout();
//                imCell.pageControl.currentPageIndicatorTintColor = Theme.theme().themeColor!
//                imCell.priceLbl.backgroundColor = Theme.theme().themeColor!
//                if self.fetchedProduct != nil {
//                     imCell.priceLbl.text = "$ \(self.fetchedProduct!.price!)";
//                    
//                }
//                imCell.priceLbl.superview?.layer.cornerRadius = 5.0;
//                imCell.priceLbl.superview?.clipsToBounds = true;
//
//                //self.configureImageCell(cell, onIndexpath: indexPath);
//            case 1:
//            cell = tableView.dequeueReusableCellWithIdentifier("ProductDetailColorCell", forIndexPath: indexPath) as! ProductDetailColorCell
//                self.configureColorCell(cell, onIndexpath: indexPath)
//            case 2:
//             cell = tableView.dequeueReusableCellWithIdentifier("ProductDetailSizeCell", forIndexPath: indexPath) as! ProductDetailSizeCell
//             self.configureSizeCell(cell, onIndexpath: indexPath);
//            case 3:
//            cell = tableView.dequeueReusableCellWithIdentifier("DescriptionViewCell", forIndexPath: indexPath) as! DescriptionViewCell
//            self.configureDescriptionCell(cell, onIndexpath: indexPath);
//            default:
//                cell = tableView.dequeueReusableCellWithIdentifier("DescriptionViewCell", forIndexPath: indexPath) as! DescriptionViewCell
//        
//        }
        

        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
       
        return cell;
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        
        var height:CGFloat! ;
//        switch indexPath.row {
//        case 0:
//            if self.firstRowHeight != nil {
//                height = self.firstRowHeight!
//            }
//            else
//            {
//
//           height = 230;
//            }
//        case 1:
//            
//            if self.fetchedProduct != nil {
//                if self.fetchedProduct!.type_id != nil {
//                    if self.fetchedProduct?.type_id == "configurable" {
//                        height = 91;
//                        break;
//                    }
//                    else if self.nonConfigurableColor() != nil
//                    {
//                    
//                    height = 91
//                        break;
//                    }
//                }
//            }
//            height = 0;
//        case 2:
//            
//            if self.fetchedProduct != nil {
//                if self.fetchedProduct!.type_id != nil {
//                    if self.fetchedProduct?.type_id == "configurable" {
//                        height = 91;
//                        break;
//                    }
//                    else if self.nonConfigurableSize() != nil
//                    {
//                        
//                        height = 91
//                        break;
//                    }
//                }
//            }
//            height = 0;
//        case 3:
//            height = UITableViewAutomaticDimension;
//            
//            
//            
//        default:
//            height = 0;
//        }
        
        var staticCount = 3;
        
        if self.reviews.count > 0 {
            staticCount = 4;
        }
        
        if indexPath.row == 0  {
            
            if self.firstRowHeight != nil {
                height = self.firstRowHeight!
            }
            else
            {
                
                height = 230;
            }
        }
        else if (indexPath.row == noOFRows! - (staticCount - 1))
        {
             height = self.webViewContentHeight + 50;
        }
        else if (indexPath.row == noOFRows! - (staticCount - 2 ))
        {
            height = self.shippingWebViewContentHeight + 50;
        }
        else if (indexPath.row == noOFRows! - (staticCount - 3) && staticCount >= 4)
        {
            return UITableViewAutomaticDimension
        }
        else
        {
            height = 58;
            
        }
        
        
        return height;
    }
    func tableView(tableView: UITableView,
                   willDisplayCell cell: UITableViewCell,
                                   forRowAtIndexPath indexPath: NSIndexPath) {
        
        guard let tableViewCell:ProductDetailimageViewCellTableViewCell = cell as? ProductDetailimageViewCellTableViewCell else { return }
        
        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }
    
    func tableView(tableView: UITableView,
                   didEndDisplayingCell cell: UITableViewCell,
                                        forRowAtIndexPath indexPath: NSIndexPath) {
        
        guard let tableViewCell:MenDualImageCell = cell as? MenDualImageCell else { return }
        storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
           func getColorFromNumber(value:Int) -> UIColor {
        switch value {
        case 50:
            return UIColor.blueColor();
        case 58:
            return UIColor.redColor();
        case 60:
            return UIColor.yellowColor();
        case 52:
            return UIColor.grayColor();
        case 57:
            return UIColor.purpleColor();
        case 56:
            return Theme.theme().themeColor!;
        case 53:
            return UIColor.greenColor();
        case 49:
            return UIColor.blackColor();
        case 59:
            return UIColor.whiteColor();
        
        default:
            return UIColor.whiteColor();
        }
    }
    func getTextFromFromColorCode(value:Int) -> String {
        switch value {
        case 50:
            return "blue";
        case 58:
            return "red";
        case 60:
            return "yellow";
        case 52:
            return "gray";
        case 57:
            return "purple";
        case 56:
            return "orange";
        case 53:
            return "green";
        case 49:
            return "black";
        case 59:
            return "white";
            
        default:
            return "white";
        }
    }

        func getSizeFromNumber(value:Int) -> String? {
            switch value {
            case 171:
                return "XS";
            case 172:
                return "S";
            case 173:
                return "M";
            case 174:
                return "L";
            case 175:
                return "XL";
            case 176:
                return "XXL";
          
                
                
            default:
                return "M";
            }
    }
    
    func checkIfAllOptionsAreSelected() -> Bool {
        var isSelected = true;
        if self.fetchedProduct != nil && self.fetchedProduct?.options != nil
        {
            let opts:[Option]!;
            if !self.customizedSelected {
                self.getDropDownOptions()
                opts = self.getDropDownOptions()
            }
            else
            {
                opts = self.fetchedProduct!.options!;
                
            }

            for opt:Option in opts {
                if opt.selectedValue == nil {
                    isSelected = false;
                    break;
                }
            }
           
        }
        return isSelected;
    }
    
    func configureDropdownCell(cell:UITableViewCell,onIndexpath:NSIndexPath) {
        cell.tag = onIndexpath.row;
        if self.fetchedProduct != nil && self.fetchedProduct?.options != nil
        {
             let desCell: DropDownCell = cell as! DropDownCell
            let opt:Option!;
            if !self.customizedSelected {
                self.getDropDownOptions()
                opt = self.getDropDownOptions()[onIndexpath.row - 1]
            }
            else
            {
                opt = self.fetchedProduct!.options![onIndexpath.row - 1];
            
            }
           // let opt:Option = self.fetchedProduct!.options![onIndexpath.row - 1];
            desCell.dropdownTextField.placeholder = opt.title;
            desCell.dropdownTextField.tag = onIndexpath.row - 1;
            desCell.dropdownTextField.delegate = self;
            
           
            if opt.optionsValues.count>0 {
                
            desCell.dropdownTextField.inputView = self.pickerViewWithTag(onIndexpath.row - 1);
            desCell.dropdownTextField.inputAccessoryView = toolBar;
                if desCell.dropImage != nil {
                     desCell.dropImage.hidden = false;
                }
               
                desCell.dropdownTextField.reloadInputViews();
            }
            else
            {
                desCell.dropdownTextField.inputView = nil;
                desCell.dropdownTextField.inputAccessoryView = nil;
              desCell.dropdownTextField.reloadInputViews();
                 if desCell.dropImage != nil {
            desCell.dropImage.hidden = true;
                }
            }
            
            if opt.selectedValue != nil {
                desCell.dropdownTextField.text = opt.selectedValue;
            }
            else
            {
               desCell.dropdownTextField.text = "";
            }

           
                       // desCell.dropdownTextField.isOptionalDropDown = false
           // desCell.dropdownTextField.optionalItemText = opt.title;
//            let toolBar = UIToolbar()
//            toolBar.barStyle = UIBarStyle.Default
//            toolBar.translucent = true
//            toolBar.tintColor = Theme.theme().themeColor!
//            toolBar.sizeToFit()
//            
//            let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(ProductDetailVC.donePicker))
            
//            
//            toolBar.setItems([doneButton], animated: false)
//            toolBar.userInteractionEnabled = true
           // desCell.dropdownTextField.delegate = self;
            
           
        }
        
    }

    func configureDescriptionCell(cell:UITableViewCell,onIndexpath:NSIndexPath)  {
        
        let desCell: DescriptionViewCell = cell as! DescriptionViewCell
        desCell.titleLbl.text = "Description :"
        var descrip:String = ""
        if self.fetchedProduct != nil
        {        if self.fetchedProduct!.custom_attributes != nil {
            
            for attr:Attribute in self.fetchedProduct!.custom_attributes! {
                if attr.attribute_code == "description" {
                    desCell.webView.tag = 420;
                    descrip = attr.value ?? "";
                    desCell.webView.delegate = self;
                    desCell.webView.loadHTMLString(descrip, baseURL: nil);
                    desCell.webView.scrollView.scrollEnabled = false;
                    
                    //setWKWebView(desCell, descrip: descrip)
                    
                   //
         
//                    let regex = try! NSRegularExpression(pattern: "<.*?>", options: [.CaseInsensitive])
//                    
//                    let range = NSMakeRange(0, descrip!.characters.count)
//                    let htmlLessString :String = regex.stringByReplacingMatchesInString(descrip!, options: [],
//                                                                                        range:range ,
//                                                                                        withTemplate: "")
                    /*let stripedHtmlStr = descrip.stripHTML();
                    
                    
                    
                    
                    desCell.descriptionLbl.text = stripedHtmlStr.stringByReplacingOccurrencesOfString("&bull;", withString: "\n");
                    desCell.descriptionLbl.hidden = true;*/
                    break;
                }
            }
        }

        }

    }
    
    
    
    func setWKWebView(desCell: DescriptionViewCell, descrip: String){
        let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
        let userScript = WKUserScript(source: jscript, injectionTime: WKUserScriptInjectionTime.AtDocumentEnd, forMainFrameOnly: true)
        let wkUController = WKUserContentController()
        wkUController.addUserScript(userScript)
        let wkWebConfig = WKWebViewConfiguration()
        wkWebConfig.userContentController = wkUController
        
        let wkWebView: WKWebView = WKWebView(frame: desCell.webView.frame, configuration: wkWebConfig)
        wkWebView.frame.size.height = self.webViewContentHeight
        
        wkWebView.tag = desCell.webView.tag
        wkWebView.navigationDelegate = self
        
        wkWebView.loadHTMLString(descrip, baseURL: nil)
        wkWebView.allowsBackForwardNavigationGestures = true
        wkWebView.scrollView.scrollEnabled = false;
        
        desCell.contentView.addSubview(wkWebView)

    }
    
    
    
    func configureReviewCell(cell:UITableViewCell,onIndexpath:NSIndexPath)  {
        let desCell: ReviewCell = cell as! ReviewCell
        let rev = self.reviews[0];
        desCell.titleLbl.text = rev.title;
         desCell.nameLbl.text = rev.nickname;
        desCell.descLbl.text = rev.detail;
    }
    
    
    func configureShippingCell(cell:UITableViewCell,onIndexpath:NSIndexPath)  {
        
        let desCell: DescriptionViewCell = cell as! DescriptionViewCell
        desCell.titleLbl.text = "Shipping Info :"
        var descrip:String?
        if self.fetchedProduct != nil
        {        if self.fetchedProduct!.custom_attributes != nil {
            
            for attr:Attribute in self.fetchedProduct!.custom_attributes! {
                if attr.attribute_code == "shipping" {
                    descrip = attr.value;
                    desCell.webView.delegate = self;
                    desCell.webView .loadHTMLString(descrip!, baseURL: nil);
                    desCell.webView.scrollView.scrollEnabled = false;
                    
                    //                    let regex = try! NSRegularExpression(pattern: "<.*?>", options: [.CaseInsensitive])
                    //
                    //                    let range = NSMakeRange(0, descrip!.characters.count)
                    //                    let htmlLessString :String = regex.stringByReplacingMatchesInString(descrip!, options: [],
                    //                                                                                        range:range ,
                    //                                                                                        withTemplate: "")
                    let stripedHtmlStr = descrip?.stripHTML();
                    
                    
                    
                    
                    desCell.descriptionLbl.text = stripedHtmlStr?.stringByReplacingOccurrencesOfString("&bull;", withString: "\n");
                    desCell.descriptionLbl.hidden = true;
                    break;
                }
            }
            }
            
        }
        
        
        
    }
    
    
    func setDefaultSelectedValues()  {
        for attr:Attribute in self.fetchedProduct!.custom_attributes! {
            if attr.attribute_code == "color" {
                self.selectedColorNumber = Int(attr.value!);
            }
            if attr.attribute_code == "size" {
                self.selectedSizeNumber = Int(attr.value!);
            }
            if attr.attribute_code == "image" {
               
                let splitArr:[String]? =  attr.value?.componentsSeparatedByString(MGNetworkClient.sharedClient.imageMiddleUrl);
                if splitArr?.count > 0 {
                    self.selectedImage = splitArr?[1];
                }
                
            
            }
        }
    }
    func getImageCout() -> Int {
        if self.fetchedProduct != nil {
            if self.fetchedProduct?.medias?.count > 0 {
                return (self.fetchedProduct?.medias?.count)!;
            }
            else
            {
                return 1;
            }
        }
        else
        {
            return 1;
        }
    }
    func configureColorCell(cell:UITableViewCell,onIndexpath:NSIndexPath)  {
               let imCEll:ProductDetailColorCell = cell as! ProductDetailColorCell;
        imCEll.delegate = self;
        if self.fetchedProduct != nil {
            
            
            if self.fetchedProduct?.extension_attributes != nil
            {
                   if self.fetchedProduct?.type_id == "configurable" {
                    
                for attr:ExtAttribute in (self.fetchedProduct?.extension_attributes!)! {
                    if attr.label == "Color" {
                        let values = attr.values;
                        var i:Int = 0;
                        for value in values! {
                            if i < imCEll.colorBtnArr?.count {
                                let btn = imCEll.colorBtnArr![i];
                                btn.hidden = false;
                                btn.backgroundColor = self.getColorFromNumber(value);
                              if  btn.backgroundColor == UIColor.whiteColor()
                              {
                                btn.layer.borderWidth = 1.0;
                                btn.layer.borderColor = UIColor .lightGrayColor().CGColor

                                }
                                btn.tag = value;
                            }
                            i += 1;
                        }
                        break;
                    }
                }
                }
                else
                   {
                    let color = self.nonConfigurableColor();
                    let btn = imCEll.colorBtnArr![0];
                    btn.hidden = false;
                    btn.backgroundColor = color
                    if  btn.backgroundColor == UIColor.whiteColor()
                    {
                        btn.layer.borderWidth = 1.0;
                        btn.layer.borderColor = UIColor .lightGrayColor().CGColor
                        
                    }
                    if self.nonConfigurableColorNumber() != nil
                    {
                    btn.tag = self.nonConfigurableColorNumber()!;
                    }
                }

                
                }
            }
        }
    
    
    func configureSizeCell(cell:UITableViewCell,onIndexpath:NSIndexPath)  {
        let imCEll:ProductDetailSizeCell = cell as! ProductDetailSizeCell;
        imCEll.delegate = self;
        if self.fetchedProduct != nil {
            if self.fetchedProduct?.extension_attributes != nil
            {  if self.fetchedProduct?.type_id == "configurable" {
                for attr:ExtAttribute in (self.fetchedProduct?.extension_attributes!)! {
                    if attr.label == "Size" {
                        let values = attr.values;
                        var i:Int = 0;
                        for value in values! {
                            if i < imCEll.sizeBtnArr?.count {
                                let btn = imCEll.sizeBtnArr![i];
                                btn.hidden = false;
                               // btn.backgroundColor = self.getColorFromNumber(value);
                                let title = self.getSizeFromNumber(value);
                                btn.setTitle(title, forState: .Normal);
                                btn.tag = value;
                            }
                            i += 1;
                        }
                        break;
                    }
                }
            }
            
                    else
                    {
                        let size = self.nonConfigurableSize()
                        let btn = imCEll.sizeBtnArr![0];
                        btn.hidden = false;
                        // btn.backgroundColor = self.getColorFromNumber(value);
                        let title = size;
                        btn.setTitle(title, forState: .Normal);
                        if self.nonConfigurableSizeNUmber() != nil
                        {
                        btn.tag = self.nonConfigurableSizeNUmber()!;
                        }
                        }
                
                    
                }
            }
        }
    
    
    func colorBtn1Tapped(sender: UIButton)
    {
       
        self.selectedColorText = self.getTextFromFromColorCode(sender.tag);
        self.selectedColorNumber = sender.tag;
        self.tableView.reloadData();
    }
    
    func colorBtn2Tapped(sender: UIButton)
    {
        self.selectedColorText = self.getTextFromFromColorCode(sender.tag);
         self.selectedColorNumber = sender.tag;
        self.tableView.reloadData();}
    func colorBtn3Tapped(sender: UIButton)
    {
        self.selectedColorText = self.getTextFromFromColorCode(sender.tag);
         self.selectedColorNumber = sender.tag;
        self.tableView.reloadData();}
    func colorBtn4Tapped(sender: UIButton)
    {
        self.selectedColorText = self.getTextFromFromColorCode(sender.tag);
         self.selectedColorNumber = sender.tag;
        self.tableView.reloadData();}
    func colorBtn5Tapped(sender: UIButton)
    {
        self.selectedColorText = self.getTextFromFromColorCode(sender.tag);
         self.selectedColorNumber = sender.tag;
        self.tableView.reloadData();}
    func colorBtn6Tapped(sender: UIButton)
    {
        self.selectedColorText = self.getTextFromFromColorCode(sender.tag);
         self.selectedColorNumber = sender.tag;
        self.tableView.reloadData();}
    func colorBtn7Tapped(sender: UIButton)
    {
        self.selectedColorText = self.getTextFromFromColorCode(sender.tag);
         self.selectedColorNumber = sender.tag;
        self.tableView.reloadData();}
    func colorBtn8Tapped(sender: UIButton)
    {
        self.selectedColorText = self.getTextFromFromColorCode(sender.tag);
         self.selectedColorNumber = sender.tag;
        self.tableView.reloadData();
    }
    
    
    func sizeBtn1Tapped(sender: UIButton){
        self.selectedSizeNumber = sender.tag;
    }
    func sizeBtn2Tapped(sender: UIButton){
     self.selectedSizeNumber = sender.tag;}
    func sizeBtn3Tapped(sender: UIButton){
     self.selectedSizeNumber = sender.tag;}
    func sizeBtn4Tapped(sender: UIButton){
     self.selectedSizeNumber = sender.tag;}
    func sizeBtn5Tapped(sender: UIButton){
     self.selectedSizeNumber = sender.tag;}
    func sizeBtn6Tapped(sender: UIButton){
     self.selectedSizeNumber = sender.tag;}
    
    func configureImageCell(cell:UITableViewCell,onIndexpath:NSIndexPath)  {
//        let imCEll:ProductDetailimageViewCellTableViewCell = cell as! ProductDetailimageViewCellTableViewCell;
//        if self.fetchedProduct != nil {
//            var imageurl1 :String? = nil;
//            
//            if self.fetchedProduct!.custom_attributes != nil {
//                
//                for attr:Attribute in self.fetchedProduct!.custom_attributes! {
//                    if attr.attribute_code == "image" {
//                        imageurl1 = attr.value;
//                        break;
//                    }
//                }
//            }
//            
//            imCEll.ImageVView.kf_setImageWithURL(NSURL(string: imageurl1!)!,
//                                                 placeholderImage: UIImage(named: "placeholder"),
//                                                 optionsInfo: nil,
//                                                 progressBlock: { (receivedSize, totalSize) -> () in
//                                                    
//                                                    debugPrint("Download Progress: \(receivedSize)/\(totalSize)")
//                },
//                                                 completionHandler: { (image, error, cacheType, imageURL) -> () in
//                                                    //  debugPrint("Downloaded and set!")
//                                                    if let im = image
//                                                    {
//                                                        if self.firstCellISUpdated == false
//                                                        {
//                                                            
//                                                            let rowH = self.rowHeightAccordingToImage(im, neww: self.view.frame.size.width);
//                                                            
//                                                           // self.tableView.beginUpdates()
//                                                            self.firstRowHeight = rowH;
//                                                             imCEll.ImageVView.image = im;
//                                                            self.tableView.reloadData();
////                                                            self.tableView.reloadRowsAtIndexPaths([onIndexpath], withRowAnimation: .Automatic)
//                                                            
//                                                           // self.tableView.endUpdates();
//                                                            self.firstCellISUpdated = true
//                                                        }
//                                                        else
//                                                        {
//                                                             imCEll.ImageVView.image = im;
//                                                        }
//                                                    }
//                }
//            )
//
//        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func nonConfigurableSize() -> String? {
        if self.fetchedProduct != nil {
            for attr:Attribute in (self.fetchedProduct?.custom_attributes)! {
                if attr.attribute_code == "size" {
                    return self.getSizeFromNumber(Int(attr.value!)!);
                    
                }
            }
            
        }
        return nil;
    }
    func nonConfigurableSizeNUmber() -> Int? {
        if self.fetchedProduct != nil {
            for attr:Attribute in (self.fetchedProduct?.custom_attributes)! {
                if attr.attribute_code == "size" {
                    return Int(attr.value!)!;
                    
                }
            }
            
        }
        return nil;
    }
    func nonConfigurableColorNumber() -> Int? {
        if self.fetchedProduct != nil {
            for attr:Attribute in (self.fetchedProduct?.custom_attributes)! {
                if attr.attribute_code == "color" {
                    return Int(attr.value!)!;
                    
                }
            }
            
        }
        return nil;
    }

    func nonConfigurableColor() -> UIColor? {
        if self.fetchedProduct != nil {
            for attr:Attribute in (self.fetchedProduct?.custom_attributes)! {
                if attr.attribute_code == "color" {
                    return self.getColorFromNumber(Int(attr.value!)!);
                   
                }
            }
            
        }
        return nil;
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 4203 {
            return pickerDataSource.count;
        }
        else
        {
            if self.fetchedProduct != nil && self.fetchedProduct?.options != nil
            {
                
                let opt:Option = self.fetchedProduct!.options![pickerView.tag];
                return (opt.optionsValues.count);
            }
            else
            {
                return 0;
            }
        }
        
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        currentPicker = pickerView;
        if pickerView.tag == 4203 {
            return pickerDataSource[row]
        }
        else
        {
            if self.fetchedProduct != nil && self.fetchedProduct?.options != nil
            {
                
                let opt:Option = self.fetchedProduct!.options![pickerView.tag];
                let optValue:OptionValue = (opt.optionsValues[row]);
                return optValue.title;
            }
            else
            {
                return "";
            }
        }
        
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        
        if pickerView.tag == 4203 {
           self.hiddenTxtField.text = pickerDataSource[row];
        }
        else
        {
            if self.fetchedProduct != nil && self.fetchedProduct?.options != nil
            {
                
                let opt:Option = self.fetchedProduct!.options![pickerView.tag];
                let optValue:OptionValue = (opt.optionsValues[row]);
                let path:NSIndexPath = NSIndexPath.init(forItem: pickerView.tag+1, inSection: 0)
                let dropCell:DropDownCell = tableView.cellForRowAtIndexPath(path) as! DropDownCell;
                dropCell.dropdownTextField.text = optValue.title;
                opt.selectedValue =  optValue.title;
            }
            else
            {
              
            }
        }

        
    }
    func textField(textField: IQDropDownTextField, didSelectItem item: String?)
    {
        if self.fetchedProduct != nil && self.fetchedProduct?.options != nil
        {
           // let desCell: DropDownCell = cell as! DropDownCell
            let opt:Option = self.fetchedProduct!.options![textField.tag];
            opt.selectedValue =  textField.selectedItem;
            }

    
    }
    func textFieldDidEndEditing(textField: UITextField)
    {
        if self.fetchedProduct != nil && self.fetchedProduct?.options != nil
        {
            
            let opt:Option = self.fetchedProduct!.options![textField.tag ];
            opt.selectedValue =  textField.text;
        }
    
    
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        if textField.isFirstResponder()
        {
            if self.fetchedProduct != nil && self.fetchedProduct?.options != nil
            {
                
                let opt:Option = self.fetchedProduct!.options![textField.tag ];
                opt.selectedValue =  textField.text;
            }
            
        }
    textField.resignFirstResponder()
        
        return true;
    }
    
    
    
    func webViewDidFinishLoad(webView: UIWebView){
        
//        if (self.webViewContentHeight == webView.scrollView.contentSize.height)
//        {
//            // we already know height, no need to reload cell
//            return
//        }
        
        if webView.tag == 420 {
            
        if webView.stringByEvaluatingJavaScriptFromString("document.readyState") ==  "complete"
            && webView.scrollView.contentSize.height - webViewContentHeight > 30 && detailSizeAdjust == true {
            self.webViewContentHeight = webView.scrollView.contentSize.height
            tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: noOFRows!-2, inSection: 0)], withRowAnimation: .Automatic)
            detailSizeAdjust = false
            MagentoHud.dismiss();
            }
        }
        else
        {
            if webView.stringByEvaluatingJavaScriptFromString("document.readyState") ==  "complete" && shippingSizeAdjust == true
                 {//&& webView.scrollView.contentSize.height - shippingWebViewContentHeight > 30
                self.shippingWebViewContentHeight = webView.scrollView.contentSize.height+5
                tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: noOFRows!-1, inSection: 0)], withRowAnimation: .Automatic)
                MagentoHud.dismiss();
                shippingSizeAdjust = false
            }
        
        }
        
        /*let textSize: Int = 240
        webView.stringByEvaluatingJavaScriptFromString("document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '\(textSize)%%'")
        
        if webView.stringByEvaluatingJavaScriptFromString("document.readyState") ==  "complete"  && webView.scrollView.contentSize.height - webViewContentHeight > 30 {
            self.webViewContentHeight = webView.scrollView.contentSize.height+20
            tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: noOFRows!-1, inSection: 0)], withRowAnimation: .Automatic)
        }*/
        
    }

    
    
    func webViewDidStartLoad(webView: UIWebView){
    
    }

    
    func webView(webView: WKWebView, didFinishNavigation navigation: WKNavigation!) {
        if webView.tag == 420 {
            webView.evaluateJavaScript("document.height") { (result, error) in
            if error == nil && self.detailSizeAdjust == true{
                let height: CGFloat = result as? CGFloat ?? 0.0
                if  height - self.webViewContentHeight > 30 {
                    self.webViewContentHeight = height
                    print("---Indexpath %@", self.noOFRows!-2)
                    self.tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: self.noOFRows!-3, inSection: 0)],
                                                          withRowAnimation: .Automatic)
                    self.detailSizeAdjust = false
                }
                //if   webView.scrollView.contentSize.height - self.webViewContentHeight > 30 {
                    //self.webViewContentHeight = result as? CGFloat ?? 0.0 //webView.scrollView.contentSize.height
                    //self.tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow:1, inSection: 0)], withRowAnimation: .Automatic)
                //}
            }
            }
        }
    }
    
    
    func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
        presentViewController(alert, animated: true, completion: nil)
    }
}


extension ProductDetailVC: UICollectionViewDelegate, UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        
        return self.getImageCout();
    }
    
    func collectionView(collectionView: UICollectionView,
                        cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ProductDetailCollectionViewCell",
                                                                         forIndexPath: indexPath)
      
            
            self.configureCollectionCell(cell, andIndexpath: indexPath ,onCollectionView: collectionView)
   
        
        cell.backgroundColor = UIColor.whiteColor()
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        
        
        let estimatedWidth = self.view.frame.size.width;
        var height:CGFloat = 209+36;
     
            if self.firstRowHeight != nil {
                height = firstRowHeight!;
            }
        
        
        return CGSizeMake(estimatedWidth, height)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }
    
    func collectionView(collection							View: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat
    {
        return 0;
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat
    {
        return 0;
    }
    
    
    func configureCollectionCell(cell:UICollectionViewCell, andIndexpath :NSIndexPath, onCollectionView:UICollectionView)  {
        
        let indexpath:NSIndexPath = NSIndexPath.init(forRow: 0, inSection: 0);
        let imCell:ProductDetailimageViewCellTableViewCell? = self.tableView.cellForRowAtIndexPath(indexpath) as? ProductDetailimageViewCellTableViewCell;
        if imCell != nil {
             imCell!.pageControl.currentPage = andIndexpath.row;
        }
       

        let collectionCell:ProductDetailCollectionViewCell = (cell as? ProductDetailCollectionViewCell)!;
        var imgUrl:String? = nil
        if self.fetchedProduct != nil {
            if self.fetchedProduct?.medias?.count > 0 {
                let media:Media = (self.fetchedProduct?.medias![andIndexpath.row])!
                imgUrl = media.file;
            }
            else
            {
                if self.fetchedProduct!.custom_attributes != nil {
                    
                    for attr:Attribute in self.fetchedProduct!.custom_attributes! {
                        if attr.attribute_code == "image" {
                            imgUrl = attr.value;
                            break;
                        }
                    }
                }
            }
        }
        
        if imgUrl != nil {
            
            if self.selectedColorText != nil {
                for colorStr in self.knownColorsArr
                {
                    if imgUrl!.containsString(colorStr) {
                     imgUrl = imgUrl!.stringByReplacingOccurrencesOfString(colorStr, withString: self.selectedColorText!);
                        break;
                    }
                
                }
            }
        
            
                collectionCell.imageView.kf_setImageWithURL(NSURL(string: imgUrl!)!,
                                                            placeholderImage: UIImage(named: "placeholder"),
                                                            optionsInfo: nil,
                                                            progressBlock: { (receivedSize, totalSize) -> () in
                                                                
                                                                debugPrint("Download Progress: \(receivedSize)/\(totalSize)")
                    },
                                                            completionHandler: { (image, error, cacheType, imageURL) -> () in
                                                                //  debugPrint("Downloaded and set!")
                                                                if let im = image
                                                                {
                                                                    
                                                                    if(self.firstCellISUpdated == false)
                                                                    {
                                                                        let rowH = self.rowHeightAccordingToImage(im, neww: self.view.frame.size.width);
                                                                        
                                                                        // self.tableView.beginUpdates()
                                                                        self.firstRowHeight = rowH;
                                                                       collectionCell.imageView.image = im;
                                                                        self.tableView.reloadData();
                                                                        //                                                            self.tableView.reloadRowsAtIndexPaths([onIndexpath], withRowAnimation: .Automatic)
                                                                        
                                                                        // self.tableView.endUpdates();
                                                                        self.firstCellISUpdated = true
                                                                       true;
                                                                    }
                                                                    else
                                                                    {
                                                                        collectionCell.imageView.image = im;
                                                                    }
                                                                    
                                                                    
                                                                    
                                                                }
                    }
                )
                
            
            
            
        }
        
        }
    
    @IBAction func moreReviewsTabbed(sender: AnyObject) {
        
        let vc:ReviewsVc = self.storyboard?.instantiateViewControllerWithIdentifier("ReviewsVc") as! ReviewsVc;
        vc.reviews = self.reviews;
        vc.product = self.fetchedProduct;
        self.navigationController?.pushViewController(vc, animated: true);
    }
}

extension String {
    /**
     Takes the current String struct and strips out HTML using regular expression. All tags get stripped out.
    :returns: String html text as plain text
     */
    
    func stripHTML() -> String {
        var stripHtmlStr =  self.stringByReplacingOccurrencesOfString("<[^>]+>", withString: "", options: NSStringCompareOptions.RegularExpressionSearch, range: nil)
        let htmlENtitie = ["&nbsp;","&gt","&cent;","&pound;","&yen;","&euro;","&copy;","&reg;",];
        
        for str in htmlENtitie {
            stripHtmlStr = stripHtmlStr.stringByReplacingOccurrencesOfString(str, withString: "");
        }
        return stripHtmlStr;
    }
}


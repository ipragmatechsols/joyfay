//
//  HomeVc.swift
//  MagentoShop
//
//  Created by Bunty on 25/06/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import MGNetworkClient
import Kingfisher
import MagentoHud
import MagentoClient




class HomeVc: BaseVc ,UITableViewDataSource,UITableViewDelegate,HomeDualBtnCellDelegate,WomenDualImageDelegate,PopularItemCellDelegate,MyMenuTableViewControllerDelegate,HomeTopCellDelegate
,UISearchBarDelegate{
   var cartButton: ENMBadgedBarButtonItem!
    var wishListButton: ENMBadgedBarButtonItem!
    @IBOutlet var tableView: UITableView!
    var homeObj:Home?;
    let rowHeight:HomeVCRowHeight = HomeVCRowHeight();;
     var storedOffsets = [Int: CGFloat]()
    var topChoiceIsSelected = true;
    var thirdCellISUpdated = false;
    
    var firstCellISUpdated = false;
    var forthCellISUpdated = false;
    var fifthCellISUpdated = false;
    var sixthCellISUpdated = false;
    var seventhCellISUpdated = false;
    var eighthCellISUpdated = false;
    var ninthCellISUpdated = false;
    let searchBar = UISearchBar()
    
   

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Home"
        // Do any additional setup after loading the view.
       // self.configureView();
        AppDelegate.appDelegate.navigationController =  self.navigationController;
        
        let navC:MyNavigationController = self.navigationController as! MyNavigationController
        navC.menuVC?.delegate = self;
        
       self.setTableViewFooter()
        
      
        self.setUpRightBarButton();
       
       // searchBar.barTintColor = Theme.theme().themeColor!
       // searchBar.tintColor = Theme.theme().themeColor!
       // searchBar.backgroundColor = Theme.theme().themeColor!
        searchBar.placeholder = "Search Product"
        searchBar.sizeToFit()
        searchBar.delegate = self;
        navigationItem.titleView = searchBar
        
        self.fetchHomeData();
       
        //}

        
       
    }
    
    
    
      
    
    func setTableViewFooter()  {
    let v  = UIView.init(frame: CGRectMake(0, 0, self.view.frame.size.width, 50));
    v.backgroundColor = Theme.theme().themeColor
        let btnWidth:CGFloat = v.frame.size.width/3;
        let btnHeight:CGFloat = 50;
        
        let hwlpbutton = UIButton(frame: CGRect(x: 0, y: 0, width: btnWidth, height: btnHeight))
        hwlpbutton.backgroundColor = .clearColor()
        hwlpbutton.setImage(UIImage.init(named: "help"), forState: .Normal);
        hwlpbutton.setTitle(" Help", forState: .Normal)
        hwlpbutton.addTarget(self,
                         action: #selector(HomeVc.help),
                         forControlEvents: UIControlEvents.TouchUpInside)

        
        v.addSubview(hwlpbutton)
        
        
        
        let abtbutton = UIButton(frame: CGRect(x: btnWidth, y: 0, width: btnWidth, height: btnHeight))
        abtbutton.backgroundColor = .clearColor()
        abtbutton.setImage(UIImage.init(named: "about"), forState: .Normal);
        abtbutton.setTitle(" About", forState: .Normal)
        abtbutton.addTarget(self,
                             action: #selector(HomeVc.about),
                             forControlEvents: UIControlEvents.TouchUpInside)
        
        
        v.addSubview(abtbutton)
        
        
        let servbutton = UIButton(frame: CGRect(x: btnWidth*2, y: 0, width: btnWidth, height: btnHeight))
        servbutton.backgroundColor = .clearColor()
        servbutton.setImage(UIImage.init(named: "service"), forState: .Normal);
        servbutton.setTitle(" Service", forState: .Normal)
        servbutton.addTarget(self,
                            action: #selector(HomeVc.service),
                            forControlEvents: UIControlEvents.TouchUpInside)
        
        
        v.addSubview(servbutton)

        
        
    self.tableView.tableFooterView = v;
    }
    
    
    func help()  {
                let vc:WEbVc = self.storyboard?.instantiateViewControllerWithIdentifier("WEbVc") as! WEbVc;
        vc.from = "Help"
                self.navigationController?.pushViewController(vc, animated: true);
        
    }
    func about()  {
        let vc:WEbVc = self.storyboard?.instantiateViewControllerWithIdentifier("WEbVc") as! WEbVc;
        vc.from = "About Us"
        self.navigationController?.pushViewController(vc, animated: true);
    }
    
    func service()  {
        if let url = NSURL(string: "tel://+18002094177") {
            UIApplication.sharedApplication().openURL(url)
        }
        
        
        
    }
    //MARK: Search Bar Delegate
    func searchBarSearchButtonClicked(searchBar: UISearchBar)
    {
    searchBar.resignFirstResponder()
        if searchBar.text?.characters.count > 0 {
            let vc:ProductListVc = self.storyboard?.instantiateViewControllerWithIdentifier("ProductListVc") as!ProductListVc
            vc.fromSearchBar = true;
            vc.searchText = searchBar.text;
            let navC:UINavigationController = sideMenuController() as! UINavigationController;
            navC.pushViewController(vc, animated: true);

        }
        
    }
   func searchBarCancelButtonClicked(searchBar: UISearchBar)
    {
     searchBar.resignFirstResponder()
    }
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        // The user clicked the [X] button or otherwise cleared the text.
        if (searchText.characters.count == 0) {
            searchBar.performSelector(#selector(UIResponder.resignFirstResponder), withObject: nil, afterDelay: 0.1)
        }
    }
    func setUpRightBarButton() {
        let image = UIImage(named: "cart")
        let button = UIButton(type: .Custom)
        if let knownImage = image {
            button.frame = CGRectMake(0.0, 0.0, knownImage.size.width, knownImage.size.height)
        } else {
            button.frame = CGRectZero;
        }
        
        button.setBackgroundImage(image, forState: UIControlState.Normal)
        button.addTarget(self,
                         action: #selector(HomeVc.cartAction(_:)),
                         forControlEvents: UIControlEvents.TouchUpInside)
        
        let newBarButton = ENMBadgedBarButtonItem(customView: button, value: "")
        cartButton = newBarButton
        
        
        let image1 = UIImage(named: "wishlist")
        let button1 = UIButton(type: .Custom)
        if let knownImage1 = image1 {
            button1.frame = CGRectMake(0.0, 0.0, knownImage1.size.width+10, knownImage1.size.height)
        } else {
            button1.frame = CGRectZero;
        }
        button1.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Right
          
        button1.setImage(image1, forState: UIControlState.Normal)
        button1.addTarget(self,
                         action: #selector(HomeVc.wishListAction(_:)),
                         forControlEvents: UIControlEvents.TouchUpInside)
        
        let newBarButton1 = ENMBadgedBarButtonItem(customView: button1, value: "")
        wishListButton = newBarButton1
        
        navigationItem.rightBarButtonItems = [wishListButton , cartButton]
        
        
    }
    func countIsCleared()
    {
     self.cartButton.badgeValue = "";
        self.wishListButton.badgeValue = "";

    }
   
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated);
        hideSideMenuView()
    }
 override  func updateCartCount()  {
    super.updateCartCount()
    self.cartButton.badgeValue = self.cartCount!;
        
    }
    
    
  override func updateWishListCount()
    {
         super.updateWishListCount()
         self.wishListButton.badgeValue = self.wishListCount!;
        
    
    }
    func configureView()  {
        tableView.dataSource = self;
        tableView.delegate = self;
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        let indexpath = NSIndexPath.init(forRow: 0, inSection: 0);
//        let homeCell:HomeTopCell = self.tableView.cellForRowAtIndexPath(indexpath) as! HomeTopCell;
 
       
    }
    @IBAction func menuAction(sender: AnyObject) {
        toggleSideMenuView()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true);
        searchBar.text = "";
        let navC:MyNavigationController = self.navigationController as! MyNavigationController
        navC.menuVC?.reloadTable({
            
        });
//         self.fetchHomeData();
        if self.cartCount == nil {
            self.cartCount = "";
        }
        self.getCartItems();
        self.getWishListItems()
        self.cartButton.badgeValue = self.cartCount!;
        self.wishListButton.badgeValue = self.wishListCount!;

        self.showNavigationBar();
           }

    
    func fetchHomeData()  {
          MagentoHud.show();
        MGNetworkClient.sharedClient.getBanners { (error:NSError?, home:Home?) in
           MagentoHud.dismiss()
            if (error == nil )
            {
            if let h = home
            {
               
                self.homeObj = h;
                self.tableView.reloadData();
                
            }
            
            }
            
        }
    }
    
    
    //MARK: - TableViewDataSource
    
    @IBAction func MenuAction(sender: AnyObject) {
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 10;
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell:UITableViewCell!
        
        
        switch indexPath.row {
        case 0:
            cell = tableView.dequeueReusableCellWithIdentifier("HomeTopCell", forIndexPath: indexPath) as! HomeTopCell
            self.configureHomeTopCell(cell,andIndexpath: indexPath);
        case 1:
            cell = tableView.dequeueReusableCellWithIdentifier("HomeDualBtnCell", forIndexPath: indexPath) as! HomeDualBtnCell
            let hdbcCell:HomeDualBtnCell = cell as! HomeDualBtnCell;
            hdbcCell.delegate = self;
            

        case 2:
            cell = tableView.dequeueReusableCellWithIdentifier("MenDualImageCell", forIndexPath: indexPath) as! MenDualImageCell
        case 3:
            cell = tableView.dequeueReusableCellWithIdentifier("WomenSingleCell", forIndexPath: indexPath) as! WomenSingleCell
            self.configureWomenSingleCell(cell, andIndexpath: indexPath)
        case 4:
            cell = tableView.dequeueReusableCellWithIdentifier("WomenDualImageCell", forIndexPath: indexPath) as! WomenDualImageCell
            self.configureWomenDualCell(cell, andIndexpath: indexPath);
        case 5:
            cell = tableView.dequeueReusableCellWithIdentifier("WomenSingleCell", forIndexPath: indexPath) as! WomenSingleCell
              self.configureMenSingleCell(cell, andIndexpath: indexPath)
        case 6:
            cell = tableView.dequeueReusableCellWithIdentifier("WomenDualImageCell", forIndexPath: indexPath) as! WomenDualImageCell
            self.configureMenDualCell(cell, andIndexpath: indexPath);
        case 7:
            cell = tableView.dequeueReusableCellWithIdentifier("newProductsCell", forIndexPath: indexPath)
        case 8:
            cell = tableView.dequeueReusableCellWithIdentifier("MenDualImageCell", forIndexPath: indexPath) as! MenDualImageCell
        case 9:
            cell = tableView.dequeueReusableCellWithIdentifier("PopularItemCell", forIndexPath: indexPath) as! PopularItemCell
            self.configurePopularCell(cell, andIndexpath: indexPath);
        default:
            cell = tableView.dequeueReusableCellWithIdentifier("DescriptionViewCell", forIndexPath: indexPath) as! DescriptionViewCell
            
        }
        
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        
        return cell;
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        
        var height:CGFloat! ;
        switch indexPath.row {
        case 0:
            if self.rowHeight.firstRow != nil {
                height = rowHeight.firstRow
            }
            else
            {
            height = 209;
            }
        case 1:
            height = 84;
        case 2:
            if self.rowHeight.thirdRow != nil {
                height = rowHeight.thirdRow! + CGFloat(36) + CGFloat(10)
            }
            else
            {
                height = 209;
            }
        case 3:
            if self.rowHeight.fourthRow != nil {
                height = rowHeight.fourthRow!
            }
            else
            {
                height = 209;
            }
        case 4:
            if self.rowHeight.fifthRow != nil {
                height = rowHeight.fifthRow!
            }
            else
            {
                height = 209;
            }
        case 5:
           // height = 0;
            if self.rowHeight.sixthRow != nil {
                height = rowHeight.sixthRow!
            }
            else
            {
                height = 209;
            }
        case 6:
             //  height = 0;
           if self.rowHeight.seventhRow != nil {
                height = rowHeight.seventhRow!
            }
            else
            {
                height = 209;
            }
        case 7:
           
                height = 40;
          
            
        case 8:
            if self.rowHeight.ninthRow != nil {
                height = rowHeight.ninthRow! + CGFloat(36) + CGFloat(10)
            }
            else
            {
                height = 209;
            }

        case 9:
            
            height = 209;
            

            
            
            
        default:
            height = 100;
        }
        
        
        return height;
    }
      func configurePopularCell(cell:UITableViewCell, andIndexpath :NSIndexPath)  {
         let popularCell:PopularItemCell = cell as! PopularItemCell;
        popularCell.delegate = self;
        if let h = self.homeObj {
            let menu = h.popularMenus![0];
            
            let b1 = menu.menus![0];
             let b2 = menu.menus![1];
             let b3 = menu.menus![2];
             let b4 = menu.menus![3];
            popularCell.btn1.setTitle(b1.name, forState:UIControlState.Normal);
             popularCell.btn2.setTitle(b2.name, forState:UIControlState.Normal);
             popularCell.btn3.setTitle(b3.name, forState:UIControlState.Normal);
             popularCell.btn4.setTitle(b4.name, forState:UIControlState.Normal);
            
            
            
        }
        
    }
    
    
    
    
    func configureWomenSingleCell(cell:UITableViewCell, andIndexpath :NSIndexPath)  {
           let womenCell:WomenSingleCell = cell as! WomenSingleCell;
          womenCell.headingTitleLbl.text = "";
        if let h = self.homeObj {
            if h.categoryBanners?.count > 0 {
                let cat:MGCategory = h.categoryBanners![0]
                womenCell.headingTitleLbl.text = cat.name!
                womenCell.bannerImageView.kf_setImageWithURL(NSURL(string: cat.imageUrl!)!,
                                                             placeholderImage: UIImage(named: "placeholder"),
                                                             optionsInfo: nil,
                                                             progressBlock: { (receivedSize, totalSize) -> () in
                                                                
                                                                //debugPrint("Download Progress: \(receivedSize)/\(totalSize)")
                    },
                                                             completionHandler: { (image, error, cacheType, imageURL) -> () in
                                                                //  debugPrint("Downloaded and set!")
                                                                if let im = image
                                                                {
                                                                    if self.forthCellISUpdated == false
                                                                    {
                                                                        
                                                                        let rowH = self.rowHeightAccordingToImage(im, neww: self.view.frame.size.width);
                                                                        womenCell.bannerImageView.image = im;
                                                                        self.tableView.beginUpdates()
                                                                        self.rowHeight.fourthRow = rowH;
                                                                        self.tableView.reloadRowsAtIndexPaths([andIndexpath], withRowAnimation: .Automatic)
                                                                        
                                                                        self.tableView.endUpdates();
                                                                        self.forthCellISUpdated = true
                                                                    }
                                                                    else
                                                                    {
                                                                        womenCell.bannerImageView.image = im;
                                                                    }
                                                                }
                    }
                )
                
            }
        }

    }
    
    func configureMenSingleCell(cell:UITableViewCell, andIndexpath :NSIndexPath)  {
        let womenCell:WomenSingleCell = cell as! WomenSingleCell;
        womenCell.headingTitleLbl.text = "";
        if let h = self.homeObj {
            if h.categoryBanners?.count > 1 {
                let cat:MGCategory = h.categoryBanners![1]
                womenCell.headingTitleLbl.text = cat.name!;
                
                womenCell.bannerImageView.kf_setImageWithURL(NSURL(string: cat.imageUrl!)!,
                                                             placeholderImage: UIImage(named: "placeholder"),
                                                             optionsInfo: nil,
                                                             progressBlock: { (receivedSize, totalSize) -> () in
                                                                
                                                                //debugPrint("Download Progress: \(receivedSize)/\(totalSize)")
                    },
                                                             completionHandler: { (image, error, cacheType, imageURL) -> () in
                                                                //  debugPrint("Downloaded and set!")
                                                                if let im = image
                                                                {
                                                                    if self.sixthCellISUpdated == false
                                                                    {
                                                                        
                                                                        let rowH = self.rowHeightAccordingToImage(im, neww: self.view.frame.size.width);
                                                                        womenCell.bannerImageView.image = im;
                                                                        self.tableView.beginUpdates()
                                                                        self.rowHeight.sixthRow = rowH;
                                                                        self.tableView.reloadRowsAtIndexPaths([andIndexpath], withRowAnimation: .Automatic)
                                                                        
                                                                        self.tableView.endUpdates();
                                                                        self.sixthCellISUpdated = true
                                                                    }
                                                                    else
                                                                    {
                                                                        womenCell.bannerImageView.image = im;
                                                                    }
                                                                }
                    }
                )
                
            }
        }
        
    }
    
    func configureWomenDualCell(cell:UITableViewCell, andIndexpath :NSIndexPath)  {
        let womenCell:WomenDualImageCell = cell as! WomenDualImageCell;
        
        womenCell.delegate = self;
        womenCell.firstControlView.tag = andIndexpath.row;
        womenCell.seconControlView.tag = andIndexpath.row;
        if let h = self.homeObj {
            if h.categoryBanners?.count > 0 {
                let cat:MGCategory = h.categoryBanners![0]
                if cat.subCategories?.count > 1 {
                
                    let b:Banner = cat.subCategories![0];
                    let b1:Banner = cat.subCategories![1];
                    womenCell.firstHeadingLbl.text = b.name;
                    womenCell.secondHeadingLbl.text = b1.name;
                    if b1.imageUrl != nil {
                        womenCell.secondImageView.kf_setImageWithURL(NSURL(string: b1.imageUrl!)!,
                                                                     placeholderImage: UIImage(named: "placeholder"),
                                                                     optionsInfo: nil,
                                                                     progressBlock: { (receivedSize, totalSize) -> () in
                                                                        
                                                                       // debugPrint("Download Progress: \(receivedSize)/\(totalSize)")
                            },
                                                                     completionHandler: { (image, error, cacheType, imageURL) -> () in
                                                                        //  debugPrint("Downloaded and set!")
                                                                        if let im = image
                                                                        {
                                                                            if self.fifthCellISUpdated == false
                                                                            {
                                                                                
                                                                                let rowH = self.rowHeightAccordingToImage(im, neww: (self.view.frame.size.width/2)-7);
                                                                                womenCell.secondImageView.image = im;
                                                                                self.tableView.beginUpdates()
                                                                                self.rowHeight.fifthRow = rowH+10;
                                                                                self.tableView.reloadRowsAtIndexPaths([andIndexpath], withRowAnimation: .Automatic)
                                                                                
                                                                                self.tableView.endUpdates();
                                                                                self.fifthCellISUpdated = true
                                                                            }
                                                                            else
                                                                            {
                                                                                womenCell.secondImageView.image = im;
                                                                            }
                                                                        }
                            }
                        )
                    }
               if b.imageUrl != nil {

                     womenCell.firstIMageView.kf_setImageWithURL(NSURL(string: b.imageUrl!)!,placeholderImage:UIImage(named: "placeholder") );
                    }
                }
                
            }
        }
        
    }
    
    func configureMenDualCell(cell:UITableViewCell, andIndexpath :NSIndexPath)  {
        let womenCell:WomenDualImageCell = cell as! WomenDualImageCell;
        womenCell.delegate = self;
        womenCell.firstControlView.tag = andIndexpath.row;
        womenCell.seconControlView.tag = andIndexpath.row;
        
        if let h = self.homeObj {
            if h.categoryBanners?.count > 1 {
                let cat:MGCategory = h.categoryBanners![1]
                if cat.subCategories?.count > 1 {
                    
                    let b:Banner = cat.subCategories![0];
                    let b1:Banner = cat.subCategories![1];
                    
                    womenCell.firstHeadingLbl.text = b.name;
                     womenCell.secondHeadingLbl.text = b1.name;
                    womenCell.secondImageView.kf_setImageWithURL(NSURL(string: b1.imageUrl!)!,
                                                                 placeholderImage: UIImage(named: "placeholder"),
                                                                 optionsInfo: nil,
                                                                 progressBlock: { (receivedSize, totalSize) -> () in
                                                                    
                                                                    debugPrint("Download Progress: \(receivedSize)/\(totalSize)")
                        },
                                                                 completionHandler: { (image, error, cacheType, imageURL) -> () in
                                                                    //  debugPrint("Downloaded and set!")
                                                                    if let im = image
                                                                    {
                                                                        if self.seventhCellISUpdated == false
                                                                        {
                                                                            
                                                                            let rowH = self.rowHeightAccordingToImage(im, neww: (self.view.frame.size.width/2)-7);
                                                                            womenCell.secondImageView.image = im;
                                                                            self.tableView.beginUpdates()
                                                                            self.rowHeight.seventhRow = rowH+10;
                                                                            self.tableView.reloadRowsAtIndexPaths([andIndexpath], withRowAnimation: .Automatic)
                                                                            
                                                                            self.tableView.endUpdates();
                                                                            self.seventhCellISUpdated = true
                                                                        }
                                                                        else
                                                                        {
                                                                            womenCell.secondImageView.image = im;
                                                                        }
                                                                    }
                        }
                    )
                    womenCell.firstIMageView.kf_setImageWithURL(NSURL(string: b.imageUrl!)!,placeholderImage:UIImage(named: "placeholder") );
                }
                
            }
        }
        
    }
    
    func firstControlTapped(sender: UIButton)
    {
        if sender.tag == 4 {
            if let h = self.homeObj {
                if h.categoryBanners?.count > 0 {
                    let cat:MGCategory = h.categoryBanners![0]
                    if cat.subCategories?.count > 1 {
                        
                        let b:Banner = cat.subCategories![1];
                        //self.movetoProductListWithCatID(Int(b.id!), title: b.name)
                        let intArray = b.subcategory_children!.map{Int($0)!}
                        self.movetoProductListWithCatIDs(intArray,title:b.name);
                       // let b1:Banner = cat.subCategories![1];
                    }
                }
            }
        }
        else
        {
            if let h = self.homeObj {
                if h.categoryBanners?.count > 1 {
                    let cat:MGCategory = h.categoryBanners![1]
                    if cat.subCategories?.count > 1 {
                        
                        let b:Banner = cat.subCategories![1];
                        // self.movetoProductListWithCatID(Int(b.id!), title: b.name)
                        //let b1:Banner = cat.subCategories![1];
                        let intArray = b.subcategory_children!.map{Int($0)!}
                        self.movetoProductListWithCatIDs(intArray,title:b.name);

                    }
                }
            }
        }
        
    }
    func secondControlTapped(sender: UIButton)
    {
        if sender.tag == 4 {
            if let h = self.homeObj {
                if h.categoryBanners?.count > 0 {
                    let cat:MGCategory = h.categoryBanners![0]
                    if cat.subCategories?.count > 0 {
                        
                        //let b:Banner = cat.subCategories![0];
                         let b:Banner = cat.subCategories![0];

                        //self.movetoProductListWithCatID(Int(b.id!), title: b.name)
                        let intArray = b.subcategory_children!.map{Int($0)!}
                        self.movetoProductListWithCatIDs(intArray,title:b.name);

                                           }
                }
            }
        }
        else
        {
            if let h = self.homeObj {
                if h.categoryBanners?.count > 1 {
                    let cat:MGCategory = h.categoryBanners![1]
                    if cat.subCategories?.count > 0 {
                        
                        //let b:Banner = cat.subCategories![0];
                         let b:Banner = cat.subCategories![0];
                        //self.movetoProductListWithCatID(Int(b.id!), title: b.name)
                        let intArray = b.subcategory_children!.map{Int($0)!}
                        self.movetoProductListWithCatIDs(intArray,title:b.name);

                       
                    }
                }
            }
        }
    }
    
    func configureHomeTopCell(cell:UITableViewCell, andIndexpath :NSIndexPath)  {
        let homeCell:HomeTopCell = cell as! HomeTopCell;
        homeCell.delegate = self;
        if let h = self.homeObj {
            
            
            let b: Banner = h.mainbanners![0];
            let b1: Banner = h.mainbanners![1];
            let b2: Banner = h.mainbanners![2];
            homeCell.bannerImageView.kf_setImageWithURL(NSURL(string: b.imageUrl!)!,
                                                        placeholderImage: UIImage(named: "placeholder"),
                                                        optionsInfo: nil,
                                                        progressBlock: { (receivedSize, totalSize) -> () in
                                                            
                                                            debugPrint("Download Progress: \(receivedSize)/\(totalSize)")
                },
                                                        completionHandler: { (image, error, cacheType, imageURL) -> () in
                                                            //  debugPrint("Downloaded and set!")
                                                            if let im = image
                                                            {
                                                                if self.firstCellISUpdated == false
                                                                {
                                                                    
                                                                    let rowH = self.rowHeightAccordingToImage(im, neww: self.view.frame.size.width);
                                                                    homeCell.bannerImageView.image = im;
                                                                    self.tableView.beginUpdates()
                                                                    self.rowHeight.firstRow = rowH;
                                                                    self.tableView.reloadRowsAtIndexPaths([andIndexpath], withRowAnimation: .Automatic)
                                                                    
                                                                    self.tableView.endUpdates();
                                                                    self.firstCellISUpdated = true
                                                                }
                                                                else
                                                                {
                                                                    homeCell.bannerImageView.image = im;
                                                                }
                                                            }
                }
            )
            
            homeCell.bannerImageView2.kf_setImageWithURL(NSURL(string: b1.imageUrl!)!,placeholderImage:UIImage(named: "placeholder") );
            homeCell.bannerImageView3.kf_setImageWithURL(NSURL(string: b2.imageUrl!)!,placeholderImage:UIImage(named: "placeholder"));
        }
        
    }

    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        switch indexPath.row {
        case 0: break
            
        case 1: break
          
        case 2:
           break
            
        case 3:
        if let h = self.homeObj {
            //if h.categoryBanners?.count > 1 {
                if h.categoryBanners?.count > 0 {
                    let cat:MGCategory = h.categoryBanners![0]
                     //self.movetoProductListWithCatID(23,title:cat.name);
                    let intArray = cat.categoryChildrens!.map{Int($0)!}
                    self.movetoProductListWithCatIDs(intArray,title:cat.name);
                }
            //}
            }
case 4: break
           
        case 5:
            if let h = self.homeObj {
            if h.categoryBanners?.count > 1 {
                let cat:MGCategory = h.categoryBanners![1]
                //self.movetoProductListWithCatID(15,title:cat.name);
                // self.movetoProductListWithCatID(Int(cat.id!),title:cat.name);
                let intArray = cat.categoryChildrens!.map{Int($0)!}
                self.movetoProductListWithCatIDs(intArray,title:cat.name);
                }
            }
                   case 6: break
           
        case 7: break
            
            
        case 8: break
            
        case 9: break
            
         
            
            
            
            
            
        default: break
           
        }
        

    }
    
    
    func moveToProductDetail(sku:String){
        let vc:ProductDetailVC = self.storyboard?.instantiateViewControllerWithIdentifier("ProductDetailVC") as! ProductDetailVC;
        vc.skuID = sku;
        self.navigationController?.pushViewController(vc, animated: true);
    }
    func movetoProductListWithCatID(catID:Int?,title:String?)  {
        let vc:ProductListVc = self.storyboard?.instantiateViewControllerWithIdentifier("ProductListVc") as!ProductListVc
        vc.catID = catID;
        vc.titleName = title;
        let navC:UINavigationController = sideMenuController() as! UINavigationController;
        navC.pushViewController(vc, animated: true);

    }
    func movetoProductListWithCatIDs(catIDs:[Int],title:String?)  {
        let vc:ProductListVc = self.storyboard?.instantiateViewControllerWithIdentifier("ProductListVc") as!ProductListVc
        vc.catIDs = catIDs;
        vc.titleName = title;
        let navC:UINavigationController = sideMenuController() as! UINavigationController;
        navC.pushViewController(vc, animated: true);
        
    }
     func tableView(tableView: UITableView,
                            willDisplayCell cell: UITableViewCell,
                                            forRowAtIndexPath indexPath: NSIndexPath) {
        
        guard let tableViewCell:MenDualImageCell = cell as? MenDualImageCell else { return }
        
        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }
    
    func tableView(tableView: UITableView,
                            didEndDisplayingCell cell: UITableViewCell,
                                                 forRowAtIndexPath indexPath: NSIndexPath) {
        
        guard let tableViewCell:MenDualImageCell = cell as? MenDualImageCell else { return }
        storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func topCellTappedWithIndex(index: Int)
    {
     if let h = self.homeObj {
    let b: Banner = h.mainbanners![index];
        if b.sku != nil {
            self.moveToProductDetail(b.sku!);
        }
        
        
        }
    }
    func topBrandsTapped(sender: UIButton)
    {
        let indexpath = NSIndexPath.init(forRow: 2, inSection: 0);
        self.topChoiceIsSelected = false;
        let cell:MenDualImageCell = self.tableView.cellForRowAtIndexPath(indexpath) as! MenDualImageCell;
        cell.collectionView.reloadData();
        
    }
    func topChoiceTapped(sender: UIButton)
    {
        let indexpath = NSIndexPath.init(forRow: 2, inSection: 0);
        self.topChoiceIsSelected = true;
        let cell:MenDualImageCell = self.tableView.cellForRowAtIndexPath(indexpath) as! MenDualImageCell;
        cell.collectionView.reloadData();
    
    }
    
    func popularFirstBtnTapped(sender: UIButton)
    {
        if let h = self.homeObj {
            let menu = h.popularMenus![0];
            
            let b = menu.menus![0];
          
             //self.movetoProductListWithCatID(Int(b.id!), title: b.name);
            let intArray = b.categoryChildrens!.map{Int($0)!}
            self.movetoProductListWithCatIDs(intArray,title:b.name);
            
            
            
        }

    }
    func popularsecondBtnTapped(sender: UIButton){
    
        if let h = self.homeObj {
            let menu = h.popularMenus![0];
            
     
            let b = menu.menus![1];
        
           // self.movetoProductListWithCatID(Int(b.id!), title: b.name);
            let intArray = b.categoryChildrens!.map{Int($0)!}
            self.movetoProductListWithCatIDs(intArray,title:b.name);
            

            
            
            
            
        }}
    func popularThirdBtnTapped(sender: UIButton){
    
        if let h = self.homeObj {
            let menu = h.popularMenus![0];
            
           
            let b = menu.menus![2];
           
            //self.movetoProductListWithCatID(Int(b.id!), title: b.name);
            let intArray = b.categoryChildrens!.map{Int($0)!}
            self.movetoProductListWithCatIDs(intArray,title:b.name);
            

            
            
            
            
        }}
    func popularFourthBtnTapped(sender: UIButton){
    
        if let h = self.homeObj {
            let menu = h.popularMenus![0];
            
        
            let b = menu.menus![3];
           // self.movetoProductListWithCatID(Int(b.id!), title: b.name);
            let intArray = b.categoryChildrens!.map{Int($0)!}
            self.movetoProductListWithCatIDs(intArray,title:b.name);
            

            
            
            
            
        }}

}


public class HomeVCRowHeight {
    
    public  var firstRow: CGFloat?
     public  var secondRow: CGFloat?
     public  var thirdRow: CGFloat?
    public  var thirdRowWIdth: CGFloat?
     public  var fourthRow: CGFloat?
      public  var fifthRow: CGFloat?
     public  var sixthRow: CGFloat?
     public  var seventhRow: CGFloat?
     public  var eighthRow: CGFloat?
    public  var ninthRow: CGFloat?
   
}
extension HomeVc: UICollectionViewDelegate, UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.tag == 8 {
             if (self.homeObj != nil && self.homeObj?.newProducts?.count > 0)
             {
                return (self.homeObj?.newProducts?.count)!;
            }
            else
             {
                return 2;
            }
            
        }
        else
        {
        if (self.homeObj != nil &&  self.homeObj?.topChoiceBanners?.count > 0) {
            let topCh =  self.homeObj?.topChoiceBanners?[0];
            if  (self.topChoiceIsSelected == true) {
                return (topCh?.topChoices?.count)!;
            }
            else
            {
            return  (topCh?.features?.count)!;
            }
        }
        else
        {
            return 2
        }
        }
    }
    
    func collectionView(collectionView: UICollectionView,
                        cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("HomeCollectionViewCell",
                                                                         forIndexPath: indexPath)
        
         if collectionView.tag == 8 {
       self.configureProductCollectionCell(cell, andIndexpath: indexPath ,onCollectionView: collectionView)
        }
        else
         {
        
             self.configureCollectionCell(cell, andIndexpath: indexPath ,onCollectionView: collectionView)
        }
        
        cell.backgroundColor = UIColor.whiteColor()
       
        return cell
    }
    
     func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
     {
        
            
        let estimatedWidth = (self.view.frame.size.width - 20)/2;
        var height:CGFloat = 209+36;
        if collectionView.tag == 8 {
        if self.rowHeight.ninthRow != nil {
            height = rowHeight.ninthRow! + CGFloat(36);
        }
        }
        else
        {
            if self.rowHeight.thirdRow != nil {
                height = rowHeight.thirdRow! + CGFloat(36);
            }
        }
        
    return CGSizeMake(estimatedWidth, height)
    }
    
      func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets
     {
        return UIEdgeInsetsMake(5, 5, 5, 5);
    }
  
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat
    {
        return 5;
    }
    
      func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat
     {
    return 5;
    }
   
    
    func configureCollectionCell(cell:UICollectionViewCell, andIndexpath :NSIndexPath, onCollectionView:UICollectionView)  {
        
        let collectionCell:HomeCollectionViewCell = (cell as? HomeCollectionViewCell)!;
        if let h = self.homeObj {
            let topCh =  h.topChoiceBanners?[0];
            var b: Banner!;
            if  (self.topChoiceIsSelected == true ) {
               
                
                b = (topCh?.topChoices![andIndexpath.row])!;
            }
            else
            {
             b = (topCh?.features![andIndexpath.row])!;
            }
            collectionCell.descLabel.text = b.name;
                if andIndexpath.row == 0 {
                    collectionCell.imageView.kf_setImageWithURL(NSURL(string: b.imageUrl!)!,
                                                                placeholderImage: UIImage(named: "placeholder"),
                                                                optionsInfo: nil,
                                                                progressBlock: { (receivedSize, totalSize) -> () in
                                                                    
                                                                    debugPrint("Download Progress: \(receivedSize)/\(totalSize)")
                        },
                                                                completionHandler: { (image, error, cacheType, imageURL) -> () in
                                                                    //  debugPrint("Downloaded and set!")
                                                                    if let im = image
                                                                    {
                                                                        
                                                                        if(self.thirdCellISUpdated == false)
                                                                        {
                                                                            let estimatedWidth = (self.view.frame.size.width - 20)/2;
                                                                            let rowH = self.rowHeightAccordingToImage(im,neww:estimatedWidth );
                                                                            collectionCell.imageView.image = im;
                                                                            self.tableView.beginUpdates()
                                                                            self.rowHeight.thirdRow = rowH;
                                                                             let indexPath = NSIndexPath.init(forRow:onCollectionView.tag, inSection: 0);self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
                                                                            
                                                                            self.tableView.endUpdates();
                                                                       // onCollectionView.reloadData();
                                                                        self.thirdCellISUpdated = true;
                                                                    }
                                                                        else
                                                                        {
                                                                           collectionCell.imageView.image = im;
                                                                        }
                                                                    
                                                                        
                                                                        
                                                                    }
                        }
                    )

                }
                else
                {
                      collectionCell.imageView.kf_setImageWithURL(NSURL(string: b.imageUrl!)!,placeholderImage:UIImage(named: "placeholder"));
                }
                
            
            
          
        }
        
    }
    
    func configureProductCollectionCell(cell:UICollectionViewCell, andIndexpath :NSIndexPath, onCollectionView:UICollectionView)  {
        
        let collectionCell:HomeCollectionViewCell = (cell as? HomeCollectionViewCell)!;
        if let h = self.homeObj {
           
            var b: Banner!
           
            
                
                b = (h.newProducts![andIndexpath.row]);
           
                        collectionCell.descLabel.text = b.name;
            if andIndexpath.row == 0 {
                collectionCell.imageView.kf_setImageWithURL(NSURL(string: b.imageUrl!)!,
                                                            placeholderImage: UIImage(named: "placeholder"),
                                                            optionsInfo: nil,
                                                            progressBlock: { (receivedSize, totalSize) -> () in
                                                                
                                                                debugPrint("Download Progress: \(receivedSize)/\(totalSize)")
                    },
                                                            completionHandler: { (image, error, cacheType, imageURL) -> () in
                                                                //  debugPrint("Downloaded and set!")
                                                                if let im = image
                                                                {
                                                                    
                                                                    if(self.ninthCellISUpdated == false)
                                                                    {
                                                                        let estimatedWidth = (self.view.frame.size.width - 20)/2;
                                                                        let rowH = self.rowHeightAccordingToImage(im,neww:estimatedWidth );
                                                                        collectionCell.imageView.image = im;
                                                                        self.tableView.beginUpdates()
                                                                        self.rowHeight.ninthRow = rowH;
                                                                        let indexPath = NSIndexPath.init(forRow:onCollectionView.tag, inSection: 0);
                                                                        self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
                                                                        
                                                                        self.tableView.endUpdates();
                                                                       // onCollectionView.reloadData();
                                                                        self.ninthCellISUpdated = true;
                                                                    }
                                                                    else
                                                                    {
                                                                        collectionCell.imageView.image = im;
                                                                    }
                                                                    
                                                                    
                                                                    
                                                                }
                    }
                )
                
            }
            else
            {
                collectionCell.imageView.kf_setImageWithURL(NSURL(string: b.imageUrl!)!,placeholderImage:UIImage(named: "placeholder"));
            }
            
            
            
            
        }
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        if collectionView.tag == 8
        {
            if let h = self.homeObj {
                
                var b: Banner!
                
                
                
                b = (h.newProducts![indexPath.row]);
               
                self.moveToProductDetail(b.sku!);
            }

           
          
        }
        else
        {
            if let h = self.homeObj {
                let topCh =  h.topChoiceBanners?[0];
                var b: Banner!;
                if  (self.topChoiceIsSelected == true ) {
                    
                    
                    b = (topCh?.topChoices![indexPath.row])!;
                }
                else
                {
                    b = (topCh?.features![indexPath.row])!;
                }
                
                self.moveToProductDetail(b.sku!);
            }
        }
    
    
    }
    
    
}
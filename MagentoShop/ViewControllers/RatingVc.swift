//
//  RatingVc.swift
//  MagentoShop
//
//  Created by vikaskumar on 21/12/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import MGNetworkClient
import MagentoClient
import MagentoHud

class RatingVc: BaseVc,UITextFieldDelegate,UITextViewDelegate {
    @IBOutlet weak var nameTxtFld: UITextField!
    @IBOutlet weak var titleTxtFld: UITextField!
    @IBOutlet weak var cmntTxtView: UITextView!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var submitBtn: UIButton!
     var product: Product?
    var placeholderLabel : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.product != nil {
            self.title = product!.name!;
        }
        self.nameTxtFld.delegate = self;
        self.titleTxtFld.delegate = self;
        self.cmntTxtView.delegate = self;
        
        placeholderLabel = UILabel()
        placeholderLabel.text = "Comment"
        placeholderLabel.font = UIFont.italicSystemFontOfSize(cmntTxtView.font!.pointSize)
        placeholderLabel.sizeToFit()
        cmntTxtView.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPointMake(5, cmntTxtView.font!.pointSize / 2)
        placeholderLabel.textColor = UIColor(white: 0, alpha: 0.3)
        placeholderLabel.hidden = !cmntTxtView.text.isEmpty
        // Do any additional setup after loading the view.
        
        self.submitBtn.layer.cornerRadius = 5.0;
        self.submitBtn.clipsToBounds = true;
        self.submitBtn.backgroundColor = Theme.theme().themeColor
    }
    func textViewDidChange(textView: UITextView) {
        placeholderLabel.hidden = !textView.text.isEmpty
    }

    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true;
    }
    @IBAction func submitBtnAction(sender: AnyObject) {
        
        if self.validateFields() {
            
            MagentoHud.show();
            let userText = nameTxtFld.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet());
            let passText = titleTxtFld.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet());
            let commentText = cmntTxtView.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet());
           
            
            
            
            

let params = ["additionalProperties":"{}" as AnyObject,"detail":commentText as! AnyObject,"nickname":userText as! AnyObject,"productId":"\(self.product!.id!)" as AnyObject,"title":passText as! AnyObject,"ratingValue":"\(ratingView.rating)" as AnyObject,"rating_votes":"" as AnyObject ,"customer_id":"\(User.fetchSavedUser().id!)" as AnyObject];
           
            MGNetworkClient.sharedClient.addReviewToItem(params, completion: { (er:NSError?, cart:Bool?) in
                
                MagentoHud.dismiss();
                if(er == nil)
                {
                    MGNetworkClient.sharedClient.showSuccessALertWithMessage("Thanks for reviewing the product")
                    self.navigationController?.popViewControllerAnimated(true);
                }
                
            })
        }
        
        
    }
    @IBAction func touchedInBackground(sender: AnyObject) {
        
        self.view.endEditing(true);
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func validateFields() -> Bool {
        let userText = nameTxtFld.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet());
        let passText = titleTxtFld.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet());
         let commentText = cmntTxtView.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet());
        
        if userText?.characters.count <= 0 {
            
            MGNetworkClient.sharedClient.showErrorALertWithMessage("Please enter your name");
            //  self.showNormalAlert("Please enter Username");
            return false;
        }
        else if passText?.characters.count <= 0
        {
            MGNetworkClient.sharedClient.showErrorALertWithMessage("Please enter title");
            //self.showNormalAlert("Please enter Password");
            return false;
        }
        else if commentText?.characters.count <= 0
        {
            MGNetworkClient.sharedClient.showErrorALertWithMessage("Please enter your Comments");
            //self.showNormalAlert("Please enter Password");
            return false;
        }

        
        return true;
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

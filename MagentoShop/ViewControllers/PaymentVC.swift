//
//  PaymentVC.swift
//  MagentoShop
//
//  Created by Bunty on 27/06/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import MGNetworkClient
import MagentoClient
import PassKit




class PaymentVC: BaseVc {

    @IBOutlet weak var payContainer: UIView!
   
    @IBOutlet weak var applePayBtn: UIButton!
    @IBOutlet weak var applePayCOntainer: UIView!
    @IBOutlet var headerBottomLine: UILabel!
    @IBOutlet var payPalradioBtn: UIButton!
    @IBOutlet var paymentBtn: UIButton!
    
    @IBOutlet weak var applePaySelectBtn: UIButton!
    @IBOutlet weak var applePayRadioBtn: UIButton!
    @IBOutlet var nextBlackOverLay: UIView!
    @IBOutlet var nextBcg: UIView!
    @IBOutlet var codRadioBtn: UIButton!
    @IBOutlet var paypalSelectBtn: UIButton!
     var shippingInfo:ShippingInfo?
    var totolAMount:String?
    var shippingAddress:String?
    var cartArr:[CartItem]?
    override func viewDidLoad() {
        super.viewDidLoad()
 self.title = "SELECT PAYMENT METHOD"
        // Do any additional setup after loading the view.
        self.codRadioBtn.selected = true;
        self.payPalradioBtn.selected = false;
         self.applePayRadioBtn.selected = false;
        self.configureView()
    }
    func configureView()  {
        self.paymentBtn.backgroundColor = Theme.theme().themeColor!
        
        self.nextBcg.backgroundColor = Theme.theme().themeColor
        self.headerBottomLine.backgroundColor = Theme.theme().themeColor!
        if PKPaymentAuthorizationViewController.canMakePayments() == false {
            payContainer.hidden = true;
            return;
        }

    }

    @IBAction func applePaySelectAction(sender: AnyObject) {
        self.codRadioBtn.selected = false;
        self.payPalradioBtn.selected = false;
        self.applePayRadioBtn.selected = true;
    }

    @IBAction func payPalSelectAction(sender: AnyObject) {
        self.codRadioBtn.selected = false;
        self.payPalradioBtn.selected = true;
          self.applePayRadioBtn.selected = false;
    }
    @IBAction func codRadioBtnAction(sender: AnyObject) {
        self.codRadioBtn.selected = true;
        self.payPalradioBtn.selected = false;
        self.applePayRadioBtn.selected = false;
    }
    @IBAction func previousAction(sender: AnyObject) {
                self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func applePayAction(sender: AnyObject) {
        
        
        
    }
   

    @IBAction func NextBtnAction(sender: AnyObject) {
        
        let vc:ConfirmationVC = self.storyboard?.instantiateViewControllerWithIdentifier("ConfirmationVC") as! ConfirmationVC;
        vc.totolAMount = self.totolAMount;
        vc.shippingAddress = self.shippingAddress;
//        vc.paymentMethod = "CHECKMO"
        vc.shippingInfo = self.shippingInfo;
        if self.codRadioBtn.selected {
             //vc.paymentMethod = "Cash On Delivery (COD)"
              vc.paymentMethod = "joyfaypay"
        }
        else if self.payPalradioBtn.selected
        {
            vc.paymentMethod = "PayPal(PBP)"
        }
        else
        {
//         vc.paymentMethod = "paypal_express"
            vc.paymentMethod = "Apple Pay"
        }
         vc.cartArr = self.cartArr;
        self.navigationController?.pushViewController(vc, animated: true);
    }
    @IBOutlet var nextAction: UIButton!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:   -Navigation
    

//    AuthNet *an = [AuthNet getInstance];
//    [an setDelegate:self];
//    // Create the transaction.
//    CreateTransactionRequest *request = [CreateTransactionRequest
//    createTransactionRequest];TransactionRequestType *requestType =
//    [TransactionRequestType transactionRequest];
//    request.transactionRequest = requestType;
//    request.transactionType = AUTH_ONLY;
//    // Set the fingerprint data, this would have been generated on your server.
//    FingerPrintObjectType *fpData = [FingerPrintObjectType
//    fingerPrintObjectType];
//    fpData.hashValue = fingerprintHash;
//    fpData.sequenceNumber=fingerprintSequence;
//    fpData.timeStamp=fingerprintTimestamp;
//    // Set the merchant authentication data.
//    request.anetApiRequest.merchantAuthentication.name = @"5KP3u95bQpv"; //
//    Note this is API Login ID
//    request.anetApiRequest.merchantAuthentication.fingerPrint = fpData;
//    // Set the opaque data.
//    OpaqueDataType *opData = [OpaqueDataType opaqueDataType]; // sample value
//    is set here. actual value is obtained from the Apple Pay SDK.
//    opData.dataValue=[self getData2];
//    opData.dataDescriptor=@"COMMON.APPLE.INAPP.PAYMENT";
//    PaymentType *paymentType = [PaymentType paymentType];
//    paymentType.creditCard= nil;
//    paymentType.bankAccount= nil;
//    paymentType.trackData= nil;
//    paymentType.swiperData= nil;
//    paymentType.opData = opData;
//    // Set the amount (and other order details).
//    NSString *strAmount = [NSString stringWithFormat:@"%.2f", amount];
//    requestType.amount = strAmount;
//    requestType.payment = paymentType;
//    requestType.retail.marketType=@"0";
//    requestType.retail.deviceType =@"7";
//    OrderType *order = [OrderType order];
//    order.invoiceNumber = [NSString stringWithFormat:@"%d", arc4random() %
//    100];
//    // Submit the transaction.
//    [an purchaseWithRequest:request];}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}



//
//  ProductDetailSizeCell.swift
//  MagentoShop
//
//  Created by Bunty on 26/06/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import MagentoClient
protocol ProductDetailSizeCellDelegate
{
    func sizeBtn1Tapped(sender: UIButton)
    func sizeBtn2Tapped(sender: UIButton)
    func sizeBtn3Tapped(sender: UIButton)
    func sizeBtn4Tapped(sender: UIButton)
    func sizeBtn5Tapped(sender: UIButton)
    func sizeBtn6Tapped(sender: UIButton)
   }
class ProductDetailSizeCell: UITableViewCell {

    @IBOutlet var firstSizeBtn: UIButton!
    @IBOutlet var secondSizeBtn: UIButton!
    @IBOutlet var thirdSizeBtn: UIButton!
    @IBOutlet var fourthSizeBtn: UIButton!
    @IBOutlet var fifthSizeBtn: UIButton!
    @IBOutlet var sixthSizeBtn: UIButton!
 
    let selectedColor:UIColor = Theme.theme().themeColor!;
    
    
    var delegate:ProductDetailSizeCellDelegate?
    var sizeBtnArr:[UIButton]?
    
    
    @IBAction func firstSizeBtnAction(sender: UIButton) {
        self.delegate?.sizeBtn1Tapped(sender);
    }


    @IBAction func secondSizeBtnAction(sender: UIButton) {
          self.delegate?.sizeBtn2Tapped(sender);
    }
    
    @IBAction func thirdSizeBtnAction(sender: UIButton) {
          self.delegate?.sizeBtn3Tapped(sender);
    }

    @IBAction func fourthSizeBtnAction(sender: UIButton) {
          self.delegate?.sizeBtn4Tapped(sender);
    }
    
    @IBAction func fifthSIzeBtnAction(sender: UIButton) {
          self.delegate?.sizeBtn5Tapped(sender);
    }
    
    @IBAction func sixthSizeBtnAction(sender: UIButton) {
          self.delegate?.sizeBtn6Tapped(sender);
    }
    
    func selectaBtn(sender:UIButton) {
        for btn:UIButton in sizeBtnArr! {
            btn.backgroundColor = UIColor.lightGrayColor();
            
        }
        sender.backgroundColor = Theme.theme().themeColor!;
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.sizeBtnArr = [firstSizeBtn,secondSizeBtn,thirdSizeBtn,fourthSizeBtn,fifthSizeBtn,sixthSizeBtn]
        
        for btn:UIButton in sizeBtnArr! {
            btn.layer.cornerRadius = self.firstSizeBtn.frame.size.width/2;
            btn.clipsToBounds = true;
            
            btn.hidden = true;
            
        }

    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

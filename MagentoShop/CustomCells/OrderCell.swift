//
//  OrderCell.swift
//  MagentoShop
//
//  Created by vikaskumar on 7/18/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit

class OrderCell: UITableViewCell {

    @IBOutlet var itemImageView: UIImageView!
    @IBOutlet var costLabelContainer: UIView!
    @IBOutlet var costLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var subtitleLabel: UILabel!
    @IBOutlet var quantityLbl: UILabel!
    @IBOutlet var qauntityTxtFld: UITextField!
     @IBOutlet var sizeTxtFld: UITextField!
     @IBOutlet var priceTxtFld: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  SideMenuTopCell.swift
//  MagentoShop
//
//  Created by vikaskumar on 7/8/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import MagentoClient
protocol SideMenuTopCellDelegate
{
    func orderBtnTapped();
    func logBtnTapped(sender: UIButton!);
}
class SideMenuTopCell: UITableViewCell {
@IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var logBtn: UIButton!
    @IBOutlet weak var abreviationContainer: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var orderBtn: UIButton!
    var delegate:SideMenuTopCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backButton.setTitleColor(Theme.theme().themeColor, forState: .Normal);
           }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func logAction(sender: UIButton!) {
        self.delegate?.logBtnTapped(sender)
    }

    @IBAction func ordersAction(sender: AnyObject) {
      self.delegate?.orderBtnTapped()
    }
    
    
}

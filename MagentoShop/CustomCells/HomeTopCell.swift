//
//  HomeTopCell.swift
//  MagentoShop
//
//  Created by Bunty on 27/06/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
protocol HomeTopCellDelegate
{
    func topCellTappedWithIndex(index: Int)

}
class HomeTopCell: UITableViewCell,UIScrollViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var bannerImageView: UIImageView!
    @IBOutlet weak var bannerImageView3: UIImageView!
    @IBOutlet weak var scrollContentView: UIView!
    @IBOutlet weak var bannerImageView2: UIImageView!
    var delegate:HomeTopCellDelegate?
    var timer:NSTimer? = nil
   
    @IBOutlet weak var topBtn: UIButton!
    @IBAction func topBtnAction(sender: UIButton) {
        self.delegate?.topCellTappedWithIndex(pageControl.currentPage);
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        pageControl.numberOfPages = 3;
        self.scrollView.delegate = self;
        if let t = self.timer {
            t.invalidate()
        }
    self.timer =     NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector:  #selector(HomeTopCell.scrollPageContinously), userInfo: nil, repeats: true);
        
        self.timer!.fire();
 
      
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width*3, 0);
     
        }
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
    
     func scrollToPage(scrollView: UIScrollView, page: Int, animated: Bool) {
        var frame: CGRect = scrollView.frame
        frame.origin.y = frame.size.width * CGFloat(page);
        frame.origin.y = 0;
        scrollView.scrollRectToVisible(frame, animated: animated)
    }

    func scrollPageContinously() {
        var pageTOScroll = 0;
        if pageControl.currentPage == 2 {
            pageTOScroll = 0;
        }
        else
        {
            pageTOScroll = pageControl.currentPage + 1 ;
        }
        self.scrollView.setContentOffset(CGPointMake(scrollView.frame.size.width * CGFloat(pageTOScroll), 0), animated: true);
       pageControl.currentPage = Int(pageTOScroll)
       // self.scrollToPage(self.scrollView, page: pageTOScroll, animated: true);
        
    }
}

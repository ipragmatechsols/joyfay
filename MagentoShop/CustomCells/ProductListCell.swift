//
//  ProductListCell.swift
//  MagentoShop
//
//  Created by Bunty on 08/07/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import MagentoClient
protocol ProductListCellDelegate
{
    func leftButtonTapped(sender: UIButton)
    func rightButtonTapped(sender: UIButton)
}
class ProductListCell: UITableViewCell {

    @IBOutlet var imageViewOne: UIImageView!
    @IBOutlet var imageView2: UIImageView!
    @IBOutlet var titleLbl1: UILabel!
    @IBOutlet var subtitleLbl1: UILabel!
    @IBOutlet var priceLbl1: UILabel!
    @IBOutlet var textContainer1: UIView!
    @IBOutlet var textContainer2: UIView!
    @IBOutlet var titleLbl2: UILabel!
    @IBOutlet var subtitleLbl2: UILabel!
    @IBOutlet var priceLbl2: UILabel!
    @IBOutlet var firstBtn: UIButton!
    @IBOutlet var secondBtn: UIButton!
    var delegate:ProductListCellDelegate?
    @IBAction func firstBtnAction(sender: UIButton) {
        self.delegate?.leftButtonTapped(sender);
    }
    @IBAction func secondBtnAction(sender: UIButton) {
        
         self.delegate?.rightButtonTapped(sender);
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.subtitleLbl1.textColor = Theme.theme().themeColor!
        self.subtitleLbl2.textColor = Theme.theme().themeColor!
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

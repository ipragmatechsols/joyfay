//
//  WomenDualImageCell.swift
//  MagentoShop
//
//  Created by Bunty on 27/06/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
protocol WomenDualImageDelegate
{
    func firstControlTapped(sender: UIButton)
    func secondControlTapped(sender: UIButton)
}
class WomenDualImageCell: UITableViewCell {

    @IBOutlet var firstControlView: UIButton!
    @IBOutlet var seconControlView: UIButton!
    var delegate:WomenDualImageDelegate?
    @IBAction func secondViewTapped(sender: UIButton) {
        self.delegate?.firstControlTapped(sender);
    }
   
    @IBAction func firstViewTapped(sender: UIButton) {
         self.delegate?.secondControlTapped(sender);
    }
    @IBOutlet weak var firstIMageView: UIImageView!
    @IBOutlet weak var secondImageView: UIImageView!
    @IBOutlet weak var firstHeadingLbl: UILabel!
    @IBOutlet weak var secondHeadingLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

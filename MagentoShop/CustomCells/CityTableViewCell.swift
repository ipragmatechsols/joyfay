//
//  CityTableViewCell.swift
//  MagentoShop
//
//  Created by vikaskumar on 7/18/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit

class CityTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var subtitleLbl3: UILabel!

    @IBOutlet weak var subtitleLbl1: UILabel!
    @IBOutlet weak var subtitleLbl2: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  DropDownCell.swift
//  MagentoShop
//
//  Created by vikaskumar on 08/10/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import IQDropDownTextField

class DropDownCell: UITableViewCell {

    @IBOutlet weak var dropImage: UIImageView!
    @IBOutlet weak var dropdownTextField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

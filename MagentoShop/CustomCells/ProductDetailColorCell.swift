//
//  ProductDetailColorCell.swift
//  MagentoShop
//
//  Created by Bunty on 26/06/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
protocol ProductDetailColorCellDelegate
{
    func colorBtn1Tapped(sender: UIButton)
    func colorBtn2Tapped(sender: UIButton)
     func colorBtn3Tapped(sender: UIButton)
     func colorBtn4Tapped(sender: UIButton)
     func colorBtn5Tapped(sender: UIButton)
     func colorBtn6Tapped(sender: UIButton)
     func colorBtn7Tapped(sender: UIButton)
     func colorBtn8Tapped(sender: UIButton)
}
class ProductDetailColorCell: UITableViewCell {

    @IBOutlet var colorBtn3: UIButton!
    @IBOutlet var ColorBtn2: UIButton!
    @IBOutlet var ColorBtn1: UIButton!
    @IBOutlet var ColorBtn4: UIButton!
    @IBOutlet var colorBtn5: UIButton!
    @IBOutlet var colorBtn6: UIButton!
    @IBOutlet var colorBtn7: UIButton!
    @IBOutlet var colorBtn8: UIButton!
    var delegate:ProductDetailColorCellDelegate?
    var colorBtnArr:[UIButton]?
    @IBAction func colorBtnActiion1(sender: UIButton) {
        self.delegate?.colorBtn1Tapped(sender);

    }
    @IBAction func colorBtnAction2(sender: UIButton) {
         self.delegate?.colorBtn2Tapped(sender);
    }
    @IBAction func colorBtnAction3(sender: UIButton) {
         self.delegate?.colorBtn3Tapped(sender);
    }
    @IBAction func colorBtnAction4(sender: UIButton) {
         self.delegate?.colorBtn4Tapped(sender);
    }
    @IBAction func colorBtnAction5(sender: UIButton) {
         self.delegate?.colorBtn5Tapped(sender);
    }
    @IBAction func colorBtnAction6(sender: UIButton) {
         self.delegate?.colorBtn6Tapped(sender);
    }
    @IBAction func colorBtnAction7(sender: UIButton) {
         self.delegate?.colorBtn7Tapped(sender);
    }
    @IBAction func colorBtnAction8(sender: UIButton) {
         self.delegate?.colorBtn8Tapped(sender);
    }
  
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        self.colorBtnArr = [ColorBtn1,ColorBtn2,colorBtn3,ColorBtn4,ColorBtn4,colorBtn5,colorBtn6,colorBtn7,colorBtn8]
        
        for btn:UIButton in colorBtnArr! {
            btn.layer.cornerRadius = self.ColorBtn1.frame.size.width/2;
           btn.clipsToBounds = true;
           
            btn.hidden = true;
          
        }
          }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}

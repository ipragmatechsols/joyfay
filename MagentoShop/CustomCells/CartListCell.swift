//
//  CartListCell.swift
//  MagentoShop
//
//  Created by Bunty on 24/06/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
protocol CartListCellDelegate
{
    func selectedRowIs(selectedRow:Int);
   
}

class CartListCell: UITableViewCell,UITextFieldDelegate {

    @IBOutlet weak var availableLbl: UILabel!
    @IBOutlet weak var subTotalLbl: UILabel!
    @IBOutlet var itemImageView: UIImageView!
    @IBOutlet var costLabelContainer: UIView!
    @IBOutlet var costLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var subtitleLabel: UILabel!
    @IBOutlet var quantityLbl: UILabel!
    @IBOutlet var qauntityTxtFld: UITextField!
     var delegate:CartListCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 func textFieldShouldBeginEditing(textField: UITextField) -> Bool
{
    self.delegate?.selectedRowIs(textField.tag);
    return true;
}
}

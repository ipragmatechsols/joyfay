//
//  ProductDetailimageViewCellTableViewCell.swift
//  MagentoShop
//
//  Created by Bunty on 26/06/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit

class ProductDetailimageViewCellTableViewCell: UITableViewCell {
    @IBOutlet var collectionView: UICollectionView!

    @IBOutlet var priceLbl: UILabel!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var ImageVView: UIImageView!
    var collectionViewOffset: CGFloat {
        get {
            return collectionView.contentOffset.x
        }
        
        set {
            collectionView.contentOffset.x = newValue
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setCollectionViewDataSourceDelegate
        <D: protocol<UICollectionViewDataSource, UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout>>
        (dataSourceDelegate: D, forRow row: Int) {
        
        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = row
        collectionView.reloadData()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  PopularItemCell.swift
//  MagentoShop
//
//  Created by Bunty on 27/06/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import MagentoClient
protocol PopularItemCellDelegate {
    func popularFirstBtnTapped(sender: UIButton)
    func popularsecondBtnTapped(sender: UIButton)
    func popularThirdBtnTapped(sender: UIButton)
    func popularFourthBtnTapped(sender: UIButton)
}


class PopularItemCell: UITableViewCell {

    @IBOutlet var popularMenuLbl: UILabel!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    var delegate:PopularItemCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        popularMenuLbl.textColor = Theme.theme().themeColor!;
        btn1.layer.cornerRadius = btn1.frame.height/2;
        btn1.clipsToBounds = true;
        btn2.layer.cornerRadius = btn2.frame.height/2;
        btn2.clipsToBounds = true;
        btn3.layer.cornerRadius = btn3.frame.height/2;
        btn3.clipsToBounds = true;
        btn4.layer.cornerRadius = btn4.frame.height/2;
        btn4.clipsToBounds = true;
        self.btn1.layer.borderWidth = 1.0;
        self.btn1.layer.borderColor = UIColor .lightGrayColor().CGColor
        self.btn2.layer.borderWidth = 1.0;
        self.btn2.layer.borderColor = UIColor .lightGrayColor().CGColor
        self.btn3.layer.borderWidth = 1.0;
        self.btn3.layer.borderColor = UIColor .lightGrayColor().CGColor
        self.btn4.layer.borderWidth = 1.0;
        self.btn4.layer.borderColor = UIColor .lightGrayColor().CGColor
    }

    @IBAction func btn1Action(sender: UIButton) {
        self.delegate?.popularFirstBtnTapped(sender);
    }
    @IBAction func btn2Action(sender: UIButton) {
        self.delegate?.popularsecondBtnTapped(sender);
    }
    @IBAction func btn3Action(sender: UIButton) {
        self.delegate?.popularThirdBtnTapped(sender);
    }
    @IBAction func btn4Action(sender: UIButton) {
        self.delegate?.popularFourthBtnTapped(sender);
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

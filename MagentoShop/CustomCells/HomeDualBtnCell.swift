//
//  HomeDualBtnCell.swift
//  MagentoShop
//
//  Created by Bunty on 27/06/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import MagentoClient
protocol HomeDualBtnCellDelegate
{
    func topBrandsTapped(sender: UIButton)
    func topChoiceTapped(sender: UIButton)
}
class HomeDualBtnCell: UITableViewCell {

    @IBOutlet var topchoicesBtn: UIButton!
    @IBOutlet var topBrandsBtn: UIButton!
    var delegate:HomeDualBtnCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.topchoicesBtn.backgroundColor = Theme.theme().themeColor!;
        self.topchoicesBtn.titleLabel?.textColor = UIColor.whiteColor();
        topchoicesBtn.layer.cornerRadius = topchoicesBtn.frame.height/2;
        topchoicesBtn.clipsToBounds = true;
        topBrandsBtn.layer.cornerRadius = topchoicesBtn.frame.height/2;
        topBrandsBtn.clipsToBounds = true;
        self.topBrandsBtn.layer.borderWidth = 1.0;
        self.topBrandsBtn.layer.borderColor = UIColor .lightGrayColor().CGColor
        self.topchoicesBtn.layer.borderWidth = 1.0;
        self.topchoicesBtn.layer.borderColor = UIColor .lightGrayColor().CGColor
    }

    @IBAction func topBrandsAction(sender: UIButton) {
        self.topBrandsBtn.backgroundColor = Theme.theme().themeColor!;
        //self.topBrandsBtn.titleLabel?.textColor = UIColor.blackColor();
        self.topBrandsBtn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        self.topchoicesBtn.backgroundColor = UIColor.whiteColor();
        self.topchoicesBtn.titleLabel?.textColor = UIColor.darkGrayColor();
        self.delegate?.topBrandsTapped(sender);
    }
    @IBAction func topChoiceAction(sender: UIButton) {
        self.topBrandsBtn.backgroundColor = UIColor.whiteColor();
        self.topBrandsBtn.titleLabel?.textColor = UIColor.darkGrayColor();
       
        self.topchoicesBtn.backgroundColor = Theme.theme().themeColor!;
        self.topchoicesBtn.titleLabel?.textColor = UIColor.whiteColor();
        
        self.delegate?.topChoiceTapped(sender)
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

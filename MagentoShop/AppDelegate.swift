////
//  AppDelegate.swift
//  MagentoShop
//
//  Created by Bunty on 11/06/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit
import MagentoClient
import MGNetworkClient



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate{
    static var appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
     var navigationController:UINavigationController?

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
       
        TestFairy.begin("d5b6e1e083a4f2fb981c585a0b0e7b4a27e7c019")
        
        let textAttributes = [NSForegroundColorAttributeName:Theme.theme().themeColor!]
        UINavigationBar.appearance().titleTextAttributes = textAttributes
        UINavigationBar.appearance().tintColor = Theme.theme().themeColor!;
        UINavigationBar.appearance().backgroundColor = UIColor.whiteColor()
//        if #available(iOS 9.0, *) {
//            UITextField.appearanceWhenContainedInInstancesOfClasses([UISearchBar.self]).backgroundColor = Theme.theme().themeColor!
//        } else {
//            // Fallback on earlier versions
//        }

        
        PayPalMobile .initializeWithClientIdsForEnvironments([PayPalEnvironmentProduction: "AfXLKV7gbcIdEWr-IrGfZuhpp76oNEtKJOsZSxLcsIMRx-d3dTkroS9GCVaXjJlP-H2UBcK88eUm0nw_",
        PayPalEnvironmentSandbox: "AeG_v-T6pr0YCb0P-WiWdvuXg9rRvyB_Do9u-K3B0gWbi3xff8tcVT1p1qENAlBFuFnPHGVv6PKsT6dH"])
        MGNetworkClient.sharedClient.getCountries()
        
        return true
    }
    
   
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}


/*

extension AppDelegate: CLLocationManagerDelegate {
    
    func locationManager(manager: CLLocationManager, didEnterRegion region: CLRegion) {
        if region is CLCircularRegion {
            print("Region = ", region)
        }
    }
    
    func locationManager(manager: CLLocationManager, didExitRegion region: CLRegion) {
        if region is CLCircularRegion {
            print("Region = ", region)
        }
    }
    
    
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation)
    {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    
    
}*/





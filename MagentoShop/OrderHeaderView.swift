//
//  OrderHeaderView.swift
//  MagentoShop
//
//  Created by vikaskumar on 7/18/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit

class OrderHeaderView: UITableViewHeaderFooterView {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var subtitleLbl3: UILabel!
    
    @IBOutlet weak var subtitleLbl1: UILabel!
    @IBOutlet weak var subtitleLbl2: UILabel!
    @IBOutlet weak var acessoryImage: UIImageView!
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

     func acessoryImageSelcted()
    {
        acessoryImage.image = UIImage(named: "up");
    }
    
    func acessoryImageNormal()
    {
        acessoryImage.image = UIImage(named: "drop");
    }
}

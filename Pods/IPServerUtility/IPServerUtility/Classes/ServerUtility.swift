//
//  ServerUtility.swift
//  ServerUtility
//
//  Created by vikaskumar on 6/10/16.
//  Copyright © 2016 vikaskumar. All rights reserved.
//  Comment

//ADDED PUT COMPATIBILITY


import UIKit
import Alamofire
import SwiftyJSON
import Haneke


public enum RequesType  {
    case GET
    case POST
    case DELETE
    case PUT
}

public class ServerUtility {
    
    // typealias  ResponseHandler = ((NSError? , [String:AnyObject]?) -> Void)?
    public  typealias ResponseHandler = (NSError?, SwiftyJSON.JSON?)->()?
    public static let sharedInstance = ServerUtility()
    public var baseUrl:String?
    public var authToken:String?
    public var networkReachabilityManager:NetworkReachabilityManager?
    private init() {}
    
    public func setConfiguration(baseUrl:String) {
        self.baseUrl = baseUrl;
    }
    
    public func makeRequest(ofType type:RequesType,  onPath:String, withParams:[String:AnyObject]?, responseHandler:ResponseHandler )  {
        
        assert(self.baseUrl != nil );//SetConfigurations First;
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        var urlString: String = onPath.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        
        let fullUrl  = self.baseUrl! + urlString; //onPath;
        
       // print(fullUrl)
       // print(withParams)
        
        
        //  let headers = [""]
        var headers = [String:String]();
        if self.authToken != nil {
            headers = [
                "Authorization": "Bearer " + self.authToken!,
                "Content-Type": "application/json"
            ]
        }
        else
        {
            headers = [
                "Content-Type": "application/json"
            ]
        }
        
        if type == .GET {
            let cache = Shared.dataCache
            
            
            if self.networkReachabilityManager != nil
            {
                if  self.networkReachabilityManager!.isReachable  == false
                {
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false

                    cache.fetch(key: fullUrl).onSuccess { data in
                        // Do something with data
                        let json = JSON(data: data);
                       
                        responseHandler(nil,json);
                    }
                        .onFailure({ (er:NSError?) in
                            
                            Alamofire.request(.GET, fullUrl,headers:headers,encoding: .JSON).validate(contentType: ["application/json","text/html"]).responseJSON { response in
                                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                                switch response.result {
                                case .Success:
                                    if let value = response.result.value {
                                        let json = JSON(value)
                                       // print(value)
                                        cache.set(value: response.data!, key: fullUrl)
                                        responseHandler(nil,json);
                                        
                                    }
                                    else
                                    {
                                        let error  = NSError.init(domain: NSURLErrorDomain, code: 1009, userInfo: [NSLocalizedDescriptionKey:"Undefined Error"]);
                                        responseHandler(error,nil);
                                    }
                                    
                                case .Failure(let error):
                                    responseHandler(error,nil);
                                   

                                }
                            }
                        
                    })
                    return
                }
          
            }
            

            Alamofire.request(.GET, fullUrl,headers:headers,encoding: .JSON).validate(contentType: ["application/json","text/html"]).responseJSON { response in
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                switch response.result {
                case .Success:
                    if let value = response.result.value {
                        //print("GET response ----- %@",value)
                        let json = JSON(value)
                        cache.set(value: response.data!, key: fullUrl)
                        responseHandler(nil,json);
                        
                    }
                    else
                    {
                        let error  = NSError.init(domain: NSURLErrorDomain, code: 1009, userInfo: [NSLocalizedDescriptionKey:"Undefined Error"]);
                        responseHandler(error,nil);
                    }
                    
                case .Failure(let error):
                    
                    responseHandler(error,nil);
                    cache.fetch(key: fullUrl).onSuccess { data in
                        // Do something with data
                        let json = JSON(data: data);
                        
                        responseHandler(nil,json);
                    }
                }
            }
            
            
        }
        else if type == .POST
        {
            Alamofire.request(.POST, fullUrl,parameters: withParams,headers:headers,encoding: .JSON).validate(contentType: ["application/json","text/html"]).responseJSON { response in
                
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                switch response.result {
                case .Success:
                    if let value = response.result.value {
                       // print("POST response ----- %@",value)
                        let json = JSON(value)
                        responseHandler(nil,json);
                        
                    }
                    else
                    {
                        let error  = NSError.init(domain: NSURLErrorDomain, code: 1009, userInfo: [NSLocalizedDescriptionKey:"Undefined Error"]);
                        responseHandler(error,nil);
                    }
                    
                case .Failure(let error):
                    responseHandler(error,nil);
                }
            }
            
        }
        else if type == .DELETE
        {
            Alamofire.request(.DELETE, fullUrl,parameters: withParams,headers:headers,encoding: .JSON).validate(contentType: ["application/json","text/html"]).responseJSON { response in
                
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                switch response.result {
                case .Success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        //print("DELETE response ----- %@", value)
                        responseHandler(nil,json);
                        
                    }
                    else
                    {
                        let error  = NSError.init(domain: NSURLErrorDomain, code: 1009, userInfo: [NSLocalizedDescriptionKey:"Undefined Error"]);
                        responseHandler(error,nil);
                    }
                    
                case .Failure(let error):
                    responseHandler(error,nil);
                }
            }
        }
        
        else if type == .PUT
        {
            Alamofire.request(.PUT, fullUrl,parameters: withParams,headers:headers,encoding: .JSON).validate(contentType: ["application/json","text/html"]).responseJSON { response in
                
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                switch response.result {
                case .Success:
                    if let value = response.result.value {
                        let json = JSON(value)
                       // print("PUT response ---- %@ ", value)
                        responseHandler(nil,json);
                        
                    }
                    else
                    {
                        let error  = NSError.init(domain: NSURLErrorDomain, code: 1009, userInfo: [NSLocalizedDescriptionKey:"Undefined Error"]);
                        responseHandler(error,nil);
                    }
                    
                case .Failure(let error):
                    responseHandler(error,nil);
                }
            }
        }
        
        
    }
    
    
    
    
    
}

# MagentoHud

[![CI Status](http://img.shields.io/travis/iPragmatech/MagentoHud.svg?style=flat)](https://travis-ci.org/iPragmatech/MagentoHud)
[![Version](https://img.shields.io/cocoapods/v/MagentoHud.svg?style=flat)](http://cocoapods.org/pods/MagentoHud)
[![License](https://img.shields.io/cocoapods/l/MagentoHud.svg?style=flat)](http://cocoapods.org/pods/MagentoHud)
[![Platform](https://img.shields.io/cocoapods/p/MagentoHud.svg?style=flat)](http://cocoapods.org/pods/MagentoHud)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MagentoHud is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "MagentoHud"
```

## Author

iPragmatech, info@ipragmatech.com

## License

MagentoHud is available under the MIT license. See the LICENSE file for more info.

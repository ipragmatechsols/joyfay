//
//  MagentoHud.swift
//  MagentoHud
//
//  Created by Bunty on 04/06/16.
//  Copyright © 2016 iPragmatech Solutions Pvt. Ltd. All rights reserved.
//

import UIKit

public class MagentoHud: UIView {
    
   public static let hud = MagentoHud();
   public var currentWindow:UIWindow?
   public var cart:Cart?
   public var topView:UIView?;
   public  var isVisible:Bool?
  
    public init() {
        
        currentWindow = UIApplication.sharedApplication().keyWindow;
        if currentWindow == nil {
            let windows = UIApplication.sharedApplication().windows.reverse();
            for window:UIWindow in windows {
                if window.windowLevel == UIWindowLevelNormal {
                    currentWindow = window;
                    break;
                }
            }
        }
        
        
        if var topController = currentWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            topView = topController.view;
            
        }
        super.init(frame: CGRect(x: 0, y:0, width: topView!.bounds.size.width, height: topView!.bounds.size.width))
        self.frame = (topView?.bounds)!;
        let point:CGPoint =  CGPointMake(self.center.x-70, self.center.y-70)
        self.cart = Cart.init(point: point);
        
        self.backgroundColor = UIColor.clearColor();
        let fakeView = UIView.init(frame: self.bounds);
        fakeView.backgroundColor  = UIColor.blackColor().colorWithAlphaComponent(0.5);
        self.addSubview(fakeView);
        self.isVisible = false;
    }
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
  public  class func show()
    {
        if MagentoHud.hud.isVisible == false {
            MagentoHud.hud.topView!.addSubview(MagentoHud.hud);
            MagentoHud.hud.addSubview(MagentoHud.hud.cart!);
            MagentoHud.hud.topView!.bringSubviewToFront(MagentoHud.hud);
            MagentoHud.hud.cart!.show();
            MagentoHud.hud.isVisible = true;
        }
       
        
    }
   public class func dismiss()
    {
        if MagentoHud.hud.isVisible == true {

        MagentoHud.hud.cart!.dismiss();
        MagentoHud.hud.cart!.removeFromSuperview();
        MagentoHud.hud.removeFromSuperview();
         MagentoHud.hud.isVisible = false;
        }
        
    }
    
    public class func showHudOnView(v:UIView)
    {
        if MagentoHud.hud.isVisible == false {
            v.addSubview(MagentoHud.hud);
            MagentoHud.hud.addSubview(MagentoHud.hud.cart!);
            v.bringSubviewToFront(MagentoHud.hud);
            MagentoHud.hud.cart!.show();
            MagentoHud.hud.isVisible = true;
        }
    }
    public class func dismissHudOnView(v:UIView)
    {
        if MagentoHud.hud.isVisible == true {
            
            MagentoHud.hud.cart!.dismiss();
            MagentoHud.hud.cart!.removeFromSuperview();
            MagentoHud.hud.removeFromSuperview();
            MagentoHud.hud.isVisible = false;
        }
    }
}

//
//  Home.swift
//  Pods
//
//  Created by Bunty on 02/07/16.
//
//

import UIKit
import SwiftyJSON


public class Home: NSObject {
    public  var mainbanners: [Banner]?
    public var categoryBanners: [MGCategory]?
    public var topChoiceBanners: [TopChoice]?
    public  var newProducts: [Banner]?
    public  var popularMenus: [Menu]?

    
    required public override init() {
        super.init();
    }
    
    convenience init(map:JSON?) {
        self.init()
        let bannerAr  = map!["banner"];
       // let bannerAr:[[String:String?]] = map!["banner"] as? [[String:String?]];
        //let bannerAr:[String:AnyObject?] = map!["banner"] as! [String:AnyObject?];
        var bannerMdlAr = [Banner]();
         for (key,subJson):(String, JSON) in bannerAr {
            let banner = Banner.init(map: subJson);
            bannerMdlAr.append(banner);
        }
        self.mainbanners = bannerMdlAr;
        
        
        let categoryAr = map!["categoryBanner"];
        var categoryMdlAr = [MGCategory]();
        for (key,subJson):(String, JSON)  in categoryAr {
            let cat = MGCategory.init(map: subJson);
            categoryMdlAr.append(cat);
        }
        self.categoryBanners = categoryMdlAr;
        
        
        let topChBannersAr = map!["topchoicebanner"]
        var topChMdlAr = [TopChoice]();
           for (key,subJson):(String, JSON) in topChBannersAr {
            let cat = TopChoice.init(map: subJson);
            topChMdlAr.append(cat);
        }
        self.topChoiceBanners = topChMdlAr;
        
        
        let newProductAr = map!["newproduct"]
        var newProductMdlAr = [Banner]();
      for (key,subJson):(String, JSON) in newProductAr {
            let banner = Banner.init(map: subJson);
            newProductMdlAr.append(banner);
        }
        self.newProducts = newProductMdlAr;
        
        let popularMenuBannerAr = map!["popularMenuBanner"] ;
        var popularMenuBannerMdlAr = [Menu]();
         for (key,subJson):(String, JSON)  in popularMenuBannerAr {
            let banner = Menu.init(map: subJson);
            popularMenuBannerMdlAr.append(banner);
        }
        self.popularMenus = popularMenuBannerMdlAr;
        
    }

}

public class MGCategory: Banner {
  
    
   public  var subCategories: [Banner]?
    public  var categoryChildrens:[String]?
    required public override init() {
        super.init();
    }
    
        convenience init(map:JSON?) {
            
        self.init()
            self.categoryChildrens = map?["category_children"].arrayObject as? [String]
        if(self.name == nil)
        {
            self.name    = map?["category_name"].string
        }
        if(self.id == nil)
        {
            self.id    = map?["category_id"].string
        }
        if(self.imageUrl == nil)
        {
            self.imageUrl    = map?["category_image"].string
        }
            let bannerAr = map!["subcategories"];
            var bannerMdlAr = [Banner]();
            for (key,subJson):(String, JSON)  in bannerAr {
                let banner = Banner.init(map: subJson);
                bannerMdlAr.append(banner);
            }
            self.subCategories = bannerMdlAr;
        
    }
}

public class TopChoice: NSObject {
    
    public var topChoices: [Banner]?
    public var features: [Banner]?
    
    required public override init() {
        super.init();
    }
    
    convenience init(map:JSON?) {
        
        self.init()
        let topChoices = map!["topchoice"] ;
        var topChoicesMdlAr = [Banner]();
        for (key,subJson):(String, JSON) in topChoices {
            let topCh = Banner.init(map: subJson);
            topChoicesMdlAr.append(topCh);
        }
        self.topChoices = topChoicesMdlAr;
        
        
        let featuresAr = map!["feature"];
        var featureMdlAr = [Banner]();
        for (key,subJson):(String, JSON) in featuresAr {
            let banner = Banner.init(map: subJson);
            featureMdlAr.append(banner);
        }
        self.features = featureMdlAr;
        
    }
}





public class Menu: NSObject {
    
    public var title: String?
    public var menus: [MGCategory]?
    
    required public override init() {
        super.init();
    }
    
    convenience init(map:JSON?) {
        
        self.init()
        self.title = map!["title"].string;
        
        
        let menusAr = map!["menus"];
        var menusMdlAr = [MGCategory]();
         for (key,subJson):(String, JSON) in menusAr {
            let banner = MGCategory.init(map: subJson);
            menusMdlAr.append(banner);
        }
        self.menus = menusMdlAr;
        
    }
}


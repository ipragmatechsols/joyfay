//
//  User.swift
//  Pods
//
//  Created by vikaskumar on 7/1/16.
//
//

import UIKit
import SwiftyJSON


public class User: NSObject {
    
    public  var id: Int?
    public  var firstname: String?
    public var lastname: String?
    public var email: String?
    public  var password: String?
    public  var website_id: Int?
    
    
    required public override init() {
        super.init();
    }
    
    convenience init(map:JSON) {
        self.init()
        self.firstname  = map["firstname"].string
        self.lastname    = map["lastname"].string
        self.email        =  map["email"].string
        self.password     = map["password"].string
        self.id           =  map["id"].int
        self.website_id   =  map["website_id"].int
    
    }
    
    
    
    public class func demoUser() ->User
    {
        
        let user = User();
        user.firstname  = "Vikas"
        user.lastname    = "Kumar"
        user.email        =  "vikaskumar@gmail.com"
        user.password     =  "123456"
        user.id           =  1
        user.website_id = 1
        
        return user;
        
        
    }
    
    public class func saveUser(user:User)
    {
        
        let defaults = NSUserDefaults.standardUserDefaults();
        if let userValue = user.firstname {
            defaults.setObject(userValue, forKey: "firstname");
        }
        if let userValue = user.lastname {
            defaults.setObject(userValue, forKey: "lastname");
        }
        if let userValue = user.email {
            defaults.setObject(userValue, forKey: "email");
        }
        if let userValue = user.password {
            defaults.setObject(userValue, forKey: "password");
        }
        if let userValue = user.id {
            defaults.setObject(userValue, forKey: "id");
        }
        
        if let userValue = user.website_id {
            defaults.setObject(userValue, forKey: "website_id");
        }else{
            defaults.setObject(0, forKey: "website_id");
        }

        
        defaults.synchronize();
        
    }
    
    public class func fetchSavedUser() -> User
    {
        let user  = User.init()
        let defaults = NSUserDefaults.standardUserDefaults();
        if let userValue = defaults.valueForKey("firstname") {
            user.firstname = userValue as! String;
        }
        if let userValue = defaults.valueForKey("lastname") {
            user.lastname = userValue as! String;
        }
        if let userValue = defaults.valueForKey("email") {
            user.email = userValue as! String;
        }
        if let userValue = defaults.valueForKey("password") {
            user.password = userValue as! String;
        }
        
        
        
        if let userValue = defaults.valueForKey("id") {
            user.id = userValue as! Int;
        }
        
        if let userValue = defaults.valueForKey("website_id") {
            user.website_id = userValue as! Int;
        }
        
        return user;
        
    }
}
public class Addres: NSObject {
    public  var  id:Int?
    public  var firstname: String?
    public var lastname: String?
    public var region: String?
    public var region_id: Int?
    
    public var region_code: String?
    
    public var country_id: String?
    public var telephone: String?
    
    public var postcode: String?
    public var customer_id: Int?
    public var email: String?
    public var street: String?
    public var street2: String?
    public var city: String?
    
    public var same_as_billing: Bool?
    
    
    required public override init() {
        super.init();
    }
    
    public convenience init(map:JSON) {
        self.init()
        self.id = map["id"].int
        self.firstname  = map["firstname"].string
        self.lastname    = map["lastname"].string
        self.email        =  map["email"].string
        
        let dictionary = map["region"]
        self.region        =  dictionary["region"].string
        self.region_id        =  map["region_id"].int
        self.region_code        =  map["region_code"].string
        self.country_id        =  map["country_id"].string
        self.telephone        =  map["telephone"].string
        self.postcode        =  map["postcode"].string
         self.country_id        =  map["country_id"].string
        self.city        =  map["city"].string
        self.customer_id        =  map["customer_id"].int
        
        let rawstreet = map["street"].array
        if rawstreet?.count > 0 {
            self.street = rawstreet![0].string!;
            
        }
        if rawstreet?.count > 1 {
            self.street2 = rawstreet![1].string!;
            
        }
//        var streetText:String = ""
//        if rawstreet != nil {
//            
//            var c = 0
//            for str in rawstreet!
//            {
//                if c == 0
//                {
//                streetText +=  str.string!;
//                }
//                else
//                {
//                 streetText += " " + str.string!;
//                }
//                c++;
//            }
//        }
//        if streetText.characters.count > 0 {
//            self.street        =  streetText;
//            
//            let strAr:Array = (self.street?.componentsSeparatedByString(" "))!;
//            
//           
//            if strAr.count > 1  {
//                var t = 0
//                for str in strAr
//                {
//                    if t == 0
//                    {
//                        self.street        =  strAr[t];
//                    }
//                    else if (t == 1)
//                    {
//                        
//                        self.street2  = "" + strAr[t];
//                    }
//                    else
//                    {
//                    self.street2  = self.street2! + " " + strAr[t];
//                    }
//                    
//                    t++;
//                }
//                
//                
//                
//            }
//        }
        
        
        self.same_as_billing        =  map["same_as_billing"].bool
    }
    
    
    public func getPramsFromAddress(ad:Addres!) -> [String:AnyObject]? {
        
        var finalParamDict = [String:AnyObject]()
        var paramDixt = NSMutableDictionary();
        paramDixt["firstname"] = ad.firstname!;
        paramDixt["lastname"] = ad.lastname!
        paramDixt["email"] = ad.email!
        paramDixt["region"] = ad.region!
        if ad.region_id != nil {
            paramDixt["region_id"] = ad.region_id
        }
        else
        {
        paramDixt["region_id"] = 0
        }
        
        if ad.region_code != nil {
            paramDixt["region_code"] = region_code
        }
        else
        {
            paramDixt["region_code"] = 0
        }
        
       // paramDixt["region_code"] = 0
        paramDixt["country_id"] = ad.country_id
        paramDixt["telephone"] = ad.telephone!
        paramDixt["postcode"] = ad.postcode!
        paramDixt["city"] = ad.city
        paramDixt["street"] = ["\(ad.street!)","\(ad.street2!)"]
        paramDixt["same_as_billing"] = 0;
        
        paramDixt["save_in_address_book"] = 0
        paramDixt["save_in_address_book"] = 1
        finalParamDict["addresses"] = paramDixt;
        
        return finalParamDict;
    }
    
    
    public func getPramsFromAddressNew(address: [Addres]) -> [String: AnyObject] {
       
        var addressArray = [[String: AnyObject]]()
        
        for ad in address{
            var paramDixt = [String: AnyObject]();
            paramDixt["firstname"] = ad.firstname ?? "";
            paramDixt["lastname"] = ad.lastname ?? ""
            // paramDixt["email"] = ad.email!
            paramDixt["region"] = ad.region ?? ""
            if ad.region_id != nil {
                paramDixt["region_id"] = ad.region_id
            }
            else
            {
                paramDixt["region_id"] = 0
            }
            
            /* if ad.region_code != nil {
             paramDixt["region_code"] = region_code
             }
             else
             {
             paramDixt["region_code"] = 0
             }*/
            
            // paramDixt["region_code"] = 0
            paramDixt["country_id"] = ad.country_id ?? ""
            paramDixt["telephone"] = ad.telephone ?? ""
            paramDixt["postcode"] = ad.postcode ?? ""
            paramDixt["city"] = ad.city ?? ""
            paramDixt["street"] = ["\(ad.street ?? "")","\(ad.street2 ?? "")"]
            // paramDixt["same_as_billing"] = 0;
            
            // paramDixt["save_in_address_book"] = 0
            // paramDixt["save_in_address_book"] = 1
            //finalParamDict["addresses"] = paramDixt;
            addressArray.append(paramDixt)
        }
        
        return ["addresses" : addressArray]
    }
    

    
    
    public func getPramsFromShippingAddress(ad:Addres!,shippingMethod:ShippingMethod?) -> [String:AnyObject]? {
        var addressINfoDict = [String:AnyObject]()
        var finalParamDict = NSMutableDictionary()
        var paramDixt = NSMutableDictionary();
        paramDixt["firstname"] = ad.firstname!;
        paramDixt["lastname"] = ad.lastname!
        paramDixt["email"] = ad.email!
        if ad.region != nil {
            paramDixt["region"] = ad.region!
            
        }
        else
        {
        paramDixt["region"] = ""
        }
        if ad.region_id != nil {
            paramDixt["region_id"] = ad.region_id
        }
        else
        {
            paramDixt["region_id"] = 0
        }
        
        if ad.region_code != nil {
            paramDixt["region_code"] = region_code
        }
        else
        {
            paramDixt["region_code"] = 0
        }

        paramDixt["country_id"] = ad.country_id
        paramDixt["telephone"] = ad.telephone!
        paramDixt["postcode"] = ad.postcode!
        paramDixt["city"] = ad.city
        if ad.street != nil {
            paramDixt["street"] = ["\(ad.street!)"]
        }
        if ad.street2 != nil {
            paramDixt["street"] = ["\(ad.street2!)"]
        }
        
        if ad.street2 != nil  &&  ad.street != nil {
 paramDixt["street"] = ["\(ad.street!)","\(ad.street2!)"]
        }
        
       
        paramDixt["same_as_billing"] = 0;
        
        paramDixt["save_in_address_book"] = 0
        paramDixt["save_in_address_book"] = 1
        
        
        
        finalParamDict["shipping_address"] = paramDixt;
        finalParamDict["billing_address"] = paramDixt;
        if shippingMethod != nil {
            finalParamDict["shipping_method_code"] = shippingMethod?.method_code;
            finalParamDict["shipping_carrier_code"] = shippingMethod?.carrier_code;
        }
        else
        {
            finalParamDict["shipping_method_code"] = "flatrate";
            finalParamDict["shipping_carrier_code"] = "flatrate";

        }
        
        addressINfoDict["addressInformation"] = finalParamDict
        
        return addressINfoDict;
    }
    
    
}
public class ShippingMethod: NSObject {
    
    public  var carrier_code: String?
    public  var method_code: String?
    public var carrier_title: String?
    public var method_title: String?
    public  var amount: Int?
    public  var base_amount: Int?
    public  var price_excl_tax: Int?
    public  var price_incl_tax: Int?
    
    
    required public override init() {
        super.init();
    }
    
    convenience init(map:JSON) {
        self.init()
        self.carrier_code  = map["carrier_code"].string
        self.method_code    = map["method_code"].string
        self.carrier_title        =  map["carrier_title"].string
        self.method_title     = map["method_title"].string
        self.amount           =  map["amount"].int
        self.base_amount           =  map["base_amount"].int
        self.price_excl_tax           =  map["price_excl_tax"].int
        self.price_incl_tax           =  map["price_incl_tax"].int}
    
    
}
public class UserDetails: NSObject {
    
   
    public  var firstname: String?
    public var lastname: String?
    public var email: String?
    public  var password: String?
    public  var telephone: String?
    public  var postcode: String?
    public  var city: String?
    public  var website_id: Int?
   
    required public override init() {
        super.init();
    }
    
    convenience init(map:JSON) {
        self.init()
        
        self.firstname  = map["firstname"].string
        self.lastname   = map["lastname"].string
        self.email      =  map["email"].string
       
        
        
        let addresses = map["addresses"];
        
        for (key,subJson):(String, JSON) in addresses {
            self.telephone   = subJson["telephone"].string
            self.postcode   =  subJson["postcode"].string
            self.city      =  subJson["city"].string
            
            if (self.telephone != nil && self.postcode != nil && self.city != nil) {
                break;
            }
        }

        if self.firstname == nil {
            self.firstname = "";
        }
        if self.lastname == nil {
            self.lastname = "";
        }
        if self.email == nil {
            self.email = "";
        }
        if self.telephone == nil {
            self.telephone = "";
        }
        if self.postcode == nil {
            self.postcode = "";
        }
        if self.city == nil {
            self.city = "";
        }
        
    }
    
    
}

public class Country : NSObject
{
    public  var countryid: String?
    public var two_letter_abbreviation: String?
    public var three_letter_abbreviation: String?
    public  var full_name_locale: String?
    public  var full_name_english: String?
    public  var available_regions: [Region]?
    
    
    required public override init() {
        super.init();
    }
    
    convenience init(map:JSON) {
        self.init()
        
        self.countryid  = map["id"].string
        self.two_letter_abbreviation   = map["two_letter_abbreviation"].string
        self.three_letter_abbreviation      =  map["three_letter_abbreviation"].string
        self.full_name_locale      =  map["full_name_locale"].string
        self.three_letter_abbreviation      =  map["three_letter_abbreviation"].string
        self.full_name_english      =  map["full_name_english"].string
        let avRegions = map["available_regions"]
        if avRegions != nil
        {
            var featureMdlAr = [Region]();
            for (key,subJson):(String, JSON) in avRegions {
                let banner = Region.init(map: subJson);
                featureMdlAr.append(banner);
            }
            self.available_regions = featureMdlAr;
        }
    }
}

public class Region : NSObject
{
    public  var id: Int?
    public var name: String?
    public var code: String?

    
    
    required public override init() {
        super.init();
    }
    
    convenience init(map:JSON) {
        self.init()
        self.id  = map["id"].int
        
        if(self.id == nil){
            self.id = Int(map["id"].string!)
        }
        
        self.name   = map["name"].string
        self.code      =  map["code"].string
    
        
    }
}

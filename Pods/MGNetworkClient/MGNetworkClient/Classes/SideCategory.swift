//
//  SideCategory.swift
//  Pods
//
//  Created by Bunty on 02/07/16.
//
//

import UIKit
import SwiftyJSON

public class SideCategory: NSObject {
    public  var id: Int?
    public  var parent_id: Int?
    public var is_active: Bool?
    public var product_count: Int?
    public var level: Int?
     public var name: String?
    public  var children_data = [SideCategory]()
    
    required public override init() {
        super.init();
    }
    
   public convenience init(map:JSON?) {
        self.init()
        self.id  = map?["id"].int
        self.parent_id    = map?["parent_id"].int
        self.level    = map?["level"].int
        self.is_active        =  map?["is_active"].bool
        self.product_count     = map?["product_count"].int
        self.name                  = map?["name"].string
    
    //if(self.level != nil && self.level != 4){
            let childArr =  map?["children_data"]
        
                if childArr != nil {
                    var childMdlAr = [SideCategory]();
                    for  (key,subJson):(String, JSON) in childArr! {
                        let sideCat = SideCategory.init(map: subJson);
                        if sideCat.is_active == true && self.level < 4
                        {
                            childMdlAr.append(sideCat);
                        }
           
                    }
                    self.children_data = childMdlAr;
        }
       // }
    }

}

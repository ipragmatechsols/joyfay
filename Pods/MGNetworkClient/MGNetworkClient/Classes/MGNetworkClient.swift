//
//  MGNetworkClient.swift
//  Pods
//
//  Created by vikaskumar on 7/1/16.
// Changed
// for updating version

import UIKit
import IPServerUtility
import Alamofire
import SwiftyJSON
import LNRSimpleNotifications




public  typealias ToKenHandler = (NSError?, String?)->()?
public class MGNetworkClient {
    
    //MARK : BASEURL
    public static let host = "http://54.160.33.236"
   // public static let host = "http://46.101.189.8"
   //public static let host = "http://magento22.ipragmatech.com"
    public var baseUrl = "\(host)/rest/V1/";
    public var imageBaseUrl = host;
    public var countries:[Country]?;
  //  public var imageMiddleUrl = "/pub/media/catalog/product";
      public var imageMiddleUrl = "/media/catalog/product";
    public let errorAlert = LNRNotificationManager()
    public let successAlert = LNRNotificationManager()
    public var lastReachable:Bool = true;
    public var networkReachabilityManager = NetworkReachabilityManager();
    public let undefinedError =  NSError.init(domain: NSURLErrorDomain, code: 1009, userInfo: [NSLocalizedDescriptionKey:"Undefined Error"]);
    let serviceInstance:ServerUtility = ServerUtility.sharedInstance;
    public static let sharedClient = MGNetworkClient.init()
    public let prefrences = NSUserDefaults.standardUserDefaults();
    private init() {
        serviceInstance.setConfiguration(self.baseUrl);
        serviceInstance.authToken = self.authToken();
        self.setUpAlerts();
        self.startMonitoringNetwork();
    }
    
    
    
    
    public func signUpwithParams(user:User , completion: (NSError?, User?)->Void){
        
        let params:[String:AnyObject] = ["customer":["firstname":user.firstname!,"lastname":user.lastname!,"email":user.email!],"password":user.password!];
        self.serviceInstance.makeRequest(ofType: .POST, onPath: "customers", withParams: params) { (error:NSError?, fetchedResponse:JSON?)  in
            let response = fetchedResponse;
            if ((error) != nil)
            {
                self.showErrorALertWithMessage(error!.localizedDescription);
                completion(error,nil);
            }
            else if((response) != nil )
            {
                if (response!["id"] != nil)
                {
                    let fetchedUser = User.init(map: response!);
                    completion(nil,fetchedUser);
                    
                    
                }
                else
                {
                    if ((response!["message"]) != nil)
                    {
                        let er = self.errorOccuredWithMessage(response!["message"].string!)
                        self.showErrorALertWithMessage(er.localizedDescription);
                        completion(er,nil);
                    }
                    else
                    {
                        self.showErrorALertWithMessage(self.undefinedError.localizedDescription);
                        completion(self.undefinedErrorOccured(),nil);
                    }
                }
                
            }
            else
            {
                self.showErrorALertWithMessage(self.undefinedError.localizedDescription);
                completion(self.undefinedError,nil);
            }
            
            return ();
            
        }
    }
    
    public func getBanners(completion: (NSError?, Home?)->Void)
    {
//        if self.isNetworkAvailable() == false {
//            completion(self.undefinedError,nil);
//            return;
//        }
        
        self.serviceInstance.makeRequest(ofType: .GET, onPath: "banners/banner", withParams: nil) { (error:NSError?, response:JSON?)  in
            
            if ((error) != nil)
            {
                self.showErrorALertWithMessage(error!.localizedDescription);
                completion(error,nil);
            }
            else if((response) != nil )
            {
                if ((response!["message"]) != nil)
                {
                    let er = self.errorOccuredWithMessage(response!["message"].string!)
                    self.showErrorALertWithMessage(er.localizedDescription);
                    completion(er,nil);
                }
                else
                {
                    
                    if response?.count > 0
                    {
                        let t = response![0];
                        let homeObj = Home.init(map: t);
                        completion(nil,homeObj)
                        
                    }
                    else
                    {
                        
                        self.showErrorALertWithMessage(self.undefinedError.localizedDescription);
                        completion(self.undefinedError,nil);
                    }
                }
                
            }
            else
            {
                self.showErrorALertWithMessage(self.undefinedError.localizedDescription);
                completion(self.undefinedError,nil);
            }
            
            return ();
            
        }
        
    }
    
    
    
    public func getCategories(completion: (NSError?, SideCategory?)->Void)
    {
        
        
        self.serviceInstance.makeRequest(ofType: .GET, onPath: "categories", withParams: nil) { (error:NSError?, fetchedResponse:JSON?)  in
            let response = fetchedResponse
            if ((error) != nil)
            {
                completion(error,nil);
            }
            else if((response) != nil )
            {
                if ((response!["message"]) != nil)
               {
                    let er = self.errorOccuredWithMessage(response!["message"].string!)
                    self.showErrorALertWithMessage(er.localizedDescription);
                    completion(er,nil);
                }
                else
                {
                    
                    let sideCat = SideCategory.init(map: fetchedResponse);
                    completion(nil,sideCat);
                    
                }
                
            }
            else
            {
                completion(self.undefinedErrorOccured(),nil);
            }
            
            return ();
            
        }
        
    }
    
    public func getProducts(categoryIds:[Int],completion: (NSError?, [Product]?)->Void)
    {
        //let path = "categories/\(categoryId)/products"
        let stringArray = categoryIds.flatMap { String($0) }
        let stringRepresentation = stringArray.joinWithSeparator(",")
        let idSTrt =  stringRepresentation
        let path =  "products/?searchCriteria[filter_groups][0][filters][0][field]=category_id&searchCriteria[filter_groups][0][filters][0][value]=\(idSTrt)&searchCriteria[filter_groups][0][filters][0][condition_type]=in&searchCriteria[current_page]=0&searchCriteria[page_size]=20"
        self.serviceInstance.makeRequest(ofType: .GET, onPath: path, withParams: nil) { (error:NSError?, fetchedResponse:JSON?)  in
            let response = fetchedResponse?["items"]
            if ((error) != nil)
            {
                completion(error,nil);
            }
            else if((response) != nil )
            {
                if ((response!["message"]) != nil)
                {
                    let er = self.errorOccuredWithMessage(response!["message"].string!)
                    self.showErrorALertWithMessage(er.localizedDescription);
                    completion(er,nil);
                }
                else
                {
                    
                    var productMdlAr = [Product]();
                    for (key,subJson):(String, JSON) in response! {
                        let banner = Product.init(map: subJson);
                        productMdlAr.append(banner);
                    }
                    completion(nil,productMdlAr);
                }
                
            }
            else
            {
                completion(self.undefinedErrorOccured(),nil);
            }
            
            return ();
            
        }
        
    }
    
   /* http://54.160.33.236/rest/V1/products/?searchCriteria[filter_groups][3][filters][0][condition_type]=eq&searchCriteria[filter_groups][2][filters][0][value]=53&searchCriteria[filter_groups][3][filters][0][field]=category_id&searchCriteria[filter_groups][3][filters][0][value]=115&searchCriteria[filter_groups][1][filters][0][condition_type]=eq&searchCriteria[filter_groups][1][filters][0][field]=size&searchCriteria[filter_groups][2][filters][0][condition_type]=eq&searchCriteria[filter_groups][1][filters][0][value]=167&searchCriteria[filter_groups][2][filters][0][field]=color&searchCriteria[current_page]=0&searchCriteria[page_size]=40*/
    
    public func getProductsBYFilter(fields: [String: String],name:String,completion: (NSError?, [Product]?)->Void)
    {
        
        var paramsString = ""
        
         for (key,value):(String, String) in fields {
            paramsString = "searchCriteria[filter_groups][0][filters][0][field]=" + key + "&" +
                           "searchCriteria[filter_groups][0][filters][0][value]=" + value + "&" +
                           "searchCriteria[filter_groups][0][filters][0][condition_type]=in"
        }
        
        
        
        let path =  String(format:"products/?%@&searchCriteria[current_page]=0&searchCriteria[page_size]=50", paramsString)
        
        self.serviceInstance.makeRequest(ofType: .GET, onPath: path, withParams: nil) { (error:NSError?, fetchedResponse:JSON?)  in
            let response = fetchedResponse?["items"]
            if ((error) != nil)
            {
                completion(error,nil);
            }
            else if((response) != nil )
            {
                if ((response!["message"]) != nil)
                {
                    let er = self.errorOccuredWithMessage(response!["message"].string!)
                    self.showErrorALertWithMessage(er.localizedDescription);
                    completion(er,nil);
                }
                else
                {
                    
                    var productMdlAr = [Product]();
                    for (key,subJson):(String, JSON) in response! {
                        let banner = Product.init(map: subJson);
                        productMdlAr.append(banner);
                    }
                    completion(nil,productMdlAr);
                }
                
            }
            else
            {
                completion(self.undefinedErrorOccured(),nil);
            }
            
            return ();
            
        }
        
    }
    //http://54.160.33.236/rest/V1/products? searchCriteria[filter_groups][0][filters][0][field]=name& searchCriteria[filter_groups][0][filters][0][value]=%25teddy%25& searchCriteria[filter_groups][0][filters][0][condition_type]=like

    
    
    public func getProductsBYName(name:String,completion: (NSError?, [Product]?)->Void)
    {
        //let path = "categories/\(categoryId)/products"
//        let stringArray = categoryIds.flatMap { String($0) }
//        let stringRepresentation = stringArray.joinWithSeparator(",")
//        let idSTrt =  stringRepresentation
//        let path =  "products/?searchCriteria[filter_groups][0][filters][0][field]=name&searchCriteria[filter_groups][0][filters][0][value]=%25\(name)%25&searchCriteria[filter_groups][0][filters][0][condition_type]=like&searchCriteria[current_page]=0&searchCriteria[page_size]=50"
        
            let path =  "products/?searchCriteria[filter_groups][0][filters][0][field]=name&searchCriteria[filter_groups][0][filters][0][value]=%25\(name)%25&searchCriteria[filter_groups][0][filters][0][condition_type]=like&searchCriteria[filter_groups][0][filters][0][field]=sku&searchCriteria[filter_groups][0][filters][0][value]=%25\(name)%25&searchCriteria[filter_groups][0][filters][0][condition_type]=like&searchCriteria[current_page]=0&searchCriteria[page_size]=50"
        
        self.serviceInstance.makeRequest(ofType: .GET, onPath: path, withParams: nil) { (error:NSError?, fetchedResponse:JSON?)  in
            let response = fetchedResponse?["items"]
            if ((error) != nil)
            {
                completion(error,nil);
            }
            else if((response) != nil )
            {
                if ((response!["message"]) != nil)
                {
                    let er = self.errorOccuredWithMessage(response!["message"].string!)
                    self.showErrorALertWithMessage(er.localizedDescription);
                    completion(er,nil);
                }
                else
                {
                    
                    var productMdlAr = [Product]();
                    for (key,subJson):(String, JSON) in response! {
                        let banner = Product.init(map: subJson);
                        productMdlAr.append(banner);
                    }
                    completion(nil,productMdlAr);
                }
                
            }
            else
            {
                completion(self.undefinedErrorOccured(),nil);
            }
            
            return ();
            
        }
        
    }
    //http://54.160.33.236/rest/V1/products? searchCriteria[filter_groups][0][filters][0][field]=name& searchCriteria[filter_groups][0][filters][0][value]=%25teddy%25& searchCriteria[filter_groups][0][filters][0][condition_type]=like
    
    public func getProductsDetailfromSKU(skuID:String?,completion: (NSError?, Product?)->Void)
    {
        //let path = "categories/\(categoryId)/products"
        let path =  "products/" + skuID!
        var escapedString = path.stringByAddingPercentEncodingWithAllowedCharacters(.URLPathAllowedCharacterSet())
        self.serviceInstance.makeRequest(ofType: .GET, onPath: escapedString!, withParams: nil) { (error:NSError?, fetchedResponse:JSON?)  in
            let response = fetchedResponse;
            if ((error) != nil)
            {
                completion(error,nil);
            }
            else if((response) != nil )
            {
                
                if ((response!["message"]) != nil)
                {
                    let er = self.errorOccuredWithMessage(response!["message"].string!)
                    self.showErrorALertWithMessage(er.localizedDescription);
                    completion(er,nil);
                }
                else
                {
                    
                    let prd = Product.init(map: response);
                    
                    
                    completion(nil,prd);
                }
                
            }
            else
            {
                completion(self.undefinedErrorOccured(),nil);
            }
            
            return ();
            
        }
        
    }
    public func createACart(completion: (NSError?, String?)->Void)
    {
        //let path = "categories/\(categoryId)/products"
        let path =  "carts/mine/"
        self.serviceInstance.makeRequest(ofType: .POST, onPath: path, withParams: nil) { (error:NSError?, fetchedResponse:JSON?)  in
            let response = fetchedResponse;
            if ((error) != nil)
            {
                completion(error,nil);
            }
            else if((response) != nil )
            {
                
                if ((response!["message"]) != nil)
                {
                    let er = self.errorOccuredWithMessage(response!["message"].string!)
                    self.showErrorALertWithMessage(er.localizedDescription);
                    completion(er,nil);
                }
                else
                {
                    
                    let cartID = response?.string
                    self.saveCartID(cartID);
                    
                    
                    completion(nil,cartID);
                }
                
            }
            else
            {
                completion(self.undefinedErrorOccured(),nil);
            }
            
            return ();
            
        }
        
    }
    
    
    public func addItemToWishList(productId:String,completion: (NSError?, Bool?)->Void)
    {
        //let path = "categories/\(categoryId)/products"
        //self.printJSON(params);
        let path =  "ipwishlist/add/\(productId)"
        self.serviceInstance.makeRequest(ofType: .POST, onPath: path, withParams: nil) { (error:NSError?, fetchedResponse:JSON?)  in
            let response = fetchedResponse;
            if ((error) != nil)
            {
                completion(error,nil);
            }
            else if((response) != nil )
            {
                
                
                
                if ((response!["message"]) != nil)
                {
                    let er = self.errorOccuredWithMessage(response!["message"].string!)
                    self.showErrorALertWithMessage(er.localizedDescription);
                    completion(er,nil);
                }
                else
                {
                    
                    // let cartIt = subJson["cartItem"]
                   
                    
                    
                    completion(nil,response?.bool);
                }
                
                
                
            }
            else
            {
                completion(self.undefinedErrorOccured(),nil);
            }
            
            return ();
            
        }
        
    }

    
    
    
    
    public func addItemToCart(params:[String:AnyObject],completion: (NSError?, CartItem?)->Void)
    {
        //let path = "categories/\(categoryId)/products"
        self.printJSON(params);
        let path =  "carts/mine/items/"
        self.serviceInstance.makeRequest(ofType: .POST, onPath: path, withParams: params) { (error:NSError?, fetchedResponse:JSON?)  in
            let response = fetchedResponse;
            if ((error) != nil)
            {
                completion(error,nil);
            }
            else if((response) != nil )
            {
                
                
                
                if ((response!["message"]) != nil)
                {
                    let er = self.errorOccuredWithMessage(response!["message"].string!)
                    self.showErrorALertWithMessage(er.localizedDescription);
                    completion(er,nil);
                }
                else
                {
                    
                    // let cartIt = subJson["cartItem"]
                    let cart = CartItem.init(map: response);
                    
                    
                    completion(nil,cart);
                }
                
                
                
            }
            else
            {
                completion(self.undefinedErrorOccured(),nil);
            }
            
            return ();
            
        }
        
    }
    
    public func addReviewToItem(params:[String:AnyObject],completion: (NSError?, Bool?)->Void)
    {
        //let path = "categories/\(categoryId)/products"
        self.printJSON(params);
        let path =  "review/mine/post"
        self.serviceInstance.makeRequest(ofType: .POST, onPath: path, withParams: params) { (error:NSError?, fetchedResponse:JSON?)  in
            let response = fetchedResponse;
            if ((error) != nil)
            {
                completion(error,nil);
            }
            else if((response) != nil )
            {
                
                
                
                if ((response!["message"]) != nil)
                {
                    let er = self.errorOccuredWithMessage(response!["message"].string!)
                    self.showErrorALertWithMessage(er.localizedDescription);
                    completion(er,nil);
                }
                else
                {
                    
                    // let cartIt = subJson["cartItem"]
                 //   let cart = CartItem.init(map: response);
                    
                    
                    completion(nil,true);
                }
                
                
                
            }
            else
            {
                completion(self.undefinedErrorOccured(),nil);
            }
            
            return ();
            
        }
        
    }

    
    public func editAIteminCart(params:[String:AnyObject],completion: (NSError?, CartItem?)->Void)
    {
        //let path = "categories/\(categoryId)/products"
        
        let path =  "carts/mine/items/"
        self.serviceInstance.makeRequest(ofType: .POST, onPath: path, withParams: params) { (error:NSError?, fetchedResponse:JSON?)  in
            let response = fetchedResponse;
            if ((error) != nil)
            {
                completion(error,nil);
            }
            else if((response) != nil )
            {
                
                
                
                if ((response!["message"]) != nil)
                {
                    let er = self.errorOccuredWithMessage(response!["message"].string!)
                    self.showErrorALertWithMessage(er.localizedDescription);
                    completion(er,nil);
                }
                else
                {
                    
                    // let cartIt = subJson["cartItem"]
                    let cart = CartItem.init(map: response);
                    
                    
                    completion(nil,cart);
                }
                
                
                
            }
            else
            {
                completion(self.undefinedErrorOccured(),nil);
            }
            
            return ();
            
        }
        
    }
    
    public func getCountries()
    {
        self.getcountryLIst(["":""]) { (er:NSError?, cts: [Country]?) in
            
            if er == nil
            {
            if cts != nil
            {
                MGNetworkClient.sharedClient.countries = cts;
               // MGNetworkClient.sharedClient.countries!.sort({ $0.full_name_english > $1.full_name_english })
              
                MGNetworkClient.sharedClient.countries!.sortInPlace {(customer1:Country, customer2:Country) -> Bool in
                    customer1.full_name_english < customer2.full_name_english
                }
                }
        
            }
    
        }
    }
    
    public func getcountryLIst(params:[String:AnyObject],completion: (NSError?, [Country]?)->Void)
    {
        //let path = "categories/\(categoryId)/products"
        
        let path =  "directory/countries"
        self.serviceInstance.makeRequest(ofType: .GET, onPath: path, withParams: params) { (error:NSError?, fetchedResponse:JSON?)  in
            
            
            let response = fetchedResponse;
            if ((error) != nil)
            {
                completion(error,nil);
            }
            else if((response) != nil )
            {
                
                
                //                let jsonData = try! NSJSONSerialization.dataWithJSONObject((response?.arrayObject!)!, options: NSJSONWritingOptions.PrettyPrinted)
                //
                //                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)! as String
                //
                //                print(jsonString)
                
                if ((response!["message"]) != nil)
                {
                    
                        let er = self.errorOccuredWithMessage(response!["message"].string!)
                        self.showErrorALertWithMessage(er.localizedDescription);
                        completion(er,nil);
                    
                }
                else
                {
                    
                    if response?.array?.count > 0
                    {
                        var cartMdlAr = [Country]();
                        for (key,subJson):(String, JSON) in response! {
                            let cart = Country.init(map: subJson);
                            cartMdlAr.append(cart);
                        }
                        completion(nil,cartMdlAr);
                    }else
                    {
                        completion(nil,[Country]());
                    }
                }
                
                
                
                
                
                
            }
            else
            {
                completion(self.undefinedErrorOccured(),nil);
            }
            
            return ();
            
        }
        
    }
    
    public func getAllWishListItems(params:[String:AnyObject],completion: (NSError?, [WishListItem]?)->Void)
    {
        //let path = "categories/\(categoryId)/products"

        let path =  "ipwishlist/items"
        self.serviceInstance.makeRequest(ofType: .GET, onPath: path, withParams: params) { (error:NSError?, fetchedResponse:JSON?)  in
            
            
            let response = fetchedResponse;
            if ((error) != nil)
            {
                completion(error,nil);
            }
            else if((response) != nil )
            {
                
                
                //                let jsonData = try! NSJSONSerialization.dataWithJSONObject((response?.arrayObject!)!, options: NSJSONWritingOptions.PrettyPrinted)
                //
                //                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)! as String
                //
                //                print(jsonString)
                
                if ((response!["message"]) != nil)
                {
                    //let er = self.errorOccuredWithMessage(response!["message"].string!)
                    let parameters = response!["parameters"]
                    let  fieldName = parameters["fieldName"].string
                    if fieldName == "cartId"
                    {
                        //                        let er = self.errorOccuredWithMessage("There is no item in your cart")
                        //                        self.showErrorALertWithMessage(er.localizedDescription);
                        completion(nil,[WishListItem]());
                    }
                    else
                    {
                        let er = self.errorOccuredWithMessage(response!["message"].string!)
                        self.showErrorALertWithMessage(er.localizedDescription);
                        completion(er,nil);
                    }
                }
                else
                {
                    
                    if response?.array?.count > 0
                    {
                        var cartMdlAr = [WishListItem]();
                        for (key,subJson):(String, JSON) in response! {
                            let cart = WishListItem.init(map: subJson);
                            cartMdlAr.append(cart);
                        }
                        completion(nil,cartMdlAr);
                    }else
                    {
                        completion(nil,[WishListItem]());
                    }
                }
                
                
                
                
                
                
            }
            else
            {
                completion(self.undefinedErrorOccured(),nil);
            }
            
            return ();
            
        }
        
    }

    public func getAllReviews(productID:String,completion: (NSError?, [Review]?)->Void)
    {
        //let path = "categories/\(categoryId)/products"
       
        let path =  "review/reviews/\(productID)"
        self.serviceInstance.makeRequest(ofType: .GET, onPath: path, withParams: nil) { (error:NSError?, fetchedResponse:JSON?)  in
            
            
            let response = fetchedResponse;
            if ((error) != nil)
            {
                completion(error,nil);
            }
            else if((response) != nil )
            {
                //  let jsonData = try! NSJSONSerialization.dataWithJSONObject((response?.arrayObject!)!, options: NSJSONWritingOptions.PrettyPrinted)
                //
                //                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)! as String
                //
                //                print(jsonString)
                
                if ((response!["message"]) != nil)
                {
                    //let er = self.errorOccuredWithMessage(response!["message"].string!)
                    let parameters = response!["parameters"]
                    let  fieldName = parameters["fieldName"].string
                    if fieldName == "cartId"
                    {
                        //                        let er = self.errorOccuredWithMessage("There is no item in your cart")
                        //                        self.showErrorALertWithMessage(er.localizedDescription);
                        completion(nil,[Review]());
                    }
                    else
                    {
                        let er = self.errorOccuredWithMessage(response!["message"].string!)
                        self.showErrorALertWithMessage(er.localizedDescription);
                        completion(er,nil);
                    }
                }
                else
                {
                    
                    if response?.array?.count > 0
                    {
                        
                        let reviews = response?.array![0]["reviews"]
                        var cartMdlAr = [Review]();
                        for (key,subJson):(String, JSON) in reviews! {
                            let cart = Review.init(map: subJson);
                            cartMdlAr.append(cart);
                        }
                        completion(nil,cartMdlAr);
                    }else
                    {
                        completion(nil,[Review]());
                    }
                }
                
            }
            else
            {
                completion(self.undefinedErrorOccured(),nil);
            }
            
            return ();
            
        }
        
    }
    
    
    public func getAllCartItems(params:[String:AnyObject],completion: (NSError?, [CartItem]?)->Void)
    {
        //let path = "categories/\(categoryId)/products"
        if self.countries == nil {
            self.getCountries();
        }
        let path =  "carts/mine/items/"
        self.serviceInstance.makeRequest(ofType: .GET, onPath: path, withParams: params) { (error:NSError?, fetchedResponse:JSON?)  in
            
            
            let response = fetchedResponse;
            if ((error) != nil)
            {
                completion(error,nil);
            }
            else if((response) != nil )
            {
                
                
//                let jsonData = try! NSJSONSerialization.dataWithJSONObject((response?.arrayObject!)!, options: NSJSONWritingOptions.PrettyPrinted)
//                
//                let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)! as String
//                
//                print(jsonString)
                
                if ((response!["message"]) != nil)
                {
                    //let er = self.errorOccuredWithMessage(response!["message"].string!)
                    let parameters = response!["parameters"]
                    let  fieldName = parameters["fieldName"].string
                    if fieldName == "cartId"
                    {
                        //                        let er = self.errorOccuredWithMessage("There is no item in your cart")
                        //                        self.showErrorALertWithMessage(er.localizedDescription);
                        completion(nil,[CartItem]());
                    }
                    else
                    {
                        let er = self.errorOccuredWithMessage(response!["message"].string!)
                        self.showErrorALertWithMessage(er.localizedDescription);
                        completion(er,nil);
                    }
                }
                else
                {
                    
                    if response?.array?.count > 0
                    {
                        var cartMdlAr = [CartItem]();
                        for (key,subJson):(String, JSON) in response! {
                            let cart = CartItem.init(map: subJson);
                            print(cart.product?.price)
                            cartMdlAr.append(cart);
                        }
                        completion(nil,cartMdlAr);
                    }else
                    {
                        completion(nil,[CartItem]());
                    }
                }
                
                
                
                
                
                
            }
            else
            {
                completion(self.undefinedErrorOccured(),nil);
            }
            
            return ();
            
        }
        
    }
    public func getAllOrders(params:[String:AnyObject]?,completion: (NSError?, [Order]?)->Void)
    {
        //let path = "categories/\(categoryId)/products"
        
        let path =  "order/myorder/"
        self.serviceInstance.makeRequest(ofType: .GET, onPath: path, withParams: params) { (error:NSError?, fetchedResponse:JSON?)  in
            let response = fetchedResponse;
            if ((error) != nil)
            {
                completion(error,nil);
            }
            else if((response) != nil )
            {
                
                
                
                
                if ((response!["message"]) != nil)
                {
                    //                    //let er = self.errorOccuredWithMessage(response!["message"].string!)
                    //                    let parameters = response!["parameters"]
                    //                    let  fieldName = parameters["fieldName"].string
                    //                    if fieldName == "cartId"
                    //                    {
                    //                        //                        let er = self.errorOccuredWithMessage("There is no item in your cart")
                    //                        //                        self.showErrorALertWithMessage(er.localizedDescription);
                    //                        completion(nil,[CartItem]());
                    //                    }
                    //                    else
                    //                    {
                    //let er = self.errorOccuredWithMessage(response!["message"].string!)
                   
                    let er = self.errorOccuredWithMessage("You dont have permission to access this service")
                    self.showErrorALertWithMessage(er.localizedDescription);
                    completion(er,nil);
                    //}
                }
                else
                {
                    
                    if response?.array?.count > 0
                    {
                        var cartMdlAr = [Order]();
                        for (key,subJson):(String, JSON) in response! {
                            let cart = Order.init(map: subJson);
                            cartMdlAr.append(cart);
                        }
                        completion(nil,cartMdlAr);
                    }else
                    {
                        completion(nil,[Order]());
                    }
                }
                
                
                
                
                
                
            }
            else
            {
                completion(self.undefinedErrorOccured(),nil);
            }
            
            return ();
            
        }
        
    }
    
    public func getAllShippingMethods(params:[String:AnyObject]?,completion: (NSError?, [ShippingMethod]?)->Void)
    {
        //let path = "categories/\(categoryId)/products"
        
        let path =  "carts/mine/shipping-methods"
        self.serviceInstance.makeRequest(ofType: .GET, onPath: path, withParams: nil) { (error:NSError?, fetchedResponse:JSON?)  in
            let response = fetchedResponse;
            if ((error) != nil)
            {
                completion(error,nil);
            }
            else if((response) != nil )
            {
                
                if ((response!["message"]) != nil)
                {
                    let er = self.errorOccuredWithMessage(response!["message"].string!)
                    self.showErrorALertWithMessage(er.localizedDescription);
                    completion(er,nil);
                }
                else
                {
                    if response?.array?.count > 0
                    {
                        var cartMdlAr = [ShippingMethod]();
                        for (key,subJson):(String, JSON) in response! {
                            let cart = ShippingMethod.init(map: subJson);
                            cartMdlAr.append(cart);
                        }
                        completion(nil,cartMdlAr);
                    }else
                    {
                        completion(nil,[ShippingMethod]());
                    }
                    
                    
                    
                   
                }
                
                
                
                
                
                
            }
            else
            {
                completion(self.undefinedErrorOccured(),nil);
            }
            
            return ();
            
        }
        
    }
    
    public func getAllAddresses(params:[String:AnyObject]?,completion: (NSError?, [Addres]?)->Void)
    {
        
        
       // let path =  "carts/mine/billing-address"
         let path =  "customers/me"
        
        self.serviceInstance.makeRequest(ofType: .GET, onPath: path, withParams: nil) { (error:NSError?, fetchedResponse:JSON?)  in
            let response = fetchedResponse;
            if ((error) != nil)
            {
                completion(error,nil);
            }
            else if((response) != nil )
            {
                
                if ((response!["message"]) != nil)
                {
                    let er = self.errorOccuredWithMessage(response!["message"].string!)
                    self.showErrorALertWithMessage(er.localizedDescription);
                    completion(er,nil);
                }
                else
                {
                    
                   // let address:Addres = Addres.init(map: response!);
                    
                    
                    let user = User.init(map: response!)
                    let fetchedUser = User.fetchSavedUser()
                    fetchedUser.firstname = user.firstname ?? fetchedUser.firstname ?? ""
                    fetchedUser.lastname = user.lastname ?? fetchedUser.lastname ?? ""
                    fetchedUser.website_id = user.website_id ?? fetchedUser.website_id ?? 0
                    User.saveUser(fetchedUser)
                    
                    let featuresAr = response!["addresses"];
                    var featureMdlAr = [Addres]();
                    for (key,subJson):(String, JSON) in featuresAr {
                        let banner = Addres.init(map: subJson);
                        if banner.email == nil
                        {
                        banner.email = User.fetchSavedUser().email;
                        }
                        featureMdlAr.append(banner);
                    }
                   // self.features = featureMdlAr;
                    completion(nil,featureMdlAr);
                }
                
                
                
                
                
                
            }
            else
            {
                completion(self.undefinedErrorOccured(),nil);
            }
            
            return ();
            
        }
        
    }

    

//    public func getAllAddresses(params:[String:AnyObject]?,completion: (NSError?, Addres?)->Void)
//    {
//       
//        
//        let path =  "carts/mine/billing-address"
//        // let path =  "customers/me"
//        
//        self.serviceInstance.makeRequest(ofType: .GET, onPath: path, withParams: nil) { (error:NSError?, fetchedResponse:JSON?)  in
//            let response = fetchedResponse;
//            if ((error) != nil)
//            {
//                completion(error,nil);
//            }
//            else if((response) != nil )
//            {
//                
//                if ((response!["message"]) != nil)
//                {
//                    let er = self.errorOccuredWithMessage(response!["message"].string!)
//                    self.showErrorALertWithMessage(er.localizedDescription);
//                    completion(er,nil);
//                }
//                else
//                {
//                    
//                    let address:Addres = Addres.init(map: response!);
//                    
//                    
//                    
//                    
//                    
//                    completion(nil,address);
//                }
//                
//                
//                
//                
//                
//                
//            }
//            else
//            {
//                completion(self.undefinedErrorOccured(),nil);
//            }
//            
//            return ();
//            
//        }
//        
//    }
    
    public func getUserDetails(params:[String:AnyObject]?,completion: (NSError?, UserDetails?)->Void)
    {
        //let path = "categories/\(categoryId)/products"
        
        let path =  "customers/me"
        self.serviceInstance.makeRequest(ofType: .GET, onPath: path, withParams: nil) { (error:NSError?, fetchedResponse:JSON?)  in
            let response = fetchedResponse;
            if ((error) != nil)
            {
                completion(error,nil);
            }
            else if((response) != nil )
            {
                
                if ((response!["message"]) != nil)
                {
                    let er = self.errorOccuredWithMessage(response!["message"].string!)
                    self.showErrorALertWithMessage(er.localizedDescription);
                    completion(er,nil);
                }
                else
                {
                    
                    let address:UserDetails = UserDetails.init(map: response!);
                    
                    
                    
                    
                    
                    completion(nil,address);
                }
                
                
                
                
                
                
            }
            else
            {
                completion(self.undefinedErrorOccured(),nil);
            }
            
            return ();
            
        }
        
    }
    public func saveUserDetails(params:[String:AnyObject]?,completion: (NSError?, UserDetails?)->Void)
    {
        //let path = "categories/\(categoryId)/products"
         let jsonData = try! NSJSONSerialization.dataWithJSONObject((params)!, options: NSJSONWritingOptions.PrettyPrinted)
                           let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)! as String
        
                    print(jsonString)
        let path =  "customers/me"
        self.serviceInstance.makeRequest(ofType: .PUT, onPath: path, withParams: params) { (error:NSError?, fetchedResponse:JSON?)  in
            let response = fetchedResponse;
            if ((error) != nil)
            {
                completion(error,nil);
            }
            else if((response) != nil )
            {
                
                if ((response!["message"]) != nil)
                {
                    let er = self.errorOccuredWithMessage(response!["message"].string!)
                    self.showErrorALertWithMessage(er.localizedDescription);
                    completion(er,nil);
                }
                else
                {
                    
                    let address:UserDetails = UserDetails.init(map: response!);
                    
                    
                    
                    
                    
                    completion(nil,address);
                }
                
                
                
                
                
                
            }
            else
            {
                completion(self.undefinedErrorOccured(),nil);
            }
            
            return ();
            
        }
        
    }
    public func saveUserProfile(completion: (NSError?, User?)->Void)
    {
        //let path = "categories/\(categoryId)/products"
        
        let path =  "customers/me"
        self.serviceInstance.makeRequest(ofType: .GET, onPath: path, withParams: nil) { (error:NSError?, fetchedResponse:JSON?)  in
            let response = fetchedResponse;
            if ((error) != nil)
            {
            }
            else if((response) != nil )
            {
                
                //                if ((response!["message"]) != nil)
                //                {
                //                    let er = self.errorOccuredWithMessage(response!["message"].string!)
                //                    self.showErrorALertWithMessage(er.localizedDescription);
                //                    completion(er,nil);
                //                }
                //                else
                //                {
                //
                let user:User = User.init(map: response!);
                
                User.saveUser(user);
                
                
                
                completion(nil,user);
            }
            
            
            
            return ();
            
            
        }
        
        
        
        
    }
    
    
    
    public func getTotalAmountToCheckout(params:[String:AnyObject]?,completion: (NSError?, Addres?)->Void)
    {
        //let path = "categories/\(categoryId)/products"
        
        let path =  "carts/mine/totals"
        self.serviceInstance.makeRequest(ofType: .GET, onPath: path, withParams: nil) { (error:NSError?, fetchedResponse:JSON?)  in
            let response = fetchedResponse;
            if ((error) != nil)
            {
                completion(error,nil);
            }
            else if((response) != nil )
            {
                
                if ((response!["message"]) != nil)
                {
                    let er = self.errorOccuredWithMessage(response!["message"].string!)
                    self.showErrorALertWithMessage(er.localizedDescription);
                    completion(er,nil);
                }
                else
                {
                    
                    let address:Addres = Addres.init(map: response!);
                    
                    
                    
                    
                    
                    completion(nil,address);
                }
                
                
                
                
                
                
            }
            else
            {
                completion(self.undefinedErrorOccured(),nil);
            }
            
            return ();
            
        }
        
    }
    
    
    
    
    
    public func resetPassword(params:[String:AnyObject]?,completion: (NSError?, Bool?)->Void)
    {
        //let path = "categories/\(categoryId)/products"
        
        let path =  "customers/password"
        self.serviceInstance.makeRequest(ofType: .PUT, onPath: path, withParams: params) { (error:NSError?, fetchedResponse:JSON?)  in
            let response = fetchedResponse;
            if ((error) != nil)
            {
                completion(error,nil);
            }
            else if((response) != nil )
            {
                completion(nil,response?.boolValue);
            }
            
            
            
            return ();
            
            
        }
        
        
        
        
    }
    
    
    
    
    
    
    public func addAddresses(params:[String:AnyObject]?,completion: (NSError?, String?)->Void)
    {
        //let path = "categories/\(categoryId)/products"
        
        //let path =  "carts/mine/billing-address"
        let path =  "customers/me"
        self.serviceInstance.makeRequest(ofType: .PUT, onPath: path, withParams: params) { (error:NSError?, fetchedResponse:JSON?)  in
            let response = fetchedResponse;
            if ((error) != nil)
            {
                completion(error,nil);
            }
            else if((response) != nil )
            {
                if ((response!["message"]) != nil)
                {
                    let er = self.errorOccuredWithMessage(response!["message"].string!)
                    self.showErrorALertWithMessage(er.localizedDescription);
                    completion(er,nil);
                }
                else
                {
                    
                    let addressID = response?.string
                    
                    
                    completion(nil,addressID);
                }
                
                
                
                
                
                
            }
            else
            {
                completion(self.undefinedErrorOccured(),nil);
            }
            
            return ();
            
        }
        
    }
    
    public func makeShippingAddresses(params:[String:AnyObject]?,completion: (NSError?, Bool? , ShippingInfo?)->Void)
    {
        //let path = "categories/\(categoryId)/products"
        
        
        let path =  "carts/mine/shipping-information"
        self.serviceInstance.makeRequest(ofType: .POST, onPath: path, withParams: params) { (error:NSError?, fetchedResponse:JSON?)  in
            let response = fetchedResponse;
            
//            let jsonData = try! NSJSONSerialization.dataWithJSONObject((response?.dictionaryObject!)!, options: NSJSONWritingOptions.PrettyPrinted)
//            
//            let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)! as String
//            
//            print(jsonString)
            
            if ((error) != nil)
            {
                completion(error,nil,nil);
            }
            else if((response) != nil )
            {
                
                //let addressID = response?.string
                
                if ((response!["message"]) != nil)
                {
                    let er = self.errorOccuredWithMessage(response!["message"].string!)
                    self.showErrorALertWithMessage(er.localizedDescription);
                    completion(er,nil,nil);
                }
                else
                {
                    let info = ShippingInfo.init(map: response);
                    completion(nil,true,info);
                }
                
                
                
                
                
                
                
            }
            else
            {
                completion(self.undefinedErrorOccured(),nil,nil);
            }
            
            return ();
            
        }
        
    }
    public func makePayment(params:[String:AnyObject]?,completion: (NSError?, String?)->Void)
    {
        //let path = "categories/\(categoryId)/products"
        var error : NSError?
        
        let jsonData = try! NSJSONSerialization.dataWithJSONObject(params!, options: NSJSONWritingOptions.PrettyPrinted)
        
        let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)! as String
        
        print(jsonString)
        
        let path =  "carts/mine/order"
        self.serviceInstance.makeRequest(ofType: .PUT, onPath: path, withParams: params) { (error:NSError?, fetchedResponse:JSON?)  in
            let response = fetchedResponse;
            if ((error) != nil)
            {
                completion(error,nil);
            }
            else if((response) != nil )
            {
                if ((response!["message"]) != nil)
                {
                    let er = self.errorOccuredWithMessage(response!["message"].string!)
                    self.showErrorALertWithMessage(er.localizedDescription);
                    completion(er,nil);
                }
                else
                {
                    
                    let orderID = response?.string
                    
                    
                    completion(nil,orderID);
                }
                
                
                
                
                
                
            }
            else
            {
                completion(self.undefinedErrorOccured(),nil);
            }
            
            return ();
            
        }
        
    }
    public func saveAddress(params:[String:AnyObject]?,completion: (NSError?, String?)->Void)
    {
        //let path = "categories/\(categoryId)/products"
        var error : NSError?
        
        let jsonData = try! NSJSONSerialization.dataWithJSONObject(params!, options: NSJSONWritingOptions.PrettyPrinted)
        
        let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)! as String
        
        print(jsonString)
        
        let path =  "customers/me"
        self.serviceInstance.makeRequest(ofType: .PUT, onPath: path, withParams: params) { (error:NSError?, fetchedResponse:JSON?)  in
            let response = fetchedResponse;
            if ((error) != nil)
            {
                completion(error,nil);
            }
            else if((response) != nil )
            {
                if ((response!["message"]) != nil)
                {
                    let er = self.errorOccuredWithMessage(response!["message"].string!)
                    self.showErrorALertWithMessage(er.localizedDescription);
                    completion(er,nil);
                }
                else
                {
                    
                    let orderID = response?.string
                    
                    
                    completion(nil,orderID);
                }
                
                
                
                
                
                
            }
            else
            {
                completion(self.undefinedErrorOccured(),nil);
            }
            
            return ();
            
        }
        
    }
    public func changePassword(params:[String:AnyObject]?,completion: (NSError?, Bool?)->Void)
    {
        //let path = "categories/\(categoryId)/products"
        var error : NSError?
        
        let jsonData = try! NSJSONSerialization.dataWithJSONObject(params!, options: NSJSONWritingOptions.PrettyPrinted)
        
        let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)! as String
        
        print(jsonString)
        
        let path =  "customers/me/password"
        self.serviceInstance.makeRequest(ofType: .PUT, onPath: path, withParams: params) { (error:NSError?, fetchedResponse:JSON?)  in
            let response = fetchedResponse;
            if ((error) != nil)
            {
                completion(error,nil);
            }
            else if((response) != nil )
            {
                if ((response!["message"]) != nil)
                {
                    let er = self.errorOccuredWithMessage(response!["message"].string!)
                    self.showErrorALertWithMessage(er.localizedDescription);
                    completion(er,nil);
                }
                else
                {
                    
                    let orderID = response?.bool
                    
                    
                    completion(nil,orderID);
                }
                
                
                
                
                
                
            }
            else
            {
                completion(self.undefinedErrorOccured(),nil);
            }
            
            return ();
            
        }
        
    }
    
    public func deleteWishListItem(itemID:String?,completion: (NSError?, Bool?)->Void)
    {
        //let path = "categories/\(categoryId)/products"
        
        let path =  "ipwishlist/delete/" + itemID!;
        self.serviceInstance.makeRequest(ofType: .DELETE, onPath: path, withParams: nil) { (error:NSError?, fetchedResponse:JSON?)  in
            let response = fetchedResponse;
            if ((error) != nil)
            {
                completion(error,nil);
            }
            else if((response) != nil )
            {
                
                
                
                if ((response!["message"]) != nil)
                {
                    let er = self.errorOccuredWithMessage(response!["message"].string!)
                    self.showErrorALertWithMessage(er.localizedDescription);
                    completion(er,nil);
                }
                else
                {
                    
                    // let cartIt = subJson["cartItem"]
                    let cart = response?.bool;
                    
                    
                    completion(nil,cart);
                }
                
                
                
            }
            else
            {
                completion(self.undefinedErrorOccured(),nil);
            }
            
            return ();
            
        }
        
    }

    public func deleteItemToCart(itemID:String?,completion: (NSError?, CartItem?)->Void)
    {
        //let path = "categories/\(categoryId)/products"
        
        let path =  "carts/mine/items/" + itemID!;
        self.serviceInstance.makeRequest(ofType: .DELETE, onPath: path, withParams: nil) { (error:NSError?, fetchedResponse:JSON?)  in
            let response = fetchedResponse;
            if ((error) != nil)
            {
                completion(error,nil);
            }
            else if((response) != nil )
            {
                
                
                
                if ((response!["message"]) != nil)
                {
                    let er = self.errorOccuredWithMessage(response!["message"].string!)
                    self.showErrorALertWithMessage(er.localizedDescription);
                    completion(er,nil);
                }
                else
                {
                    
                    // let cartIt = subJson["cartItem"]
                    let cart = CartItem.init(map: response);
                    
                    
                    completion(nil,cart);
                }
                
                
                
            }
            else
            {
                completion(self.undefinedErrorOccured(),nil);
            }
            
            return ();
            
        }
        
    }
    
    
    //    public func getProducts(categoryId:Int,completion: (NSError?, [Product]?)->Void)
    //    {
    //        let path = "categories/\(categoryId)/products"
    //
    //        self.serviceInstance.makeRequest(ofType: .GET, onPath: path, withParams: nil) { (error:NSError?, fetchedResponse:JSON?)  in
    //            let response = fetchedResponse
    //            if ((error) != nil)
    //            {
    //                completion(error,nil);
    //            }
    //            else if((response) != nil )
    //            {
    //
    //                var productMdlAr = [Product]();
    //                for (key,subJson):(String, JSON) in response! {
    //                    let banner = Product.init(map: subJson);
    //                    productMdlAr.append(banner);
    //                }
    //                              completion(nil,productMdlAr);
    //
    //            }
    //            else
    //            {
    //                completion(self.undefinedErrorOccured(),nil);
    //            }
    //
    //            return ();
    //
    //        }
    //
    //    }
    
    
    
    public func getCustomerAuthenticationToken(user:User , completion: (NSError?, User?)->Void)
    {
        let params:[String:AnyObject] = ["username":user.email!,"password":user.password!];
        fetchToken(ofType: .POST, onPath: "integration/customer/token", withParams: params) { (error:NSError?, token:String?)  in
            
            if ((error) != nil)
            {
                self.showErrorALertWithMessage(error!.localizedDescription);
                completion(error,nil);
                
            }
                
            else if((token) != nil )
            {
                self.saveToken(token);
                self.serviceInstance.authToken = token;
                User.saveUser(user);
                
                completion(nil,user);
                
            }
            else
            {
                self.showErrorALertWithMessage(self.undefinedError.localizedDescription);
                
                completion(error,nil);
                
                
            }
            return ();
            
        }
        
    }
    
    
    
    
    
    
    
    
    
    //MARK: Auth Token
    
    public func fetchToken(ofType type:RequesType,  onPath:String, withParams:[String:AnyObject], responseHandler:ToKenHandler )  {
        
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        let fullUrl  = baseUrl + onPath;
        //  let headers = [""]
        let headers = [
            "Content-Type": "application/json"
        ]
        Alamofire.request(.POST, fullUrl,parameters: withParams,headers:headers,encoding: .JSON).validate(contentType: ["application/json","text/html"]).responseJSON { response in
            
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    // let json = JSON(value)
                    if let token = value as? String
                    {
                        responseHandler(nil,token);
                        
                    }
                    else  if ((value["message"]) != nil)
                    {
                        
                        let error  = NSError.init(domain: NSURLErrorDomain, code: 1009, userInfo: [NSLocalizedDescriptionKey:value["message"]! as! String]);
                        responseHandler(error,nil);
                    }
                    else
                    {
                        let error  = NSError.init(domain: NSURLErrorDomain, code: 1009, userInfo: [NSLocalizedDescriptionKey:"Undefined Error"]);
                        responseHandler(error,nil);
                    }
                    
                    
                }
                else
                {
                    let error  = NSError.init(domain: NSURLErrorDomain, code: 1009, userInfo: [NSLocalizedDescriptionKey:"Undefined Error"]);
                    responseHandler(error,nil);
                }
                
            case .Failure(let error):
                responseHandler(error,nil);
            }
        }
        
        
        
        
    }
    public func undefinedErrorOccured()-> NSError {
        let error  = NSError.init(domain: NSURLErrorDomain, code: 1009, userInfo: [NSLocalizedDescriptionKey:"Undefined Error"]);
        return error;
        
        
    }
    
    public func errorOccuredWithMessage(message:String) -> NSError{
        let error  = NSError.init(domain: NSURLErrorDomain, code: 1009, userInfo: [NSLocalizedDescriptionKey:message]);
        
        return error;
        
    }
    
    public func saveToken(token:String!)
    {
        prefrences.setObject(token, forKey: "AuthToken");
        prefrences.synchronize();
    }
    public func authToken()->String?
    {
        let str =   prefrences.valueForKey("AuthToken")
        if str != nil {
            return str as! String;
        }
        else
        {
            return nil;
        }
    }
    public func removeAuthToken()
    {
        prefrences.removeObjectForKey("AuthToken");
        prefrences.synchronize();
    }
    
    public func saveCartID(cartID:String!)
    {
        prefrences.setObject(cartID, forKey: "CART");
        prefrences.synchronize();
    }
   
    public func saveCartCount(cartID:String!)
    {
        prefrences.setObject(cartID, forKey: "CARTCOUNT");
        prefrences.synchronize();
    }
    
    public func getCartCount() -> String!
    {
        let str =   prefrences.valueForKey("CARTCOUNT")
        if str != nil {
            return str as! String;
        }
        else
        {
            return "";
        }    }
    public func removeCartCount()
    {
        prefrences.removeObjectForKey("CARTCOUNT");
        prefrences.synchronize();
    }

    
    public func saveWishListCount(cartID:String!)
    {
        prefrences.setObject(cartID, forKey: "WishList");
        prefrences.synchronize();
    }
    public func getWishListCount() -> String!
    {
        let str =   prefrences.valueForKey("WishList")
        if str != nil {
            return str as! String;
        }
        else
        {
            return "";
        }    }
    public func removeWishListCount()
    {
        prefrences.removeObjectForKey("WishList");
        prefrences.synchronize();
    }

    public func cartID()->String?
    {
        let str =   prefrences.valueForKey("CART")
        if str != nil {
            return str as! String;
        }
        else
        {
            return nil;
        }
    }
    public func saveProductDict(productDict:[String:String])
    {
        prefrences.setObject(productDict, forKey: "productDict");
        prefrences.synchronize();
    }
    
    public func productDict()->[String:String]
    {
        let str =   prefrences.valueForKey("productDict")
        if str != nil {
            return str as! [String:String];
        }
        else
        {
            return [String:String]();
        }
    }
    public func removeProductDict()
    {
        prefrences.removeObjectForKey("productDict");
        prefrences.synchronize();
    }
    public func removeCartID()
    {
        prefrences.removeObjectForKey("CART");
        prefrences.synchronize();
    }
    
    public func setUpAlerts()  {
        
        successAlert.notificationsPosition = LNRNotificationPosition.Top
        successAlert.notificationsBackgroundColor = UIColor.init(red: 46/255.0, green: 139/255.0, blue: 87/255.0, alpha: 1)
        successAlert.notificationsTitleTextColor = UIColor.whiteColor()
        successAlert.notificationsBodyTextColor = UIColor.whiteColor()
        successAlert.notificationsSeperatorColor = UIColor.grayColor()
        successAlert.notificationsIcon = UIImage(named: "success")
        
        //        let alertSoundURL: NSURL? = NSBundle.mainBundle().URLForResource("click", withExtension: "wav")
        //        if let _ = alertSoundURL {
        //            var mySound: SystemSoundID = 0
        //            AudioServicesCreateSystemSoundID(alertSoundURL!, &mySound)
        //            notificationManager1.notificationSound = mySound
        //        }
        
        errorAlert.notificationsPosition = LNRNotificationPosition.Top
        errorAlert.notificationsBackgroundColor = UIColor.init(red: 165/255.0, green: 42/255.0, blue: 42/255.0, alpha: 1)
        errorAlert.notificationsTitleTextColor = UIColor.whiteColor()
        errorAlert.notificationsBodyTextColor = UIColor.whiteColor()
        errorAlert.notificationsSeperatorColor = UIColor.grayColor()
        errorAlert.notificationsIcon = UIImage(named: "error")
        //        if let _ = alertSoundURL {
        //            var mySound: SystemSoundID = 0
        //            AudioServicesCreateSystemSoundID(alertSoundURL!, &mySound)
        //            notificationManager2.notificationSound = mySound
        //        }
        
        
    }
    
    public func showErrorALertWithMessage(message:String)
    {
        errorAlert.showNotification("Error!", body:message, onTap: { () -> Void in
            
            self.errorAlert.dismissActiveNotification({ () -> Void in
                // print("Notification dismissed")
            })
        })
        
    }
    
    public func showSuccessALertWithMessage(message:String)
    {
        successAlert.showNotification("Sucess!", body:message, onTap: { () -> Void in
            
            self.successAlert.dismissActiveNotification({ () -> Void in
                // print("Notification dismissed")
            })
        })
        
    }
    
    
    public func startMonitoringNetwork()
    {
        serviceInstance.networkReachabilityManager =  self.networkReachabilityManager;
        self.networkReachabilityManager?.startListening()
        
        self.networkReachabilityManager?.listener = {status in
            
            if  self.networkReachabilityManager?.isReachable ?? false {
                
                if ((self.networkReachabilityManager?.isReachableOnEthernetOrWiFi) != nil) {
                    
                    if  self.lastReachable == false {
                        self.showSuccessALertWithMessage("You are online. Enjoy Shopping");
                    }
                    self.lastReachable = true;
                }else if(self.networkReachabilityManager?.isReachableOnWWAN)! {
                    
                    if  self.lastReachable == false {
                        self.showSuccessALertWithMessage("You are online. Enjoy Shopping");
                    }
                    self.lastReachable = true;
                }
            }
            else {
                
                self.lastReachable = false;
                self.showErrorALertWithMessage("You are Offline. Please check your internet connection");
            }
            
            
        }
    }
    
    
    public func isNetworkAvailable() ->Bool
    {
        if (self.networkReachabilityManager?.isReachable == true) {
            return true;
        }
        else
        {
            self.showErrorALertWithMessage("You are not connected to Internet. Please connect to internet to enjoy our services");
            return false;
        }
    }
    
    func printJSON(params:[String:AnyObject]?)  {
        var error : NSError?
        
        let jsonData = try! NSJSONSerialization.dataWithJSONObject(params!, options: NSJSONWritingOptions.PrettyPrinted)
        
        let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)! as String
        
        print(jsonString)
    }
}

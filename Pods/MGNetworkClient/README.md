# MGNetworkClient

[![CI Status](http://img.shields.io/travis/ipragmatech/MGNetworkClient.svg?style=flat)](https://travis-ci.org/ipragmatech/MGNetworkClient)
[![Version](https://img.shields.io/cocoapods/v/MGNetworkClient.svg?style=flat)](http://cocoapods.org/pods/MGNetworkClient)
[![License](https://img.shields.io/cocoapods/l/MGNetworkClient.svg?style=flat)](http://cocoapods.org/pods/MGNetworkClient)
[![Platform](https://img.shields.io/cocoapods/p/MGNetworkClient.svg?style=flat)](http://cocoapods.org/pods/MGNetworkClient)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MGNetworkClient is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "MGNetworkClient"
```

## Author

ipragmatech, info@ipragmatech.com

## License

MGNetworkClient is available under the MIT license. See the LICENSE file for more info.

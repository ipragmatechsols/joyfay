//
//  Theme.swift
//  Pods
//
//  Created by Bunty on 28/05/16.
//
//

import Foundation


public class Theme: NSObject {
    
public  var themeColor: UIColor?
public  var appTitleName: String?
public static let sharedClient = Theme.init()
    
    private override init() {
//       self.themeColor = UIColor.init(colorLiteralRed: 41/255.0, green: 84/255.0, blue: 163/255.0, alpha: 1)
         self.themeColor = UIColor.init(colorLiteralRed: 203/255.0, green: 54/255.0, blue: 217/255.0, alpha: 1)
         self.appTitleName = "Joyfay"
       // self.themeColor = UIColor.blueColor();

    }
    
    public class func theme() ->Theme
    {
        return sharedClient;
    }
}

//
//  StyleKitName.swift
//  ProjectName
//
//  Created by Vikas Chauhan on 11/06/16.
//  Copyright (c) 2016 iPragmatech. All rights reserved.
//
//  Generated by PaintCode (www.paintcodeapp.com)
//



import UIKit

public class CustomViewKit : NSObject {

    //// Drawing Methods

    public class func drawTextFieldRect(rect:CGRect) {
        //// Color Declarations
        let color = UIColor(red: 0.500, green: 0.500, blue: 0.500, alpha: 0.000)

        //// Rectangle Drawing
        let rectanglePath = UIBezierPath(roundedRect: rect, cornerRadius: 31)
        color.setFill()
        rectanglePath.fill()
        UIColor.whiteColor().setStroke()
        rectanglePath.lineWidth = 10
        rectanglePath.stroke()
    }

}



# MagentoClient

[![CI Status](http://img.shields.io/travis/iPragmatech Solutions Pvt. Ltd./MagentoClient.svg?style=flat)](https://travis-ci.org/iPragmatech Solutions Pvt. Ltd./MagentoClient)
[![Version](https://img.shields.io/cocoapods/v/MagentoClient.svg?style=flat)](http://cocoapods.org/pods/MagentoClient)
[![License](https://img.shields.io/cocoapods/l/MagentoClient.svg?style=flat)](http://cocoapods.org/pods/MagentoClient)
[![Platform](https://img.shields.io/cocoapods/p/MagentoClient.svg?style=flat)](http://cocoapods.org/pods/MagentoClient)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MagentoClient is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "MagentoClient"
```

## Author

iPragmatech Solutions Pvt. Ltd., info@ipragmatech.com

## License

MagentoClient is available under the MIT license. See the LICENSE file for more info.
